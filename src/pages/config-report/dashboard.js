import React, {useState, useEffect} from 'react';
import api from '../../_helper/api';
import { Button } from 'primereact/button';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import { NavLink } from 'react-router-dom';

const Dashboard = () => {

  const [data, setData] = useState([]);

  useEffect(() => {
    api.get('config-report')
    .then(result => {
      console.log(result);
      setData(result.data);
    })
    .catch(err => {
      console.log(err);
    })
  }, []);

  const actionColumn = (rowData, column) => {
    return <div>
        <NavLink to={'config-report/edit/'+rowData.id }>
          <Button label="Edit" icon="pi pi-pencil" style={{ marginRight: '.25em' }} />
        </NavLink>
    </div>;
  }

  return (
    <DataTable 
      value={data} autoLayout={true}
      emptyMessage="tidak ada data"
      >
      <Column field="crontime" header="Crontime" />
      <Column field="email_host" header="Email Host" />
      <Column field="email_user" header="Email User" />
      <Column field="subject" header="Subject" />
      <Column field="body" header="Body" />
      <Column field="recipient" header="Recipient" />
      <Column field="name" header="Name" />
      <Column field="path" header="Path" />
      <Column body={actionColumn} style={{textAlign:'center', width: '8em'}}/>
    </DataTable>
  )

};

export default Dashboard;