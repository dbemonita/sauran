import React from 'react';
import { Toolbar } from 'primereact/toolbar';
import {Button} from 'primereact/button';
import { NavLink } from 'react-router-dom';

const Topbar = () => {

  return (
    <Toolbar>
      <div className="p-toolbar-group-left">
        <NavLink to='/config-report'>
          <Button label="List Report Config" icon="pi pi-home" style={{ marginRight: '.25em' }} />
        </NavLink>
        <NavLink to='/config-report/set-email'>
          <Button label="Config Email Sender" icon="pi pi-envelope" style={{ marginRight: '.25em' }} />
        </NavLink>
        <i className="pi pi-bars p-toolbar-separator" style={{ marginRight: '.25em' }} />
        <NavLink to='/config-report/new'>
          <Button label="New Report Config" icon="pi pi-pencil" style={{ marginRight: '.25em' }} />
        </NavLink>
      
      </div>
      <div className="p-toolbar-group-right">
        {/* <Button icon="pi pi-search" style={{ marginRight: '.25em' }} /> */}
        {/* <Button icon="pi pi-calendar" className="p-button-success" style={{ marginRight: '.25em' }} />
        <Button icon="pi pi-times" className="p-button-danger" /> */}
      </div>
    </Toolbar>
  );

};


export default Topbar;