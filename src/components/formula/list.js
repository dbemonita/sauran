import React, {useState, useEffect} from 'react';
import API from '../../_helper/api';
import {ListBox} from 'primereact/listbox';

const FormulaList = React.memo((props) => {

  const [formulaList, setFormulaList] = useState([]);
  const [formula, setFormula] = useState([]);

  useEffect( () => {

    async function fetchData(){
      const result = await API.get('admin/formula');
      setFormulaList(result.data);
    }

    fetchData();
  },[])

  return (
    <div>
      <span>Formula List</span>
      <ListBox
        style={{
          width: '100%',
          marginTop: 5
        }}
        filter={true}
        value={formula}
        options={formulaList}
        onChange={(e) => {
          if(e.value){
            setFormula(e.value);
            props.onUserSelected(e.value);
          }
        }}
        optionLabel="name"
      />
    </div>
  );

})

export default FormulaList;
