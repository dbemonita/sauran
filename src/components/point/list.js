import React, {useState, useEffect} from 'react';
import API from '../../_helper/api';
import {Dialog} from 'primereact/dialog';
import AddPoint from './add';
import MaterialTable from 'material-table'
import {ContextMenu} from "primereact/contextmenu";

const ListPoint = ((props) => {

  const [assetId, setAssetId] = useState('');
  const [typeListObj, setTypeListObj] = useState({});
  const [list, setList] = useState([]);
  const [updated, setUpdated] = useState({});
  const [addDialogVisible, setAddDialogVisible] = useState(false);
  const [isShowButtonUpdate, setIsShowButtonUpdate] = useState(false);
  const [selectedPoint, setSelectedPoint] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const menu = [{
    "label": "Delete",
    "icon": "fa fa-recycle",
    command: () => {
      window.confirm("Delete Confirmation") && onDelete();
    }
  }];

  useEffect(() => {
    API.get('admin/point-type')
      .then(typeList => {
        let _typeListObj = {};
        typeList.data.forEach(el => {
          _typeListObj[el.value] = el.label;
        });

        setTypeListObj(_typeListObj);
      });
  }, []);

  useEffect(() => {
    async function fetchData(){
      API.get('admin/point', {
        params: {
          assetId: props.assetId
        }
      })
        .then(result => setList(result.data))
        .catch(error => console.log(error))
        .finally( () => setIsLoading(false));
    }

    if (props.assetId !== '') {
      setAssetId(props.assetId);
      setIsLoading(true);
      fetchData();
    }

  }, [props.assetId]);

  const onDelete = ((data) => {
    API.delete('admin/point/' + data.id)
      .then((data) => {
        API.get('admin/point', {
          params: {
            assetId: assetId
          }
        })
          .then(result => setList(result.data))
          .catch(error => console.log(error));
      })
  });

  const onSaveChange = (() => {

    let payloads = [];
    setIsShowButtonUpdate(false);

    for (let idx in updated) {
      if (updated.hasOwnProperty(idx)) {
        let obj = {};
        let source = list[idx];

        updated[idx].forEach(el => {
          obj.idRef = source.idRef;
          obj[el] = source[el];
        });

        payloads.push(obj);
      }
    }

    API.patch('admin/point/' + assetId, {
      data: payloads
    }).then(result => {
      // loadData(assetId)
      //   .then(result => setList(result.data));
      API.get('admin/point', {
        params: {
          assetId: assetId
        }
      })
        .then(result => {
          setList(result.data);
          setUpdated({});
        })
        .catch(error => console.log(error));
    }).catch(err => {
      // loadData(assetId)
      //   .then(result => setList(result.data));
      API.get('admin/point', {
        params: {
          assetId: assetId
        }
      })
        .then(result => setList(result.data))
        .catch(error => console.log(error));
    })

  });

  const onUpdate = ((newData, oldData) => {
    try{
      const data = list;
      const index = data.indexOf(oldData);
      data[index] = newData;

      let field = '';
      for(let key in newData){
        if(newData.hasOwnProperty(key)){
          if(newData[key] !== oldData[key]){
            field = key;
          }
        }
      }

      if(!list[index].idRef){
        data[index].idRef = oldData.id;
      }

      setList(data);


      trackUpdated({rowIndex: index, field});
    }catch (e) {
      console.log(e);
    }
  });

  const trackUpdated = ((dt) => {
    const {rowIndex, field} = dt;

    let updating = (updated[rowIndex]) ? [...updated[rowIndex], field] : [field];

    setUpdated({
      ...updated,
      [rowIndex]: updating
    });

    if (Object.keys(updating).length > 0) {
      setIsShowButtonUpdate(true)
    }
  });

  const loadData = ((assetId) => {
    API.get('admin/point', {
      params: {
        assetId: assetId
      }
    })
      .then(result => setList(result.data))
      .catch(error => console.log(error));
  });

  let cm;

  return (
    <div className="p-grid p-fluid">
      <div className="p-col-12 p-lg-12">
        <ContextMenu model={menu} ref={el => cm = el} onHide={() => setSelectedPoint(null)}/>
        <Dialog header="New Point" visible={addDialogVisible} onHide={() => setAddDialogVisible(false)}>
          <AddPoint
            hide={() => setAddDialogVisible(false)} assetId={assetId}
            reload={() => {
              loadData(assetId)
            }} />
        </Dialog>
      </div>
      <div style={{ width: "100%" }}>
      <MaterialTable
        columns={[
          { title: 'Id', field: 'id', type: 'numeric' },
          { title: 'Name', field: 'name' },
          { title: 'Last Value', field: 'lastValue' },
          { title: 'Unit', field: 'unit' },
          { title: 'Type', field: 'point_type_id', lookup: typeListObj },
          { title: 'Range Min', field: 'range_min', type: 'numeric' },
          { title: 'Range Max', field: 'range_max', type: 'numeric' },
          { title: 'M', field: 'm', type: 'numeric' },
          { title: 'C', field: 'c', type: 'numeric' },
        ]}
        data={list}
        title="Demo Title"
        options={{
          headerStyle: {
            paddingTop: 1,
            paddingBottom: 1,
            border: '1px solid rgba(224, 224, 224, 1)'
          },
          cellStyle: {
            paddingTop: 1,
            paddingBottom: 1,
            border: '1px solid rgba(224, 224, 224, 1)'
          },
          padding: "dense"
        }}
        actions={[
          {
            icon: 'add',
            tooltip: 'Add',
            isFreeAction: true,
            onClick: (event) => setAddDialogVisible(true)
          },
          {
            icon: 'save',
            tooltip: 'Update',
            isFreeAction: true,
            hidden: (!isShowButtonUpdate),
            onClick: (event) => onSaveChange()
          }
        ]}
        editable={{
          onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            onUpdate(newData, oldData);
            resolve();
          }),
          onRowDelete: oldData =>
            new Promise((resolve, reject) => {
              onDelete(oldData);
              resolve();
            })
        }}
      />
      </div>
    </div>
  )

});

export default ListPoint;