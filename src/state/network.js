const networkReducer = (state, action) => {
  switch (action.type) {
    case 'setNetwork':
      return {
        ...state,
        [action.payload.type]: action.payload.status
      };
    case 'setOnApiRequest':
      return {
        ...state,
        onApiRequest: action.payload
      };
    default:
      return state;
  }
};

export default networkReducer;