import React, { Component } from 'react';
import { InputText } from 'primereact/inputtext';
import {Dropdown} from "primereact/dropdown";
import { Button } from 'primereact/button';
import API from "../../_helper/api";

export class EditModule extends Component {
	constructor(props){
		super(props);
		this.state = {
			data: {
        name: '',
        serial_number: '',
        status: 1,
        timeout: '',
        type: ''
      },
       typeList: [{
        label: 'A',
        value: 'A'
      },{
        label: 'B',
        value: 'B'
      },{
        label: 'C',
        value: 'C'
      },{
        label: 'Modbus',
        value: 'Modbus'
      }]
		};

		this.onChangeValue = this.onChangeValue.bind(this);
		this.onSave = this.onSave.bind(this);
	}

	componentDidMount() {
	  const {data} = this.props;

	  if(data.id){
	    this.setState({
      data: data
    })
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.data.id !== this.props.data.id){
      this.setState({
        data: nextProps.data
      })
    }
  }

  onChangeValue(e) {
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value
      }
    })
  }

  onSave(){
    const { data } = this.state;

    API.patch('admin/module/' + data.id, {
      name: data.name,
      serialNumber: data.serial_number,
      type: data.type,
      status: data.status,
      timeout: data.timeout
    }).then(result => {
      this.props.done()
    }).catch(err => {
      console.log(err);
    })
  }

  render() {
    const { data } = this.state;
    
    return (
      <div className="p-grid p-fluid">
        <div className="p-col-12 p-lg-12">
          <h1> Module Edit {data.name} </h1>
          <div className="card card-w-title">
            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="name">Name</label>
                <InputText name="name" value={data.name} onChange={this.onChangeValue} autoFocus />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="parent_id">Serial Number</label>
                <InputText name="serial_number" value={data.serial_number} onChange={this.onChangeValue} />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-6">
                <label htmlFor="address">Address</label>
                <Dropdown
                  name="type"
                  value={data.type}
                  options={this.state.typeList}
                  onChange={this.onChangeValue}
                  placeholder="Select Module Type"
                />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="timeout">Timeout</label>
                <InputText name="timeout" value={data.timeout} onChange={this.onChangeValue} />
              </div>
            </div>
            
            <Button label="Save" className="p-button-raised" onClick={this.onSave}/>

          </div>
        </div>
      </div>
    )
  }
}