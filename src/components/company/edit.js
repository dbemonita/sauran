import React, { Component } from 'react';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { Button } from 'primereact/button';
import API from '../../_helper/api';

export class EditCompany extends Component {
	constructor(props){
		super(props);
		this.state = {

			data: {
			  id: '',
				name: 'Name',
				parentId: 0,
				address: ''
			}
		};

		this.onChangeValue = this.onChangeValue.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
	}

	componentWillReceiveProps(nextProps) {
    if (nextProps.node.key !== this.props.node.key) {
      this.setState({
        data: {
          id: nextProps.node.data.id,
          name: nextProps.node.data.name,
          parentId: nextProps.node.data.parentId || '',
          address: nextProps.node.data.address || ''
        }
      });
    }
  }

	componentDidMount(){
    const {data} = this.props.node;
    console.log(data);
		this.setState({
      data: {
        id: data.id,
        name: data.name || '',
        parentId: data.parentId || '',
        address: data.address || ''
      }
    });
	}

	onChangeValue(e) {
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value
      }
    })
  }

  onUpdate(){
    const {data} = this.state;
    API.patch('admin/company/' + data.id , {
      name: data.name,
      parentId: data.parentId,
      address: data.address,
      status: data.status
    }).then(result => {
      console.log(result);
      // this.props.reload();
    }).catch(err => {
      console.log(err);
    })
  }

	render() {
    const { data } = this.state;
    
    return (
      <div className="p-grid p-fluid">
        <div className="p-col-12 p-lg-12">
          <div className="card card-w-title">
            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="name">Name</label>
                <InputText name="name" value={data.name} onChange={this.onChangeValue} autoFocus />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="parent_id">Parent ID</label>
                <InputText name="parent_id" value={data.parentId || ''} onChange={this.onChangeValue}  />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-6">
                <label htmlFor="address">Address</label>
                <InputTextarea name="address" value={data.address} onChange={this.onChangeValue} rows={5} >

                </InputTextarea>
              </div>
            </div>
            
            <Button label="Update" className="p-button-raised" onClick={this.onUpdate}/>

          </div>
        </div>
      </div>
    )
  }
}