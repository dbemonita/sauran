import React, {lazy, Suspense, memo } from 'react';
import {Route, Switch} from 'react-router-dom';

const Dashboard = (
  lazy(() => (
    import('../pages/Dashboard')
  ))
);

const AssetManagement = (
  lazy(() => (
    import('../pages/AssetManagement')
  ))
);

const UserManagement = (
  lazy(() => (
    import('../pages/UserManagement')
  ))
);

const ModuleParsing = (
  lazy(() => (
    import('../pages/ModuleParsing')
  ))
);


const VisualMenu = (
  lazy(() => (
    import('../pages/VisualMenu')
  ))
);

const VisualEditor = (
  lazy(() => (
    import('../pages/VisualEditor')
  ))
);

const Formula = (
  lazy(() => (
    import('../pages/Formula')
  ))
);

const Login = (
  lazy(() => (
    import('../pages/Login')
  ))
);

const ConfigReport = (
  lazy(() => (
    import('../pages/ConfigReport')
  ))
);

const LoadingMessage = () => (
  "Loading..."
);

const AppRoute = () => {
  return (
    <div className="layout-main">
      <Suspense fallback={<LoadingMessage />}>
        <Switch>
          <Route path="/login" >
            <Login />
          </Route>
          <Route path="/" exact >
            <Dashboard />
          </Route>
          <Route path="/asset-management" >
            <AssetManagement />
          </Route>
          <Route path="/module-parsing" >
            <ModuleParsing />
          </Route>
          <Route path="/user-management" >
            <UserManagement />
          </Route>
          <Route path="/visual-editor" >
            <VisualEditor />
          </Route>
          <Route path="/visual-menu" >
            <VisualMenu />
          </Route>
          <Route path="/formula" >
            <Formula />
          </Route>
          <Route path="/config-report" >
            <ConfigReport />
          </Route>
        </Switch>
      </Suspense>
    </div>
  )

}

export default memo(AppRoute, () => true);
