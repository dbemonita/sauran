import React, {useState, useEffect} from 'react';
import API from '../../_helper/api';
import AceEditor from 'react-ace';
import 'brace/mode/xml';
import {InputText} from "primereact/inputtext";
import 'brace/theme/monokai';
import 'brace/theme/github';
import 'brace/theme/tomorrow';
import 'brace/theme/kuroir';
import 'brace/theme/twilight';
import 'brace/theme/xcode';
import 'brace/theme/textmate';
import 'brace/theme/solarized_dark';
import 'brace/theme/solarized_light';
import 'brace/theme/terminal';

import 'brace/ext/searchbox'
import {Menubar} from 'primereact/menubar';
import {Dropdown} from "primereact/dropdown";

const themesList = [
  {
    label: 'monokai',
    value: 'monokai'
  },
  {
    label: 'github',
    value: 'github'
  },
  {
    label: 'tomorrow',
    value: 'tomorrow'
  },
  {
    label: 'kuroir',
    value: 'kuroir'
  },
  {
    label: 'twilight',
    value: 'twilight'
  },
  {
    label: 'xcode',
    value: 'xcode'
  },
  {
    label: 'textmate',
    value: 'textmate'
  },
  {
    label: 'solarized_light',
    value: 'solarized_light'
  },
  {
    label: 'solarized_dark',
    value: 'solarized_dark'
  },
  {
    label: 'terminal',
    value: 'terminal'
  }
]

const XmlEditor = ((props) => {
  const [theme, setTheme] = useState({value: 'monokai', label: 'monokai'});
  const [themes] = useState(themesList);
  const [editorFontSize, setEditorFontSize] = useState(14);
  const [file, setFile] = useState('');
  const [fileName, setFileName] = useState('');
  const [filePath, setFilePath] = useState('');

  const menu = [
    {
      label: 'Save',
      icon: 'pi pi-fw pi-save',
      command: () => {
        onSave();
      }
    }
  ];

  useEffect(() => {
    if(props.filePath){
      API.get('visual/xml/' + props.filePath)
      .then(response => {
        let spl = props.filePath.split('/');

        setFile(response.data);
        setFilePath(props.filePath);
        setFileName(spl[spl.length - 1]);
      })
      .catch(error => {
        console.log(error);
      });
    }
  }, [props.filePath]);

  const onSave = () => {
    if (file !== '') {
      API.post('admin/xml', {
        content: file,
        path: filePath
      }).then(result => {
        console.log('xml saved');
      }).catch(error => {
        console.log(error);
      })
    }
  };

  return (
    <div>
      <Menubar model={menu} style={{marginBottom: 10}}>
        <div className="p-inputgroup" style={{display: 'inline-block', marginRight: 10}}>
          <span className="p-inputgroup-addon">Font Size</span>
          <InputText
            size={3}
            value={editorFontSize}
            onChange={(e) => {
              setEditorFontSize(parseInt(e.currentTarget.value))
            }} type="text"/>
        </div>
        <div className="p-inputgroup" style={{display: 'inline-block'}}>
          <span className="p-inputgroup-addon">Theme</span>
          <Dropdown
            style={{marginRight: 10}}
            optionLabel="label"
            value={theme}
            options={themes}
            onChange={(e) => {
              setTheme(e.value)
            }}
            placeholder="Themes"
          />
        </div>
        <label style={{marginRight: 15}}> File : {fileName}</label>
      </Menubar>
      <AceEditor
        mode="xml"
        tabSize={2}
        fontSize={editorFontSize}
        theme={theme.value}
        width='100%'
        height={(window.innerHeight - 138) + 'px'}
        value={file}
        path={filePath}
        onChange={(e) => setFile(e)}
        name="xml_editor"
        editorProps={{
          $blockScrolling: true
        }}
      />
    </div>

  )
})

export default XmlEditor;
