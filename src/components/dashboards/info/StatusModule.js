import React, {useState, useEffect } from 'react';
import API from '../../../_helper/api';
import { IntlProvider, FormattedRelativeTime } from "react-intl";

const StatusModule = ((props) => {

  console.log(props);
  const [firstTime, setFirstTime] = useState(0);
  const [lastTime, setLastTime] = useState(0);
  const [moduleId, setModuleId] = useState('');

  useEffect(() => {
    if(props.moduleId){
      API.get('monitor/module/status/' + props.moduleId)
      .then((data) => {
        if(data.data){
          setModuleId(props.moduleId);
          setFirstTime(data.data.firstTime);
          setLastTime(data.data.lastTime)
        }
      })
    }
  }, [props.moduleId]);

  return (
    (moduleId !== '') ?
      <IntlProvider locale="id-ID">
        <div >
          <p>
            First Time :
            <FormattedRelativeTime
              numeric="auto"
              unit="second"
              updateIntervalInSeconds={1 * 60}
              value={ firstTime - (Math.floor(new Date().getTime() / 1000)) }
            />
          </p>
          <p>
            Last Time :
            <FormattedRelativeTime
              numeric="auto"
              unit="second"
              updateIntervalInSeconds={1 * 60}
              value={ lastTime - (Math.floor(new Date().getTime() / 1000)) }
            />
          </p>
        </div>
      </IntlProvider> : ''
  )

})

export default StatusModule;
