import React, {useEffect, useState} from 'react';
import {subscriber, emitter} from "../../../_helper/subscriber";
import API from '../../../_helper/api';
import {Fieldset} from 'primereact/fieldset';
import {Button} from "primereact/button";

function fetchInfo(){
  return API.get("admin/service/loket/info");
}

const SystemLoket = () => {

  const [data, setData] = useState({
    cpu: '',
    memory: '',
  });

  const [info, setInfo] = useState({
    uptime: 0,
    normalRestart: 0,
    unstableRestart: 0
  });

  const [restart, setRestart] = useState(0);

  useEffect(() => {
    function onWsMessage(resp){
      let data = JSON.parse(resp);

      setData({
        ...data,
        [Object.keys(data)[0]]: Object.values(data)[0]
      });
    }

    let syMon = subscriber.subscribe('systemLoketMonitoring');
    syMon.watch(onWsMessage);

    emitter.emit('getSystemLoketMonitoring');

    return function cleaning(){
      let channel = subscriber.channel('systemLoketMonitoring');
      if(channel){
        channel.unwatch(onWsMessage);
        subscriber.unsubscribe('systemLoketMonitoring');
      }
    };
  }, []);

  useEffect(() => {
    fetchInfo()
      .then(response => {
        if(Object.keys(response.data).length > 0){
          setInfo({
            ...response.data
          })
        }
      })
      .catch(err => {
        console.log(err);
      })
  }, [restart]);

  const restartLoket = () => {
    API.get("admin/service/loket/restart")
      .then( () => setRestart(restart + 1));

  };

  return (
    <Fieldset legend="Loket " toggleable={true}>
      <div>
        <p>CPU : {data.cpu}</p>
        <p>Memory : {data.memory}</p>
        <p>Uptime : {info.uptime} s</p>
        <p>Normal Restart : {info.normalRestart}</p>
        <p>Unstable Restart : {info.unstableRestart}</p>
        <Button label="restart" onClick={restartLoket}/>
      </div>
    </Fieldset>
  )

};

export  default  SystemLoket;
