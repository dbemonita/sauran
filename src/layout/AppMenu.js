import React, {  useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom'
import classNames from 'classnames';
import API from "../_helper/api";

const AppSubmenu = (props) => {
  const [activeIndex, setActiveIndex] = useState(null);
  const [items, setItems] = useState(null);

  useEffect(() => {
    function onMenuItemClick (event, item, index) {
      //avoid processing disabled items
      if (item.disabled) {
        event.preventDefault();
        return true;
      }

      //execute command
      if (item.command) {
        item.command({originalEvent: event, item: item});
      }

      if (index === activeIndex)
        setActiveIndex(null);
      else
        setActiveIndex(index);

      if (props.onMenuItemClick) {
        props.onMenuItemClick({
          originalEvent: event,
          item: item
        });
      }
    }

    function renderLinkContent (item) {
      let submenuIcon = item.items && <i className="pi pi-fw pi-angle-down menuitem-toggle-icon"> </i>;
      let badge = item.badge && <span className="menuitem-badge">{item.badge}</span>;

      return (
        <React.Fragment>
          <i className={item.icon}> </i>
          <span>{item.label}</span>
          {submenuIcon}
          {badge}
        </React.Fragment>
      );
    }

    function renderLink (item, i) {
      let content = renderLinkContent(item);

      if (item.to) {
        return (
          <NavLink activeClassName="active-route" to={item.to} onClick={(e) => onMenuItemClick(e, item, i)} exact target={item.target}>
            {content}
          </NavLink>
        )
      }
      else {
        return (
          <a href={item.url} onClick={(e) => onMenuItemClick(e, item, i)} target={item.target}>
            {content}
          </a>
        );

      }
    }

    if(props.items && props.items.length > 0){
      let _items = props.items && props.items.map((item, i) => {
        let active = activeIndex === i;
        let styleClass = classNames(item.badgeStyleClass, { 'active-menuitem': active && !item.to });

        return (
            <li className={styleClass} key={i}>
              {item.items && props.root === true && <div className='arrow'></div>}
              {renderLink(item, i)}
              <AppSubmenu items={item.items} onMenuItemClick={props.onMenuItemClick} />
            </li>
        );
      });

      setItems(_items ? <ul className={props.className}>{_items}</ul> : null);
    }

  }, [props.items, activeIndex, props.className, props.onMenuItemClick, props.root]);

  return items;
};

const AppMenu = (props) => {

  const [items, setItems] = useState([]);

  useEffect(() => {
    API.get('/admin/page')
      .then(result => {
        setItems(result.data);
      });
  }, []);

  return <div className="menu">
    <AppSubmenu items={items} className="layout-main-menu" onMenuItemClick={props.onMenuItemClick} root={true} />
  </div>
};

export default AppMenu;
