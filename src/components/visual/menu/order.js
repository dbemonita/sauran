import React, {useEffect, useState} from 'react';
import {Tree} from 'primereact/tree';
import {Button} from "primereact/button";
import API from '../../../_helper/api';
import {buildTreeUser} from '../../../_helper/built-tree';
import { Fab, Action } from 'react-tiny-fab';
import 'react-tiny-fab/dist/styles.css';
import {Inplace,InplaceDisplay,InplaceContent} from 'primereact/inplace';
import {InputText} from 'primereact/inputtext';

const difference = (oldData, newData) => {
  let added =  newData.filter(el => { return !oldData.includes(el); });
  let deleted = oldData.filter(el => { return !newData.includes(el); });

  return {added, deleted};
};

const flatten = (children, extractChildren) => Array.prototype.concat.apply(
  children, 
  children.map(x => flatten(extractChildren(x) || [], extractChildren))
);

const extractChildren = x => x.children;

const MenuOrder = (props) => {

  const [expandedKeys, setExpandedKeys] = useState({});
  const [visualArray, setVisualArray] = useState([]);
  const [visualObject, setVisualObject] = useState([]);
  const [visualUser, setVisualUser] = useState([]);
  const [visualChecked, setVisualChecked] = useState({});
  const [visualUserList, setVisualUserList] = useState([]);
  const [edit, setEdit] = useState({});
  const [onEditing, setOnEditing] = useState(false);

  useEffect(() => {
    try {
      const fetchData = async () => {
        let {data} = await API.get('admin/visual/list?tree=false');
  
        if(data.length > 0){
          let dataIdKey = data.reduce((obj, el) => {
            return {
              ...obj,
              [el.id]: el
            };
          }, {});
          setVisualArray(data);
          setVisualObject(dataIdKey);
        }
      };

      fetchData();
    } catch (error) {
      console.log(error);
    }
  }, []);

  useEffect(() => {
    if(props.visualChecked){

      let expanding = {};
      Object.keys(props.visualChecked).forEach(el => {
        let toArr = el.split('-');
        for (let i = 0; i < toArr.length; i++) {
          let idx = toArr.slice(0, (toArr.length-i));
          if(idx.length > 0){
            expanding[idx.join('-')] = true;
          }else{
            expanding[idx] = true;
          }
          
        }
      });

      if(Object.keys(visualChecked).length > 0){
        let {deleted, added} = difference(Object.keys(visualChecked), Object.keys(props.visualChecked));
        let newCheckedVList = visualChecked;
        if(added.length > 0){
          added.forEach(el => {
            let onlyPathArr = visualArray.filter( elem => elem.key.toString() === el.toString());
            if(onlyPathArr.length > 0) {
              newCheckedVList[onlyPathArr[0].key] = {
                order: props.visualChecked[el] ? props.visualChecked[el].order : 0,
                ...onlyPathArr[0]
              };
            }
          });
        }

        if(deleted.length > 0){
          deleted.forEach( el => delete newCheckedVList[el]);
        }

        const {tree, list} = buildTreeUser(newCheckedVList, visualArray, visualObject);

        setVisualUser(tree);
        setVisualUserList(list);
        setVisualChecked(newCheckedVList);
      }else{
        let newCheckedVList = {};
        
        Object.keys(expanding).forEach( val => {
          let onlyPathArr = visualArray.filter( elem => elem.key.toString() === val.toString());

          if(onlyPathArr.length > 0) {
            newCheckedVList[onlyPathArr[0].key] = {
              ...props.visualChecked[val],
              ...onlyPathArr[0]
            };
          }
        });

        const {tree, list} = buildTreeUser(newCheckedVList, visualArray, visualObject);
      
        setVisualUser(tree);
        setVisualUserList(list);
        setVisualChecked(newCheckedVList);
      }

      setExpandedKeys(expanding);
    }
    
  }, [props.visualChecked]);

  const onSave = async () => {
    try {
      let payload = {
        userId: props.user.userId,
        visual: []
      };

      let flats = [];
      visualUser.forEach(el => {
        let flatted = flatten(extractChildren(el), extractChildren)
          .map(x => delete x.children && x);
        flats = [
          ...flats,
          ...flatted
        ];
      });

      payload.visual = flats.filter(el => el.leaf === true).map((el, i) => {
        let order = i+1;
        return {
          ...el,
          order
        };
      });

       API.patch('admin/visual/user', payload);
    } catch (error) {
      console.log(error);
    }
  };

  const onChangeName = (e) => {
    const {value, dataset} = e.currentTarget;
    setEdit({
      name: value,
      path: dataset.path
    });
    
  };

  const onDoneChange = () => {
    console.log(edit);
    let newCheckedVList = {...visualChecked };

    let key;
    Object.keys(newCheckedVList).forEach(el => {
      if(newCheckedVList[el].path === edit.path) {
        key = el;
      }
    });

    newCheckedVList[key].name = edit.name;

    const {tree, list} = buildTreeUser(newCheckedVList, visualArray, visualObject);
  
    setVisualUser(tree);
    setVisualUserList(list);
    setVisualChecked(newCheckedVList);

  };
  
  const listTemplate = (node) => {
    if(node.path){
      return (<div>
        <b>{node.path}</b> <br/>
        <Inplace closable={true} onClose={onDoneChange}>
          <InplaceDisplay>
            {node.name}
            <i className="pi pi-pencil"></i>
          </InplaceDisplay>
          <InplaceContent>
            <InputText autoFocus data-path={node.path} onChange={onChangeName} placeholder={node.name}/>
          </InplaceContent>
        </Inplace>
      </div>)
    }else{
      return (<div>
        <b>{node.label}</b>
      </div>)
    }
  }

  return (
    <div>
      <Tree
        style={{
          height: (window.innerHeight - 145) + 'px',
          maxHeight: (window.innerHeight - 145 ) + 'px'
        }}
        value={visualUser}
        expandedKeys={expandedKeys}
        onToggle={e => {
          setExpandedKeys(e.value)
        }}
        dragdropScope="vmuser"
        onSelect={e => {
          // console.log('onSelect', e);
        }}
        onSelectionChange={e => {
          // console.log('onSelectionChange', e);
        }}
        nodeTemplate={listTemplate}
        onDragDrop={ e=> {
          // console.log(e);
          setVisualUser(e.value);
        }}
      />

      <Fab
        mainButtonStyles={{
          backgroundColor: '#00b5ad',
        }}
        position={{
          bottom: 0,
          right: 0,
        }}
        icon={<i className="pi pi-save" ></i>}
        event="hover"
        key={-1}
        alwaysShowTitle={false}
        onClick={() => onSave()}
        text="Save"
      />
    </div>
  );
};

export default MenuOrder;