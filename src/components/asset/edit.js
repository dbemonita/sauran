import React, {useState, useEffect} from 'react';
import {InputText} from 'primereact/inputtext';
import {Dropdown} from 'primereact/dropdown';
import {Button} from 'primereact/button';
import API from '../../_helper/api';
import {TabView, TabPanel} from 'primereact/tabview';
import ListPoint from '../point/list';

const EditAsset = ((props) => {

  const [showButtonUpdate, setShowButtonUpdate] = useState(false);
  const [activeIndex, setActiveIndex] = useState(0);
  const [data, setData] = useState({
    id: '',
    name: '',
    parentId: '',
    type: ''
  });
  const [assetTypeList, setAssetTypeList] = useState([]);

  useEffect(() => {
    API.get('admin/asset-type')
      .then(result => {
        setAssetTypeList(result.data);
      })
      .catch(e => {
        console.log(e);
      })
  }, []);

  useEffect(() => {
    if(props.node){
      setData({
        id: props.node.data.id,
        name: props.node.data.name,
        parentId: props.node.data.parentId || '',
        type: props.node.data.assetTypeId
      })
    }
  }, [props.node]);

  const onChangeValue = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    });

    setShowButtonUpdate(true);
  };

  const onUpdate = () => {
    API.patch('admin/asset/crud/' + data.id, {
      name: data.name,
      parentId: data.parentId,
      type: data.type
    }).catch(err => {
      console.log(err);
    })
  };

  return (
    <div className="p-grid p-fluid">
      <div className="p-col-12 p-lg-12">
        <TabView activeIndex={activeIndex} onTabChange={(e) => {
          setActiveIndex(e.index)
        }}>
          <TabPanel header="Detail" leftIcon="fa fa-edit">
            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="name">Name</label>
                <InputText name="name" value={data.name} onChange={onChangeValue} autoFocus/>
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <InputText name="name" type="hidden" value={data.parentId} onChange={onChangeValue}/>
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="assetType">Asset Type</label>
                <Dropdown
                  name="assetType"
                  value={data.type}
                  options={assetTypeList}
                  onChange={onChangeValue}
                  placeholder="Select Asset Type"
                />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                {(showButtonUpdate) && (
                  <Button label="Update" className="p-button-raised" onClick={onUpdate}/>
                )}
              </div>
            </div>
          </TabPanel>
          <TabPanel header="Points" rightIcon="fa fa-dot-circle-o" style={{padding: 0}} contentStyle={{padding: '0px'}}>
            <ListPoint assetId={props.node.data.id} style={{padding: 0}} />
          </TabPanel>
        </TabView>
      </div>
    </div>
  )

});

export default EditAsset;
