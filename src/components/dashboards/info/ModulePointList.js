import React, { memo, useState, useEffect } from 'react';
import API from '../../../_helper/api';
import {Message} from 'primereact/message';

const ModulePointList = (props) => {

  // const [moduleId, setModuleId] = useState('');
  const [data, setData] = useState([]);

  useEffect(() => {
    console.log('in effect');
    function fetchData() {
      console.log('fetching');
      API.get('/monitor/module/info-parsing/' + props.moduleId)
        .then((data) => {
          if(data.data){
            setData(data.data)
          }else{
            setData([]);
          }
        })
    }

    if(props.moduleId){
      fetchData();
    }
  }, [props.moduleId]);

  return (
    <div className="p-grid" style={{marginTop: '.5em'}}>
      {
        data.map((el, i )=>
          <div key={i} className="p-col-12 p-md-2">
            <Message severity="info" text={el.order +') '+ el.pointId +'-'+ (el.pointName) } />
          </div>
        )
      }
      {data.length > 0 && <div className="p-col-12">
        <Message style={{fontSize: 10}} severity="warn" text="Order - Point Id - Point Name" />
      </div>}
    </div>
  )
};

export default memo(ModulePointList);