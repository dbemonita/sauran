import React, { Component } from 'react';
import { InputText } from 'primereact/inputtext';
import API from '../../_helper/api';
import { Button } from 'primereact/button';
import { Dropdown } from 'primereact/dropdown';

export class EditPoint extends Component {
  constructor(props) {
    super(props);
    this.state = {
      parentNode: props.node,
      pointTypeList: [],
      data: {
        id: '',
        name: '',
        unit: '',
        pointTypeId: null,
        rangeMin: 0,
        rangeMax: 100,
        m: 1,
        c: 0,
        assetId: ''
      }
    };

    this.onChangeValue = this.onChangeValue.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
  }

  async componentDidMount() {
    let typeList = await API.get('admin/point-type');
    const {data} = this.props.node;

    this.setState({
      pointTypeList: typeList.data,
      data: {
        id: data.id,
        name: data.name,
        unit: data.unit,
        pointTypeId: data.point_type_id,
        rangeMin: data.range_min,
        rangeMax: data.range_max,
        m: data.m,
        c: data.c,
        assetId: data.asset_id,
      }
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.node.id !== this.state.parentNode.id) {
      this.setState({
        parentNode: nextProps.node,
        data: {
          id: nextProps.node.data.id,
          name: nextProps.node.data.name,
          unit: nextProps.node.data.unit,
          pointTypeId: nextProps.node.data.point_type_id,
          rangeMin: nextProps.node.data.range_min,
          rangeMax: nextProps.node.data.range_max,
          m: nextProps.node.data.m,
          c: nextProps.node.data.c,
          assetId: nextProps.node.data.asset_id,
        }
      });
    }
  }

  onChangeValue(e) {
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value
      }
    })
  }

  onUpdate() {
    const { data } = this.state;
    API.patch('admin/point/' + data.id, {
      name: data.name,
      unit: data.unit,
      pointTypeId: data.pointTypeId,
      rangeMin: data.rangeMin,
      rangeMax: data.rangeMax,
      m: data.m,
      c: data.c,
      assetId: data.assetId
    }).then(result => {
      // this.props.reload();
    }).catch(err => {
      console.log(err);
    })
  }

  render(){
    const {data} = this.state;
    return (
      <div className="p-grid p-fluid">
        <div className="p-col-12 p-lg-12">
          <div className="card card-w-title">
            <h1>Point</h1>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="id">ID</label>
                <InputText name="id" value={data.id} onChange={this.onChangeValue}  />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="name">Name</label>
                <InputText name="name" value={data.name} onChange={this.onChangeValue} autoFocus />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-4">
                <label htmlFor="unit">Unit</label>
                <InputText name="unit" value={data.unit} onChange={this.onChangeValue}  />
              </div>
              <div className="p-col-12 p-md-4">
                <label htmlFor="m">M</label>
                <InputText name="m" value={data.m} onChange={this.onChangeValue}  />
              </div>
              <div className="p-col-12 p-md-4">
                <label htmlFor="c">C</label>
                <InputText name="c" value={data.c} onChange={this.onChangeValue}  />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-6">
                <label htmlFor="rangeMin">Range Min</label>
                <InputText name="rangeMin" value={data.rangeMin} onChange={this.onChangeValue}  />
              </div>
              <div className="p-col-12 p-md-6">
                <label htmlFor="rangeMax">Range Max</label>
                <InputText name="rangeMax" value={data.rangeMax} onChange={this.onChangeValue}  />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="pointTypeId">Point Type</label>
                <Dropdown
                  name="pointTypeId"
                  value={this.state.data.pointTypeId}
                  options={this.state.pointTypeList}
                  onChange={this.onChangeValue}
                  placeholder="Select Points Type"
                />
              </div>
            </div>
            <Button label="Update" className="p-button-raised" onClick={this.onUpdate} />
          </div>
        </div>
      </div>
    )
  }
}