import socket from "./socket-cluster";

const subscribedChannels = {};

const subscriber = {
  channel: (channel) => {
    return subscribedChannels[channel];
  },
  subscribe: (channel) => {
    subscribedChannels[channel] = socket.subscribe(channel);
    return subscribedChannels[channel];
  },
  unsubscribe: (channel) => {
    subscribedChannels[channel].unsubscribe(channel);
  }
};

const emitter = {
  emit: (channel, param, handler) => {
    return socket.emit(channel, param, handler);
  }
};

export {subscriber, emitter};