import React from "react"
import { Route, Redirect } from "react-router-dom";
// import { withContext } from "../AppContext"

function ProtectedRoute(props) {
  console.log('protected route ', props);
  const { component: Component, ...rest } = props;

  return (
    props.token ?
      <Route {...props} /> :
      <Redirect to="/login" />
  )
}

// export default withContext(ProtectedRoute);
export default ProtectedRoute;
