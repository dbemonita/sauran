import React, {useState, useEffect, memo} from 'react';
import {Message} from "primereact/message";
import {useStateValue} from '../appState';
import {ProgressBar} from 'primereact/progressbar';

const AppFooter = () => {
  const [ws, setWs] = useState(false);
  const [api, setApi] = useState(false);
  const [onApiRequest, setOnApiRequest] = useState(false);
  const [{network}] = useStateValue();

  useEffect(() => {
    setOnApiRequest(network.onApiRequest);
  }, [network.onApiRequest]);

  useEffect(() => {
    setApi(network.api);
  }, [network.api]);

  useEffect(() => {
    setWs(network.ws);
  }, [network.ws]);

  return (
    <div >
      {(onApiRequest) &&
        <ProgressBar style={{
          height:5,
          zIndex:9999,
          position: 'fixed',
          width: '100%',
          bottom: 0,
          left: 0
        }} mode="indeterminate" />}
      <div className="layout-footer"
        style={{
          position: 'fixed',
          flexGrow: 1,
          width: '100%',
          bottom: 0,
          padding: 3
        }}>
        {!ws && (
          <Message style={{
            padding: 0,
            marginRight: 3
          }} severity="error" text="Websocket error" />
        )}

        {!api && (
          <Message style={{
            padding: 0,
            marginRight: 3
          }} severity="error" text="API error" />
        )}

      </div>
    </div>
  );
};

export default memo(AppFooter, () => false);
