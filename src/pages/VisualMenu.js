import React, {useState} from 'react';
import {VisualUser} from '../components/visual/user';
// import VisualSelect from "../components/visual/select.bak.js";
import VisualSelect from "../components/visual/select";

const VisualMenu = (props) => {

  const [user, setUser] = useState(null);

  const onUserSelected = (user) => {
    setUser(user);
  };

  return (
    <div  className="p-grid">
      <div className="p-md-3 p-sm-12">
        <span> Select User </span>
        <VisualUser onUserSelected={onUserSelected}/>
      </div>
      <div className="p-md-9 p-sm-12" >
        <span>Pick menu for selected user </span>
        <VisualSelect user={user}/>
      </div>
    </div>
  )
}

export default VisualMenu;