import React, { Component } from 'react';
import {Accordion, AccordionTab} from 'primereact/accordion';
import {InputText} from "primereact/inputtext";
import {Button} from "primereact/button";
import API from '../../_helper/api';

export class AddParsingRef extends Component {
  constructor(props) {
    super(props);
    this.state = {
      moduleId: '',
      data: [],
      parsingRef: {},
      dataChanged: [],
      activeIndex: 0
    };

    this.onChangeValue = this.onChangeValue.bind(this);
    this.onSave = this.onSave.bind(this);
    this.loadData = this.loadData.bind(this);
    this.setData = this.setData.bind(this);
    this.onTabChange = this.onTabChange.bind(this);
  }

  componentDidMount() {
    const {moduleId} = this.props;
    if(moduleId){
      this.setState({
        moduleId: moduleId
      }, () => {
        this.loadData();
      });
    }
  }

  loadData(){
    let params = {};
    if(this.state.moduleId){
      params = {
        moduleId: this.state.moduleId
      }
    }

    API.get('admin/parsing-ref', {
      params: params
    }).then( result => {
      this.setState({
        data: result.data.data
      });
      this.setData((result.data.data));
    }).catch( e => console.log(e));
  }

  setData(data){
    let parsingRef = {};
    data.forEach( asset => {
      asset.data.forEach( el => {
        parsingRef[el.pointId] = el.order || ''
      })
    });

    this.setState({
      parsingRef: parsingRef
    });
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.moduleId){
      if (nextProps.moduleId !== this.props.moduleId) {
        this.setState({
          moduleId: nextProps.moduleId
        }, () => {
          this.loadData();
        });
      }
    }
  }

  onChangeValue(e) {
    this.setState({
      parsingRef: {
        ...this.state.parsingRef,
        [e.target.name]: e.currentTarget.value
      }
    })
  }

  onSave(e) {
    const {data, activeIndex, parsingRef} = this.state;
    let pointOrder = {};

    data[activeIndex].data.forEach(el => {
      if(parsingRef[el.pointId]){
        pointOrder[el.pointId] = parsingRef[el.pointId];
      }
    });

    let assetId = this.state.data[this.state.activeIndex].assetId;
    let payload = {
      moduleId: this.state.moduleId,
      pointOrder: pointOrder,
      assetId: assetId
    };

    API.post('admin/parsing-ref', payload)
      .then( result => {
        console.log(result);
      })
      .catch(e => {
        console.log(e);
      });
  }

  onTabChange(e){
    this.setState({activeIndex: e.index})
  }

  render() {
    const { data, parsingRef } = this.state;

    return (
      <div className="p-grid p-fluid">
        <Accordion activeIndex={this.state.activeIndex} onTabChange={this.onTabChange}>
        {data.map( (el,i) => {
          return <AccordionTab data-asset={el.assetId} header={el.assetName} toggleable={true} key={i} >
              {el.data.map((elem, k) => {
                return <div className="p-inputgroup" key={k}>
                  <span style={{width: "50%"}} className="p-inputgroup-addon">{ elem.pointId } - { elem.pointName }</span>
                  <InputText
                    name={elem.pointId}
                    style={{width: "auto"}}
                    placeholder="Urutan"
                    value={parsingRef[elem.pointId] || ""}
                    onChange={this.onChangeValue}
                    keyfilter="pint"
                  />
                </div>
              })}
            </AccordionTab>
        })}
        </Accordion>
        <div className="p-col">
          <Button className="p=button-info" label="Save" onClick={this.onSave} />
        </div>
      </div>
    )
  }
}