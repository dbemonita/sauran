import React from "react";
import {Dropdown} from 'primereact/dropdown';
import {InputText} from 'primereact/inputtext';


function Item(props) {
  var fileName = '';

  function onTypeChange(e) {

    props.onDone({
      fileName: fileName,
      visualType: e.value
    })
  }

  function onFileNameChange(e){
    fileName = e.currentTarget.value;
  }

  return (
    <div >
        <InputText placeholder="Name File" onChange={onFileNameChange} />
        <Dropdown value={1} options={props.visualType} onChange={onTypeChange} placeholder="Select Type" optionLabel="label"/>
    </div>
  );
}

const NewXml = React.memo(Item, (prevProps, nextProps) => {
  if (prevProps.fileName === nextProps.fileName) {
    return true;
  }
  return false;
});

export default NewXml;
