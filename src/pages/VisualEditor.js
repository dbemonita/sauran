import React, {Component} from 'react';
import XmlEditor from "../components/visual/editor";
import {VisualList} from "../components/visual/list";
// import API from '../_helper/api';

class VisualEditor extends Component {

  constructor(props) {
    super(props);
    this.state = {
      filePath: '',
    };

    this.onOpenFile = this.onOpenFile.bind(this);
  }

  onOpenFile(filePath){
    this.setState({
      filePath: filePath
    })
  }

  render() {
    return (
      <div  className="p-grid">
        <div className="p-md-3 p-sm-12">
          <VisualList onOpenFile={this.onOpenFile} />
        </div>
        <div className="p-md-9 p-sm-12">
          <XmlEditor
            filePath={this.state.filePath}
          />
        </div>
      </div>
    )
  }
}

export default VisualEditor;