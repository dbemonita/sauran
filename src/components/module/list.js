import React, {useEffect, useState} from 'react';
import API from '../../_helper/api';
import {Button} from "primereact/button";
import AddModule from './add';
import {OverlayPanel} from "primereact/overlaypanel";
import {Fieldset} from "primereact/fieldset";
import {Column} from "primereact/column";
import {DataTable} from "primereact/datatable";
import {InputText} from "primereact/inputtext";
import {ContextMenu} from "primereact/contextmenu";
import {Dialog} from 'primereact/dialog';
import EditModule from "./edit";

function fetchModule(){
  return API.get('admin/module');
}

const ModuleList = () => {

  const [moduleList, setModuleList] = useState([]);
  // const [moduleId, setModuleId] = useState('')
  const [moduleSelected, setModuleSelected] = useState({});
  const [visibleEdit, setVisibleEdit] = useState(false);
  const [globalFilter, setGlobalFilter] = useState('');
  // const [dt, setDt] = useState('');
  const [cm, setCm] = useState('');
  const [op, setOp] = useState('');

  const menu = [{
    "label": "edit",
    "icon": "fa fa-pencil",
    command: () => {
      setVisibleEdit(true)
    }
  },{
    "label": "delete",
    "icon": "fa fa-warning",
    command: () => {
      window.confirm("Delete Confirmation") && onDelete();
    }
  }]

  useEffect(() => {
    fetchModule()
    .then(data => setModuleList(data.data))

  }, []);

  const reload = () => {
    fetchModule()
    .then(data => setModuleList(data.data))
  }

  const onDelete = async () => {
    await API.delete('admin/module/' + moduleSelected.id)
    reload()
  };

  const header = <div style={{'textAlign':'left'}}>
      <i className="pi pi-search" style={{margin:'4px 4px 0 0'}}>
      </i>
      <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" size="50"/>
      <Button style={{marginLeft: 10}} icon="pi pi-plus" className="p-button-raised" onClick={(e) => op.toggle(e)}  />
    </div>;

  return (
    <div  className="p-grid">
      <OverlayPanel ref={(el) => setOp(el)}>
        <AddModule reload={reload} onClose={(e) => op.toggle(e)} />
      </OverlayPanel>

      <Dialog header="Module Edit" visible={visibleEdit} style={{width: '30vw'}} onHide={() => setVisibleEdit(false)} >
        <EditModule data={moduleSelected}
          done={ () => {
            setVisibleEdit(false);
            reload()
          }}/>
      </Dialog>

      <div className="p-col-12">
        <Fieldset legend="List Module " toggleable={true}>
          <ContextMenu model={menu} ref={el => setCm(el)} />
          <DataTable
            editable={true}
            header={header}
            globalFilter={globalFilter}
            emptyMessage="No records found"

            value={moduleList}
            contextMenuSelection={moduleSelected}
            onContextMenuSelectionChange={e => setModuleSelected(e.value)}
            onContextMenu={e => cm.show(e.originalEvent)}
          >
            <Column field="name" header="Name" />
            <Column field="serial_number" header="Serial Number" />
            <Column field="timeout" header="Timeout" />
            <Column field="type" header="Type" />
          </DataTable>
        </Fieldset>
      </div>
    </div>
  )

};

export default ModuleList;
