import socketCluster  from 'socketcluster-client';
console.log(socketCluster);
const ENDPOINT = (process.env.NODE_ENV === 'development') ? 'localhost' : process.env.REACT_APP_ENDPOINT_WS;
const PORT = (process.env.NODE_ENV === 'development') ? 5000 :process.env.REACT_APP_ENDPOINT_PORT;
// const token = localStorage.getItem('token');
const socket = socketCluster.create({
  hostname: ENDPOINT,
  port: PORT,
  autoConnect: true,
  secure: false,
  rejectUnauthorized: false,
  connectTimeout: 10000, //milliseconds
  ackTimeout: 10000, //milliseconds
  channelPrefix: null,
  disconnectOnUnload: true,
  multiplex: true
});

socket.on('unsubscribe', function(channelname) {
  console.log('unsubscribe:' + channelname);
});

//
// socket.on('subscribeStateChange', function(data) {
//   console.log('subscribeStateChange:' + JSON.stringify(data));
// });

// socket.on('message', function(data) {
//   console.log('message:' + data);
// });

export default socket;
