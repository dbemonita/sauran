import React from 'react';
import AppWrapper from './layout/AppWrapper';
import { StateProvider } from './appState';

import userReducer from './state/user';
import networkReducer from './state/network';
import responseReducer from './state/response';

const mainReducer = ({ user, network, message }, action) => ({
  user: userReducer(user, action),
  network: networkReducer(network, action),
  message: responseReducer(message, action)
});

const initialState = {
  user: JSON.parse(localStorage.getItem("user")) || {},
  network: {
    onApiRequest: false,
    ws: false,
    api: false
  }
};

const App = () => {
  return (
    <StateProvider initialState={initialState} reducer={mainReducer}>
      <AppWrapper />
    </StateProvider>
  );

};

export default App;