import React, {Component} from 'react';
import API from '../../_helper/api';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Button} from "primereact/button";
import {Dropdown} from "primereact/dropdown";
import {InputText} from "primereact/inputtext";

class VisualSelect extends Component {

  constructor(props){
    super(props);
    this.state = {
      user: {},
      visualListSelected: [],
      visualUser: [],
      visualList: [],
      visualListInit: [],
      visualTypeList: [],
      globalFilter: null
    };

    this.fetchVisualList = this.fetchVisualList.bind(this);
    this.fetchVisualUser = this.fetchVisualUser.bind(this);
    this.fetchVisualType = this.fetchVisualType.bind(this);
    this.onTypeEditorChange = this.onTypeEditorChange.bind(this);
    this.typeEditor = this.typeEditor.bind(this);
    this.onSave = this.onSave.bind(this);
    this.commonEditor = this.commonEditor.bind(this);
    this.inputTextEditor = this.inputTextEditor.bind(this);
    this.onEditorValueChange = this.onEditorValueChange.bind(this);
  }

  componentDidMount() {
    this.fetchVisualList(true);
    this.fetchVisualType();
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.user !== this.props.user){
      this.setState({
        user: nextProps.user,
        visualListSelected: []
      }, () => {
        this.fetchVisualUser();
      })
    }
  }

  fetchVisualUser(){
    API.get('admin/visual/user/' + this.state.user.userId)
      .then(response => {
        const {visualListInit, visualListSelected} = this.state;
        let newVisualList = [...visualListInit];
        let newVisualListSelected = (visualListSelected.length > 0) ? [...visualListSelected] : [];

        response.data.forEach(el => {
          let indx = visualListInit.findIndex(elem => elem.path === el.path);
          newVisualList[indx] = {
            ...visualListInit[indx],
            type: el.visualType,
            order: el.order,
            visualId: el.visualId,
            visualTypeId: el.visualTypeId
          };
          newVisualListSelected.push(newVisualList[indx]);
        });

        this.setState({
          visualUser: response.data,
          visualList: newVisualList,
          visualListSelected: newVisualListSelected
        });

      })
      .catch( err => {
        console.log(err);
      })
  }

  fetchVisualList(init = false){
    API.get('admin/visual/list?tree=false')
      .then(response => {
        if(init){
          this.setState({
            visualList: response.data,
            visualListInit: response.data
          });
        }else{
          this.setState({
            visualList: response.data
          });
        }
      })
      .catch( err => {
        console.log(err);
      })
  }

  fetchVisualType(){
    API.get('admin/visual-type')
      .then(response => {
        this.setState({
          visualTypeList: response.data
        });
      })
      .catch( err => {
        console.log(err);
      })
  }

  typeEditor(props){
    return (this.state.visualListSelected.includes(props.rowData)) && (<Dropdown
      value={props.value[props.rowIndex].visualTypeId} options={this.state.visualTypeList}
      onChange={(e) => this.onTypeEditorChange(props, e)} style={{width:'100%'}} placeholder="Select Type"/>)
  }

  onTypeEditorChange(props, e) {

    let visualListUpdated = [...props.value];
    let visualTypeId = e.value;
    let visualType = this.state.visualTypeList.filter(el => el.value === visualTypeId)[0];
    visualListUpdated[props.rowIndex][props.field] = visualType.label;
    visualListUpdated[props.rowIndex].visualTypeId = visualType.value;

    this.setState({visualList: visualListUpdated});
  }

  commonEditor(props){
    return this.inputTextEditor(props);
  }

  inputTextEditor(props) {
    const {field} = props;
    return (this.state.visualListSelected.includes(props.rowData)) ? (
      <InputText type="text" value={props.rowData[field]} onChange={(e) => this.onEditorValueChange(props, e.target.value)} />)
      : props.rowData[field];
  }

  onEditorValueChange(props, value){
    let visualListUpdated = [...props.value];
    visualListUpdated[props.rowIndex][props.field] = value;

    this.setState({visualList: visualListUpdated});
  }

  onSave(){
    const {visualListSelected, user, visualList} = this.state;

    let payload = {
      userId: user.userId,
      visual: visualListSelected
    };

    API.patch('admin/visual/user', payload)
      .then( response => {
        let updated = visualListSelected.filter(el => el.registered === false);
        let newVisualList = [...visualList];
        updated.forEach(elUpdated => {
          let idx = visualList.findIndex(el => el === elUpdated);
          newVisualList[idx].registered = true;
        });

        this.setState({
          visualList:  newVisualList
        });
      })
      .catch( error => {
        console.log(error);
      });
  }

  static registeredTemplate(rowData, column){
    return <div style={{textAlign: 'center'}}>
      <i className={(rowData.registered) ? 'pi pi-check' : 'pi pi-times'}> </i>
    </div>
  }

  render() {
    let header = (
      <div style={{'textAlign':'left'}}>
        <i className="pi pi-search" style={{margin:'4px 4px 0 0'}}></i>
        <InputText type="search" onInput={(e) => this.setState({globalFilter: e.target.value})} placeholder="Global Search" size="50"/>
        <Button
          label="Save"
          onClick={this.onSave}
          style={{
            float: 'right',
            display: (this.state.visualListSelected.length > 0) ? 'block': 'none'}}
        />
      </div>
    );

    return(
      <div>
        <span >Select XML Visual for user : {this.state.user.username || ''}</span>
        <DataTable
          scrollable={true}
          header={header}
          scrollHeight="70vh"
          globalFilter={this.state.globalFilter}
          style={{marginTop: 5}}
          value={this.state.visualList}
          selection={this.state.visualListSelected}
          onSelectionChange={e => this.setState({visualListSelected: e.value})}
        >
          <Column selectionMode="multiple" style={{width:'3em', display: (this.state.user.username) ? '' : 'none'}}/>
          <Column field="name" header="Name" editor={this.commonEditor}/>
          <Column field="path" header="Path" />
          <Column field="registered" header="Registered" body={VisualSelect.registeredTemplate} />
          <Column field="order" header="Urutan Menu" editor={this.commonEditor}/>
        </DataTable>
      </div>
    )
  }
}

export default VisualSelect;
