import React, { Component } from 'react';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import { Button } from 'primereact/button';
import API from '../../_helper/api';

export class AddModule extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        name: '',
        serialNumber: '',
        status: 1,
        timeout: '',
        type: ''
      },
      typeList: [{
        label: 'A',
        value: 'A'
      },{
        label: 'B',
        value: 'B'
      },{
        label: 'C',
        value: 'C'
      },{
        label: 'Modbus',
        value: 'Modbus'
      }]
    };

    this.onChangeValue = this.onChangeValue.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  async componentDidMount() {

  }

  onSave(e) {
    const { data } = this.state;

    API.post('admin/module/create', {
      name: data.name,
      serialNumber: data.serialNumber,
      type: data.type,
      status: data.status,
      timeout: data.timeout
    }).then(result => {
      console.log(result);
      this.props.reload();
      this.props.onClose();
    }).catch(err => {
      console.log(err);
    })
  }

  componentWillReceiveProps(nextProps) {

  }

  onChangeValue(e) {
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value
      }
    })
  }

  render() {
    const { data } = this.state;
    return (
      <div className="p-grid p-fluid">
        <div className="p-col-12 p-lg-12">
          <div className="card card-w-title">
            <h1>New Module</h1>
            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="name">Name</label>
                <InputText autoComplete="off" name="name" value={data.name} onChange={this.onChangeValue} autoFocus />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="serialNumber">Serial Number</label>
                <InputText autoComplete="off" name="serialNumber" value={data.serialNumber} onChange={this.onChangeValue} />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="type">Module Type</label>
                <Dropdown
                  name="type"
                  value={this.state.data.type}
                  options={this.state.typeList}
                  onChange={this.onChangeValue}
                  placeholder="Select Module Type"
                />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="status">Status</label>
                <InputText name="status" value={data.status} onChange={this.onChangeValue} />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="timeout">Timeout</label>
                <InputText name="timeout" value={data.timeout} onChange={this.onChangeValue} />
              </div>
            </div>

            <Button label="Save" className="p-button-raised" onClick={this.onSave} />
          </div>
        </div>
      </div>
    )
  }


}