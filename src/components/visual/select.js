import React, {useEffect, useState} from 'react';
import MenuAvailable from "./menu/available";
import MenuOrder from "./menu/order";
import API from '../../_helper/api';
import {buildTree} from '../../_helper/built-tree';

const VisualSelect = (props) => {

  const [visualTree, setVisualTree] = useState([]);
  const [visualChecked, setVisualChecked] = useState([]);

  useEffect(() => {
    try {
      const fetchData = async () => {
        let {data} = await API.get('admin/visual/list?tree=false');
  
        let tree = buildTree(data);
        setVisualTree(tree);
      
      };

      fetchData();
    } catch (error) {
      console.log(error);
    }
  }, []);

  useEffect(() => {
    if(props.user){
      try {
        const fetchData = async function(){
          const {data}  = await API.get('v2/admin/visual/checked/user/' + props.user.userId);
          setVisualChecked(data);
        };
  
        fetchData();
      } catch (error) {
        console.log(error);
      }
    }
  }, [props.user]);

  const onChangeVisualChecked = (data) => {
    console.log(data);
    setVisualChecked(data);
  };

  return (
    <div
      className="p-grid"
      style={{
        background: 'linear-gradient(to right, #e1e4e8, #edf0f5)',
        marginTop: 0
      }}>

      <div className="p-md-6 p-sm-12">
        <span>Visual Menu List</span>
        <MenuAvailable
          onChangeVisualChecked={onChangeVisualChecked}
          visualChecked={visualChecked}
          visualTree={visualTree}
        />
      </div>
      <div className="p-md-6 p-sm-12">
        <span>Visual Menu List Selected (drag to re-order)</span>
        <MenuOrder
          visualChecked={visualChecked}
          user={props.user}
        />
      </div>
    </div>
  )

};

export default VisualSelect;