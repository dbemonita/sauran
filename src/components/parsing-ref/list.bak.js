import React, { Component } from 'react';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {InputText} from 'primereact/inputtext';
import {MultiSelect} from 'primereact/multiselect';
import {Button} from "primereact/button";
import API from '../../_helper/api';
import ListModule from './list.module';
import {ContextMenu} from "primereact/contextmenu";
import {Fieldset} from "primereact/fieldset";

export class ListParsingRef extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataList: [],
      moduleList: [],
      assetList: [],
      asset: null,
      data: '',
      updates: [],
      inserts: [],
      clears: [],
      selectedData: null,
      menu: [{
        "label": "clear",
        "icon": "fa fa-code-fork",
        command: () => {
          window.confirm("Delete Confirmation") && this.onClear();
        }
      }]
    };

    this.onAssetFilter = this.onAssetFilter.bind(this);
    this.fetchParsingRef = this.fetchParsingRef.bind(this);
    this.fetchAsset = this.fetchAsset.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onOrderChange = this.onOrderChange.bind(this);
    this.onModuleChange = this.onModuleChange.bind(this);
    this.orderEditor = this.orderEditor.bind(this);
    this.moduleEditor = this.moduleEditor.bind(this);
    this.fetchModule = this.fetchModule.bind(this);
    this.onClear = this.onClear.bind(this);
  }

  componentDidMount() {
    this.fetchParsingRef();
    this.fetchAsset();
    this.fetchModule();
  }

  fetchParsingRef(){
    API.get('admin/parsing-ref').then((data) => {
      this.setState({
        dataList: data.data
      })
    })
  }

  fetchAsset() {
    API.get('admin/asset/crud')
      .then((data) => {
        if(data.data){
          let collect = data.data.map( el => {
            return {
              label: el.name,
              value: el.name
            }
          });
          this.setState({
            assetList: collect
          })
        }
      })
  }

  fetchModule() {
    API.get('admin/module')
      .then((data) => {
        let collect = data.data.map( el => {
          return {
            label: el.name,
            serial: el.serial_number,
            value: el.id
          }
        });
        this.setState({
          moduleList: collect
        });
      })
  }

  onSave(){
    const {clears, updates, inserts} = this.state;
    // debugger;
    API.post('admin/parsing-ref', {
      clears, updates, inserts
    }).then( response => {
      console.log(response);
    }).catch( err => {
      console.log(err);
    })
  }

  onClear(){
    const {selectedData, dataList} = this.state;

    let index = dataList.findIndex( el => {
      return (selectedData.pointId === el.pointId);
    });

    if(index > -1){
      const {clears, updates, inserts} = this.state;
      const updatedDataList = [...this.state.dataList];
      updatedDataList[index].order = '';
      updatedDataList[index].moduleName = '';
      updatedDataList[index].moduleSerialNumber = '';

      if(!clears.includes(selectedData.parsingRefId)){
        this.setState({
          clears: [
            ...clears,
            selectedData.parsingRefId
          ]
        });
      }

      let newUpdates = updates.filter( el => !el.parsingRefId === selectedData.parsingRefId);
      let newInserts = inserts.filter( el => !el.pointId === selectedData.pointId);

      this.setState({
        updates: newUpdates,
        inserts: newInserts,
        dataList: updatedDataList
      })
    }
  }

  orderEditor(props){
    return this.inputTextEditor(props, 'order');
  }

  moduleEditor(props) {
    return <ListModule props={{
      ...props,
      moduleList: this.state.moduleList,
      onModuleChange: this.onModuleChange
    }}/>
  }

  onModuleChange(props, e){
    const {value} = e;
    let updatedDataList = [...props.value];
    const {moduleList} = this.state;

    let moduleSelected = moduleList.filter(el => el.value === value)[0];

    if(!props.rowData.parsingRefId){
      const {inserts} = this.state;
      let index = inserts.findIndex( el => {
        return el.pointId.toString() === props.rowData.pointId.toString()
      });

      if(index < 0){
        this.setState({
          inserts: [
            ...inserts,
            {
              pointId: props.rowData.pointId,
              moduleId: moduleSelected.value
            }
          ]
        })
      }else {
        let updateData = [...inserts];
        updateData[index] = {
          ...updateData[index],
          moduleId: moduleSelected.value
        };

        this.setState({
          inserts: updateData
        })
      }

    }else{
      const {updates} = this.state;
      let index = updates.findIndex( el => {
        return el.parsingRefId === props.rowData.parsingRefId
      });

      if(index < 0){
        this.setState({
          updates: [
            ...updates,
            {
              parsingRefId: props.rowData.parsingRefId,
              moduleId: moduleSelected.value,
              pointId: props.rowData.pointId,
            }
          ]
        })
      }else{
        let updateData = [...updates];
        updateData[index] = {
          ...updateData[index],
          moduleId: moduleSelected.value
        };

        this.setState({
          updates: updateData
        })
      }
    }

    updatedDataList[props.rowIndex].moduleName = moduleSelected.label;
    updatedDataList[props.rowIndex].moduleId = moduleSelected.value;
    updatedDataList[props.rowIndex].moduleSerialNumber = moduleSelected.serial;

    this.setState({dataList: updatedDataList});
  }

  inputTextEditor(props, field) {
    return <InputText type="number" value={props.rowData.order} onChange={(e) => this.onOrderChange(props, e.target.value)} />;
  }
  onOrderChange(props, value) {
    if(!props.rowData.parsingRefId){
      const {inserts} = this.state;
      let index = inserts.findIndex( el => {
        return el.pointId.toString() === props.rowData.pointId.toString()
      });

      if(index < 0){
        this.setState({
          inserts: [
            ...inserts,
            {
              order: parseInt(value),
              pointId: props.rowData.pointId
            }
          ]
        })
      }else{
        let updateData = [...inserts];
        updateData[index] = {
          ...updateData[index],
          order: parseInt(value),
          pointId: props.rowData.pointId
        };

        this.setState({
          inserts: updateData
        })
      }
    }else{
      const {updates} = this.state;
      let index = updates.findIndex( el => {
        return el.parsingRefId === props.rowData.parsingRefId
      });

      if(index < 0){
        this.setState({
          updates: [
            ...updates,
            {
              order: parseInt(value),
              pointId: props.rowData.pointId,
              parsingRefId: props.rowData.parsingRefId
            }
          ]
        })
      }else{
        let updateData = [...updates];
        updateData[index] = {
          ...updateData[index],
          order: parseInt(value),
          pointId: props.rowData.pointId
        };

        this.setState({
          updates: updateData
        })
      }
    }

    let updatedDataList = [...props.value];
    updatedDataList[props.rowIndex][props.field] = value;
    this.setState({
      dataList: updatedDataList
    });
  }

  onAssetFilter(event) {
    this.dt.filter(event.value, 'assetName', 'in');
    this.setState({asset: event.value});
  }

  render() {
    let header = <div style={{'textAlign':'left'}}>
      <i className="pi pi-search" style={{margin:'4px 4px 0 0'}}>
      </i>
      <InputText type="search" onInput={(e) => this.setState({globalFilter: e.target.value})} placeholder="Global Search" size="50"/>
      <Button label="Simpan Perubahan" style={{float: "right"}} className="p-button-raised" onClick={this.onSave} />
    </div>;

    let assetFilter= <MultiSelect
      style={{width:'100%'}}
      value={this.state.asset}
      options={this.state.assetList}
      onChange={this.onAssetFilter}
    />;

    return (
      <Fieldset legend="Parsing Ref" toggleable={true}>
        <ContextMenu model={this.state.menu} ref={el => this.cm = el}
          // onHide={() => this.setState({selectedData: null}) }
        />
        <DataTable
          editable={true}
          header={header}
          ref={(el) => this.dt = el} value={this.state.dataList}
          globalFilter={this.state.globalFilter}
          emptyMessage="No records found"
          contextMenuSelection={this.state.selectedData}
          onContextMenuSelectionChange={e => this.setState({selectedData: e.value})}
          onContextMenu={e => this.cm.show(e.originalEvent)}>
          <Column field="assetName" header="Asset" filter={true} filterElement={assetFilter} />
          <Column field="moduleName" header="Module" editor={this.moduleEditor}/>
          <Column field="moduleSerialNumber" header="Serial" filter={true} />
          <Column field="order" header="Order" editor={this.orderEditor} />
          <Column field="pointId" header="ID Point" filter={true} />
          <Column field="pointName" header="Point Name" filter={true} />
        </DataTable>
      </Fieldset>
    )
  }
}
