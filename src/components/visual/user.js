import React, {Component} from 'react';
import API from '../../_helper/api';
import {ListBox} from 'primereact/listbox';

export class VisualUser extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user: null,
      userList: []
    };

    this.fetchUser = this.fetchUser.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.fetchUser();
  }

  componentWillReceiveProps(nextProps, nextContext) {

  }

  fetchUser() {
    API.get('admin/user')
      .then(result => {
        this.setState({
          userList: result.data
        })
      })
      .catch(err => {
        console.log(err);
      })
  }

  onChange(e) {
    const {user} = this.state;
    if (e.value) {
      if (user !== e.value) {
        this.setState({
          user: e.value
        }, () => {
          this.props.onUserSelected(e.value);
        });
      }
    }
  }

  render() {
    return (
      <div>
        <ListBox
          style={{
            width: '100%',
            marginTop: 5,
            maxHeight: (window.innerHeight - 138) + 'px'
          }}
          filter={true}
          value={this.state.user}
          options={this.state.userList}
          onChange={this.onChange}
          optionLabel="username"
        />
      </div>
    );
  }

}
