import React, {useEffect, useState} from 'react';
import {CompassIndex} from '../components/asset/index';
import EditAsset from '../components/asset/edit';
import {AddAsset} from '../components/asset/add';
import ListAsset from '../components/asset/list';

const AssetManagement = () => {

  const [childNode, setChildNode] = useState({});
  const [editPageType, setEditPageType] = useState(null);
  const [addPageType, setAddPageType] = useState(null);
  const [pageComponent, setPageComponent] = useState(<CompassIndex/>);

  const onSelect = ({addPageType, childNode, editPageType}) => {
    setAddPageType(addPageType);
    setChildNode(childNode);
    setEditPageType(editPageType);

  };

  const reloadListAsset = (childNode) => {
    setChildNode(childNode);
  };

  useEffect(() => {
    if (editPageType !== null) {
      if(editPageType === 'asset'){
        setPageComponent(<EditAsset reloadListAsset={reloadListAsset} node={childNode}/>)
      }else{
        setPageComponent(<CompassIndex/>);
      }
    }

    if (addPageType !== null) {
      if(addPageType === 'asset'){
        setPageComponent(<AddAsset reloadListAsset={reloadListAsset} node={childNode}/>)
      }else{
        setPageComponent(<CompassIndex/>);
      }
    }
  },[addPageType, childNode, editPageType]);

  return (
    <div className="p-grid">
      <div className="p-col-12 p-lg-3 p-md-3 p-sm-12">
        <ListAsset onSelect={onSelect} />
      </div>
      <div className="p-col-12 p-md-9 p-md-9 p-sm-12">
        {pageComponent}
      </div>
    </div>
  )
};

export default AssetManagement;
