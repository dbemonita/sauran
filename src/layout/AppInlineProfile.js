import React, { useState, memo, useEffect } from 'react';
import {useStateValue} from '../appState';
import classNames from 'classnames';

const AppInlineProfile = () => {

  const [expanded, setExpanded] = useState(false);
  const [userLogin, setUserLogin] = useState({});
  const [{user}] = useStateValue();
  
  useEffect(() => {
    setUserLogin(user);
  }, [user]);

  const onLogout = () => {
    console.log('logout');
  };

  return  (
    <div className="profile">
      {/* <div>
        <img src="assets/layout/images/profile.png" alt="" />
      </div> */}
      <button className="p-link profile-link" onClick={(event) => {
        setExpanded(!expanded);
        event.preventDefault();
      }}>
        <span className="username">{userLogin.username}</span>
        <i className="pi pi-fw pi-cog"/>
      </button>
      <ul className={classNames({'profile-expanded': expanded})}>
        <li><button className="p-link" onClick={onLogout}><i className="pi pi-fw pi-power-off"/><span>Logout</span></button></li>
      </ul>
    </div>
  );
};

// export default withContext(AppInlineProfile);
function areEqual(prev, next) {
  // return prev.user.username === next.user.username;
  return true;
}

export default memo(AppInlineProfile, areEqual);
