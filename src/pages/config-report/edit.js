import React, { useState, useEffect } from 'react';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { useParams } from 'react-router-dom'
import { ReactMultiEmail, isEmail } from 'react-multi-email';
import {Message} from 'primereact/message';
import api from '../../_helper/api';
import 'react-multi-email/style.css';
import {Card} from 'primereact/card';
import cronstrue from 'cronstrue';
import {Dropdown} from 'primereact/dropdown';

const formInit = {
  crontime: '* * * * *',
  subject: '',
  body: '',
  recipient: '',
  epaperId: '',
  configEmailId: ''
};
const Edit = () => {

  let { id } = useParams();
  const [emails, setEmails] = useState([]);
  const [emailSetup, setEmailSetup] = useState([]);
  const [emailSetupSelect, setEmailSetupSelect] = useState([]);
  const [epaper, setEpaper] = useState([]);
  const [msgError, setMsgError] = useState({});
  const [form, setForm] = useState(formInit);
  const [template, setTemplate] = useState({});

  const removeEmail = (index) => {
    let newEmail = [...emails];
    newEmail.splice(index, 1);
    setEmails(newEmail);
  };

  useEffect(() => {
    api.get('config-report/setup-email')
    .then(result => {
      const {data} = result;
      if(data.length){
        let _khd = data.map(function (el) {
          let _id = el.id;
          return {
            _id: _id,
            name: '('+el.id+') ' + el.host + ' - ' + el.user
          };
        });
        setEmailSetup(_khd);
      }
    })
    .catch(e => {
      console.log(e);
    });
  }, []);

  useEffect(() => {
    api.get('config-report/epaper')
    .then(result => {
      setEpaper(result.data);
    })
    .catch(e => {
      console.log(e);
    });
  }, []);

  useEffect(() => {
    api.get('config-report/find/'+id)
    .then(result => {
      const data = result.data[0];
      setForm({
        crontime: data.crontime,
        subject: data.subject,
        body: data.body,
        recipient: JSON.parse(data.recipient),
        epaperId: data.epaper_id,
        configEmailId: data.config_email_id
      });

      setEmails(JSON.parse(data.recipient));

      if(emailSetup.length > 0){
        let currentEmailSetup = emailSetup.filter(el => el._id === data.config_email_id);
        setEmailSetupSelect(currentEmailSetup[0]);
      }

      let currentEpaper = epaper.filter(el => el.id === data.epaper_id);

      setTemplate(currentEpaper[0]);
      
      // debugger;
    })
    .catch(e => {
      console.log(e);
    });

  }, [id, emailSetup, epaper]);

  const onChange = (e) => {
    const {value, name} = e.currentTarget;
    setForm({
      ...form,
      [name]: value
    });
  };

  const checkError = () => {
    let isError = false;
    let msg = {};
    for(let name in form){
      if(form.hasOwnProperty(name)){
        if(form[name] === ''){
          msg[name] = 'Perlu untuk diisi';
          isError = true;
        }
      }
    }

    setMsgError({
      ...msg
    });
    return isError;
  };

  const onProceed = (e) => {
    if(!checkError()){
      const {recipient} = form;
      
      api.patch('config-report/'+id, {
        ...form,
        recipient: (typeof recipient === 'string') ? form.recipient : JSON.stringify(form.recipient)
      })
      .then(result => {
        if(result.data.status){
          alert('Data saved');
        }
      })
      .catch(err => {
        console.log(err);
      });
    }
  };

  const footer = (
    <div style={{overflow: 'auto' }}>
      <Button
        type='button'
        icon="pi pi-check"
        style={{ float: 'right'}}
        label="Proceed"
        className="p-button-raised p-button-rounded" 
        onClick={onProceed}
      />
    </div>
  )

  return (

    <div className="p-grid p-justify-center" style={{ marginTop: '.5em' }}>
      <div className="p-col-4">
        <Card footer={footer}>
          <h4>Report Setup</h4>
          <p>Cron Time</p>
          <InputText 
            id='crontime'
            name='crontime'
            style={{ width: '100%' }}
            value={form.crontime}
            onChange={onChange}
            className={ msgError.crontime ? 'p-error' : ''}
          />
          {msgError.crontime && 
            <Message style={{marginTop: '0.5em'}} severity="error" text={msgError.crontime} />
          }

          <p>Setup Email</p>
          <Dropdown value={emailSetupSelect} 
            options={emailSetup} 
            onChange={(e) => {
              setEmailSetupSelect(e.value);
              setForm({
                ...form,
                configEmailId: e.value._id
              })
            }}
            optionLabel="name"
            className={ msgError.configEmailId ? 'p-error' : ''}
            style={{ width: '100%' }}
          />
          {msgError.configEmailId &&
            <Message style={{marginTop: '0.5em'}} severity="error" text={msgError.configEmailId} />
          }

          <p>Subject</p>
          <InputText 
            id="subject" 
            name="subject" 
            style={{ width: '100%' }} 
            value={form.subject}
            onChange={onChange}
            className={ msgError.subject ? 'p-error' : ''}
          />
          {msgError.subject && 
            <Message style={{marginTop: '0.5em'}} severity="error" text={msgError.subject} />
          }

          <p>Body (html)</p>
          <InputTextarea 
            id="body" 
            name="body" 
            style={{ width: '100%' }} 
            value={form.body}
            onChange={onChange}
            className={ msgError.body ? 'p-error' : ''}
          />
          {msgError.body && 
            <Message style={{marginTop: '0.5em'}} severity="error" text={msgError.body} />
          }

          <p>Recipient</p>
          <ReactMultiEmail
            style={{ width: '100%', border: msgError.recipient ? '1px solid red' : '1px solid #a6a6a6' }}
            id="recipient"
            placeholder="email"
            emails={emails}
            onChange={(email) => {
              setEmails(email);
              setForm({
                ...form,
                recipient: JSON.stringify(email)
              })
            }}
            validateEmail={email => {
              return isEmail(email);
            }}
            getLabel={(email, index) => {
              return (
                <div data-tag key={index}>
                  {email}
                  <span data-tag-handle onClick={() => removeEmail(index)}>
                    ×
                    </span>
                </div>
              );
            }}
          />
          {msgError.recipient && 
            <Message style={{marginTop: '0.5em'}} severity="error" text={msgError.recipient} />
          }

          <p>Template</p>
          <Dropdown value={template} 
            options={epaper}
            dataKey='id'
            onChange={(e) => {
              setTemplate(e.value);
              setForm({
                ...form,
                epaperId: e.value.id
              })
            }}
            optionLabel="name"
            className={ msgError.epaperId ? 'p-error' : ''}
            style={{ width: '100%' }}
          />
          {msgError.epaperId &&
            <Message style={{marginTop: '0.5em'}} severity="error" text={msgError.epaperId} />
          }
        </Card>
      </div>

      <div className="p-col-4" >
        <h4>Info</h4>
          {cronstrue.toString(form.crontime, {throwExceptionOnParseError: false})}
      </div>
    </div>
  )

};

export default Edit;