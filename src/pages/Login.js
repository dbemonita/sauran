import React, {useState, useEffect} from 'react';
import {withRouter} from 'react-router-dom';
import {InputText} from 'primereact/inputtext';
import {Button} from 'primereact/button';
import {Message} from 'primereact/message';
import {useStateValue} from "../appState";
import API from "../_helper/api";

const Login = (props) => {
  console.log('[Component : Login]');
  const [credential, setCredential] = useState({
    username: '',
    password: ''
  });
  const [errorMessage, setErrorMessage] = useState('');
  const [error, setError] = useState(false);
  const [{user}, dispatch] = useStateValue();

  useEffect(() => {
    if(user.username){
      // props.history.push('/');
      console.log('reload');
    }
  }, [user]);

  const onChangeValue = (e) => {
    setCredential({
      ...credential,
      [e.target.name]: e.currentTarget.value
    })
  };
  const onLogin = (e) => {
    e.preventDefault();
    API.post("admin/auth/login", credential)
    .then( (response) => {
      if(response){
        console.log(response.data);
        if(response.data.success){
          const {token, user} = response.data;
          localStorage.setItem("token", token);
          localStorage.setItem("user", JSON.stringify(user));
          dispatch({
            type: 'setUser',
            payload: user
          });
          // props.history.push('/');
          window.location.reload();
        }
      }
    }).catch( e => {
      let resp = e.response;

      setErrorMessage(resp.data.message);
      setError(true);
    })
  };

  return (
    <div className="p-grid p-fluid" style={{paddingTop: 60}}>
      <div className="p-col-4 p-offset-4">
        <div className="card card-w-title">
          <h1>Login</h1>

          {(error) && (
            <Message severity="error" text={errorMessage} />
          )}

          <form >
            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="name">Username</label>
                <InputText name="username" value={credential.username} onChange={onChangeValue} autoFocus/>
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="name">Password</label>
                <InputText name="password" type="password" value={credential.password} onChange={onChangeValue} />
              </div>
            </div>
            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <Button type="submit" label="Login" className="p-button-raised" onClick={onLogin}/>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
};

export default withRouter(Login);