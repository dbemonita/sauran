import React, { useState, useEffect, memo } from 'react';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {InputText} from 'primereact/inputtext';
import {MultiSelect} from 'primereact/multiselect';
import {Button} from "primereact/button";
import API from '../../_helper/api';
import ListModule from './list.module';
import {ContextMenu} from "primereact/contextmenu";
import {Fieldset} from "primereact/fieldset";

let dt = '';
let cm;
const ListParsingRef = ({props}) => {

  const [dataList, setDataList] = useState([]);
  const [moduleList, setModuleList] = useState([]);
  const [assetList, setAssetList] = useState([]);
  const [asset, setAsset] = useState(null);
  const [updates, setUpdates] = useState([]);
  const [inserts, setInserts] = useState([]);
  const [clears, setClears] = useState([]);
  const [selectedData, setSelectedData] = useState(null);
  const [globalFilter, setGlobalFilter] = useState("");
  const menu = [{
    "label": "clear",
    "icon": "fa fa-code-fork",
    command: () => {
      window.confirm("Delete Confirmation") && onClear();
    }
  }];

  useEffect(() => {
    fetchParsingRef();
    fetchAsset();
    fetchModule();
  }, []);

  const fetchParsingRef = () => {
    API.get('admin/parsing-ref').then((data) => {
      setDataList(data.data)
    })
  };

  const fetchAsset = () => {
    API.get('admin/asset/crud')
      .then((data) => {
        if(data.data){
          let collect = data.data.map( el => {
            return {
              label: el.name,
              value: el.name
            }
          });

          setAssetList(collect)
        }
      })
  }

  const fetchModule = () => {
    API.get('admin/module')
      .then((data) => {
        let collect = data.data.map( el => {
          return {
            label: el.name,
            serial: el.serial_number,
            value: el.id
          }
        });
        setModuleList(collect)
      })
  }

  const onSave = () => {
    API.post('admin/parsing-ref', {
      clears, updates, inserts
    }).then( response => {
      console.log(response);
    }).catch( err => {
      console.log(err);
    })
  }

  const onClear = () => {

    let index = dataList.findIndex( el => {
      return (selectedData.pointId === el.pointId);
    });

    if(index > -1){
      const updatedDataList = [...dataList];
      updatedDataList[index].order = '';
      updatedDataList[index].moduleName = '';
      updatedDataList[index].moduleSerialNumber = '';

      if(!clears.includes(selectedData.parsingRefId)){
        // this.setState({
        //   clears: [
        //     ...clears,
        //     selectedData.parsingRefId
        //   ]
        // });

        setClears([...clears, selectedData.parsingRefId])
      }

      let newUpdates = updates.filter( el => !el.parsingRefId === selectedData.parsingRefId);
      let newInserts = inserts.filter( el => !el.pointId === selectedData.pointId);

      setUpdates(newUpdates);
      setInserts(newInserts);
      setDataList(updatedDataList);
    }
  }

  const orderEditor = (props) => {
    return inputTextEditor(props, 'order');
  }

  const moduleEditor = (props) => {
    return <ListModule props={{
      ...props,
      moduleList,
      onModuleChange
    }}/>
  }

  const onModuleChange = (props, e) => {
    const {value} = e;
    let updatedDataList = [...props.value];

    let moduleSelected = moduleList.filter(el => el.value === value)[0];

    if(!props.rowData.parsingRefId){

      let index = inserts.findIndex( el => {
        return el.pointId.toString() === props.rowData.pointId.toString()
      });

      if(index < 0){
        // this.setState({
        //   inserts: [
        //     ...inserts,
        //     {
        //       pointId: props.rowData.pointId,
        //       moduleId: moduleSelected.value
        //     }
        //   ]
        // })

        setInserts([
          ...inserts,
          {
            pointId: props.rowData.pointId,
            moduleId: moduleSelected.value
          }
        ])

      }else {
        let updateData = [...inserts];
        updateData[index] = {
          ...updateData[index],
          moduleId: moduleSelected.value
        };

        setInserts(updateData);
      }

    }else{
      let index = updates.findIndex( el => {
        return el.parsingRefId === props.rowData.parsingRefId
      });

      if(index < 0){
        // this.setState({
        //   updates: [
        //     ...updates,
        //     {
        //       parsingRefId: props.rowData.parsingRefId,
        //       moduleId: moduleSelected.value,
        //       pointId: props.rowData.pointId,
        //     }
        //   ]
        // })

        setUpdates([
          ...updates,
          {
            parsingRefId: props.rowData.parsingRefId,
            moduleId: moduleSelected.value,
            pointId: props.rowData.pointId,
          }
        ])
      }else{
        let updateData = [...updates];
        updateData[index] = {
          ...updateData[index],
          moduleId: moduleSelected.value
        };

        setUpdates(updateData)
      }
    }

    updatedDataList[props.rowIndex].moduleName = moduleSelected.label;
    updatedDataList[props.rowIndex].moduleId = moduleSelected.value;
    updatedDataList[props.rowIndex].moduleSerialNumber = moduleSelected.serial;

    setDataList(updatedDataList);
  }

  const inputTextEditor = (props, field) => {
    return <InputText type="number" value={props.rowData.order} onChange={(e) => onOrderChange(props, e.target.value)} />;
  }

  const onOrderChange = (props, value) => {
    if(!props.rowData.parsingRefId){
      let index = inserts.findIndex( el => {
        return el.pointId.toString() === props.rowData.pointId.toString()
      });

      if(index < 0){
        setInserts([
          ...inserts,
          {
            order: parseInt(value),
            pointId: props.rowData.pointId
          }
        ])
      }else{
        let updateData = [...inserts];
        updateData[index] = {
          ...updateData[index],
          order: parseInt(value),
          pointId: props.rowData.pointId
        };

        setInserts(updateData)
      }
    }else{
      let index = updates.findIndex( el => {
        return el.parsingRefId === props.rowData.parsingRefId
      });

      if(index < 0){
        setUpdates([
          ...updates,
          {
            order: parseInt(value),
            pointId: props.rowData.pointId,
            parsingRefId: props.rowData.parsingRefId
          }
        ])
      }else{
        let updateData = [...updates];
        updateData[index] = {
          ...updateData[index],
          order: parseInt(value),
          pointId: props.rowData.pointId
        };

        setUpdates(updateData)
      }
    }

    let updatedDataList = [...props.value];
    updatedDataList[props.rowIndex][props.field] = value;
    setDataList(updatedDataList)
  }

  const onAssetFilter = (event) => {
    dt.filter(event.value, 'assetName', 'in');
    setAsset(event.value)
  }

  let header =
  <div style={{'textAlign':'left'}}>
    <i className="pi pi-search" style={{margin:'4px 4px 0 0'}}>
    </i>
    <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" size="50"/>
    <Button label="Simpan Perubahan" style={{float: "right"}} className="p-button-raised" onClick={onSave} />
  </div>;

  let assetFilter= <MultiSelect
    style={{width:'100%'}}
    value={asset}
    options={assetList}
    onChange={onAssetFilter}
  />;

  return (
    <Fieldset legend="Parsing Ref" toggleable={true}>
      <ContextMenu model={menu} ref={el => cm = el}
        // onHide={() => this.setState({selectedData: null}) }
      />
      <DataTable
        editable={true}
        header={header}
        ref={(el) => dt = el} value={dataList}
        globalFilter={globalFilter}
        emptyMessage="No records found"
        contextMenuSelection={selectedData}
        onContextMenuSelectionChange={e => setSelectedData(e.value)}
        onContextMenu={e => cm.show(e.originalEvent)}>
        <Column field="assetName" header="Asset" filter={true} filterElement={assetFilter} />
        <Column field="moduleName" header="Module" editor={moduleEditor}/>
        <Column field="moduleSerialNumber" header="Serial" filter={true} />
        <Column field="order" header="Order" editor={orderEditor} />
        <Column field="pointId" header="ID Point" filter={true} />
        <Column field="pointName" header="Point Name" filter={true} />
      </DataTable>
    </Fieldset>
  )

};

export default memo(ListParsingRef);
