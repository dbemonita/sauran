import axios from 'axios';
const ENDPOINT = (process.env.NODE_ENV === 'development') ? 'http://localhost' : process.env.REACT_APP_ENDPOINT_API;
const PORT = (process.env.NODE_ENV === 'development') ? 5000 :process.env.REACT_APP_ENDPOINT_PORT;

// const ENDPOINT = (process.env.NODE_ENV === 'development') ? 'http://10.1.62.96' : process.env.REACT_APP_ENDPOINT_API;
// const PORT = (process.env.NODE_ENV === 'development') ? 5000 :process.env.REACT_APP_ENDPOINT_PORT

const api =  axios.create({
  baseURL:ENDPOINT + ':' + PORT,
  timeout: 10000,
  withCredentials: false,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  }
});

api.interceptors.request.use(function (config) {
    let tokenStorage = localStorage.getItem('token');
    config.headers.common['Authorization'] = 'Bearer ' + tokenStorage;

    return config;
  }, function (error) {
    return Promise.reject(error);
});

export default api;
