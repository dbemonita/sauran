import React, {useState, useEffect} from 'react';
import API from '../../_helper/api';
import {DataTable} from 'primereact/components/datatable/DataTable';
import {Column} from "primereact/column";
import {Dropdown} from "primereact/dropdown";
import {InputText} from "primereact/inputtext";
import {Button} from "primereact/button";
import AddUser from "./add";
import {Dialog} from "primereact/dialog";
import {ContextMenu} from "primereact/contextmenu";
import { IntlProvider, FormattedRelativeTime } from "react-intl";
import {useStateValue} from "../../appState";

let cm;
const UserList = props => {

  const [userList, setUserList] = useState([]);
  const [roleList, setRoleList] = useState([]);
  const [isShowButtonUpdate, setIsShowButtonUpdate] = useState(false);
  const [addDialogVisible, setAddDialogVisible] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);
  const [updated, setUpdated] = useState({});
  const [{user}] = useStateValue();

  const menu = [{
    "label": "Delete",
    "icon": "fa fa-recycle",
    command: () => {
      window.confirm("Delete Confirmation") && onDelete();
    }
  }];

  useEffect(() => {
    API.get('admin/user')
      .then( result => {
        setUserList(result.data);
      })
      .catch( err => {
        console.log(err);
      });

    API.get('admin/role')
      .then( result => {
        setRoleList(result.data);
      })
      .catch( err => {
        console.log(err);
      })
  }, []);

  const roleEditor = (props) => {
    return (props.user.roleId === 0) ? <Dropdown
      value={props.value[props.rowIndex].role_id} options={roleList}
      onChange={(e) => onRoleEditorChange(props, e)} style={{width:'100%'}} placeholder="Select Type"/> : props.rowData.role;
  };

  const onRoleEditorChange = (props, e)  => {
    const {value, originalEvent} = e;
    const label = originalEvent.currentTarget.textContent;

    let updated = [...props.value];

    updated[props.rowIndex].userId = this.state.userList[props.rowIndex].userId;
    updated[props.rowIndex][props.field] = label;
    updated[props.rowIndex].role_id = value;

    trackUpdated({...props, field: 'role_id'});
  };

  const trackUpdated = (props) => {
    const {rowIndex, field} = props;
    const {updated} = this.state;

    let updating;
    if (updated[rowIndex]) {
      if (!updated[rowIndex].includes(field)) {
        updating = [...updated[rowIndex], field]
      } else {
        updating = updated[rowIndex];
      }
    } else {
      updating = [field];
    }

    setUpdated({...updated, [rowIndex]: updating});

    if (Object.keys(updated).length > 0) {
      setIsShowButtonUpdate(true);
    }

  };

  const inputTextEditor = (props) => {
    const {field} = props;
    return <InputText type="text" value={props.rowData[field]} onChange={(e) => onEditorValueChange(props, e.target.value)} />;
  };

  const onEditorValueChange = (props, value) => {
    let updated = [...props.value];

    updated[props.rowIndex].userId = userList[props.rowIndex].userId;
    updated[props.rowIndex][props.field] = value;
    setUserList(updated);
    trackUpdated(props);
  }

  const commonEditor = (props) => {
    return inputTextEditor(props);
  }

  const onHideAddDialog = () => {
    setAddDialogVisible(false);
  };

  const onSaveChange = () => {
    let payloads = [];

    setIsShowButtonUpdate(false);

    for (let idx in updated){
      if(updated.hasOwnProperty(idx)){
        let obj = {};
        let source = userList[idx];

        updated[idx].forEach(el => {
          obj.user_id= source.userId;
          obj[el] = source[el];
        });

        payloads.push(obj);
      }
    }

    API.patch('admin/user/1', payloads).then(result => {
      fetchUser();
    }).catch(err => {
      fetchUser();
    })
  };

  const onAddUser = () => {
    setAddDialogVisible(true);
  };

  const onDelete = () => {
    API.delete('admin/user/' + selectedUser.userId)
      .then(() => {
        fetchUser();
      })
  };

  const fetchUser = () => {
    API.get('admin/user')
      .then( result => {
        setUserList(result.data);
      })
      .catch( err => {
        console.log(err);
      });
  };

  const lastLoginTempl = (rowData, column) => {
    return <FormattedRelativeTime
      numeric="auto"
      unit="second"
      updateIntervalInSeconds={60}
      value={ (rowData.last_login) - (Math.floor(new Date().getTime() / 1000)) } />
  };

  const header = <div className="p-clearfix" style={{'lineHeight':'1.87em'}}> User
    <Button icon="pi pi-plus" style={{'float':'right'}} onClick={onAddUser}/>
  </div>;

  const footer = <div className="p-clearfix" style={{
    lineHeight:'1.87em',
    display: (isShowButtonUpdate) ? "block" : "none"
  }}>
    <Button icon="pi pi-save" style={{'float':'right'}} onClick={onSaveChange} label="Update"/>
  </div>;

  return (
      <div  className="p-grid">
        <div className="p-col-12 ">

          <ContextMenu model={menu} ref={el => cm = el} onHide={() => setSelectedUser(null)}/>
          <Dialog header="New User" visible={addDialogVisible} onHide={onHideAddDialog}>
            <AddUser hide={onHideAddDialog} roleList={roleList} reload={fetchUser}/>
          </Dialog>
          <IntlProvider locale="id-ID">
            <DataTable
              value={userList}
              editable={true}
              header={header}
              footer={footer}
              contextMenuSelection={selectedUser}
              onContextMenuSelectionChange={e => setSelectedUser(e.value)}
              onContextMenu={e => cm.show(e.originalEvent)}
            >
              <Column field="username" header="Username" editor={commonEditor}  rowSpan={2}/>
              <Column field="role" header="Role" editor={roleEditor}/>
              { user.roleId === 0 && (
                <Column field="password" header="New Password" editor={commonEditor}/>
              )}
              <Column field="email" header="Email" editor={commonEditor} rowSpan={2}/>
              <Column field="last_login" body={lastLoginTempl} header="Last Login"  rowSpan={2}/>
            </DataTable>
          </IntlProvider>
        </div>
      </div>
    )

};

export default UserList;