import React, { useState, useEffect } from 'react';
import {Fieldset} from 'primereact/fieldset';
import {subscriber, emitter} from "../../_helper/subscriber";
import {InputTextarea} from 'primereact/inputtextarea';

const LogModule = (props) => {
  const [moduleSelected, setModuleSelected] = useState({});
  const [moduleId, setModuleId] = useState('');
  const [moduleRequest, setModuleRequest] = useState('');
  const [moduleRequests, setModuleRequests] = useState([]);

  useEffect(() => {
    function requestStringify(){
      if(moduleRequests.length > 0){
        setModuleRequest(moduleRequests.toString().replace(/,/g, "\n"));
      }
    }

    function onWsMessage(data) {

      if(moduleRequests.length >= 10){
        setModuleRequests(data, ...moduleRequests.slice(0,9));
      }else{
        setModuleRequests(data, ...moduleRequests);
      }

      requestStringify();
    }

    function subscribeLog(){
      const accessRequest = subscriber.subscribe('moduleRequest:' + props.moduleSelected.serial_number);

      accessRequest.watch(onWsMessage);

      emitter.emit('getModuleRequest', props.moduleSelected.serial_number, (err, data) => {
        if(err){
          console.log(err);
        }else{
          console.log('===========', data);
          setModuleRequests(data);
          setModuleRequest(data.toString().replace(/,/g, "\n"))
        }
      });
      setModuleSelected(props.moduleSelected);
    }

    subscribeLog();

    return function cleaning(){
      let channel = subscriber.channel('moduleRequest:' + moduleSelected.serial_number);
      if(channel){
        channel.unwatch(onWsMessage);
        subscriber.unsubscribe('moduleRequest:' + moduleSelected.serial_number);
      }
    }
  }, [props.moduleId, props.moduleSelected])

  return (
    <Fieldset legend="Log Pengiriman" toggleable={true}>
      <InputTextarea
        rows={5}
        style={{width: '100%'}}
        value={moduleRequest}
        onChange={(e) => setModuleRequest(e.target.value)}
        autoResize={true} />
    </Fieldset>
  )
};

export default LogModule;

// export class LogModule extends Component{

//   constructor(props){
//     super(props);
//     this.state = {
//       moduleSelected: {},
//       moduleId: '',
//       moduleRequest: "",
//       moduleRequests: [],
//     };

//     this.subscribeLog = this.subscribeLog.bind(this);
//     this.onWsMessage = this.onWsMessage.bind(this);
//   }

//   componentDidMount() {
//     if(this.state.moduleId){
//       this.subscribeLog();
//     }
//   }

//   componentWillUnmount() {
//     const {moduleSelected} = this.state;
//     let channel = subscriber.channel('moduleRequest:' + moduleSelected.serial_number);
//     if(channel){
//       channel.unwatch(this.onWsMessage);
//       subscriber.unsubscribe('moduleRequest:' + moduleSelected.serial_number);
//     }
//   }

//   subscribeLog(){
//     const {moduleSelected} = this.state;
//     const accessRequest = subscriber.subscribe('moduleRequest:' + moduleSelected.serial_number);

//     accessRequest.watch(this.onWsMessage);

//     emitter.emit('getModuleRequest', moduleSelected.serial_number, (err, data) => {
//       if(err){
//         console.log(err);
//       }else{
//         this.setState({
//           moduleRequests: data,
//           moduleRequest: data.toString().replace(/,/g, "\n")
//         })
//       }
//     });
//   }

//   onWsMessage(data){
//     const {moduleRequests} = this.state;

//     if(moduleRequests.length >= 10){
//       this.setState({
//         moduleRequests: [data, ...moduleRequests.slice(0,9)]
//       })
//     }else{
//       this.setState( {
//         moduleRequests: [data, ...moduleRequests]
//       });
//     }

//     this.requestStringify();
//   }

//   requestStringify(){
//     const { moduleRequests } = this.state;

//     if(moduleRequests.length > 0){
//       this.setState({
//         moduleRequest: moduleRequests.toString().replace(/,/g, "\n")
//       });
//     }
//   }

//   componentWillReceiveProps(nextProps) {
//     if (nextProps.moduleId !== this.props.moduleId) {
//       this.setState({
//         moduleId: nextProps.moduleId,
//         moduleSelected: nextProps.moduleSelected
//       }, () => {
//         this.subscribeLog();
//       })
//     }
//   }

//   render() {
//     return (
//       <Fieldset legend="Log Pengiriman" toggleable={true}>
//         <InputTextarea
//           rows={5}
//           style={{width: '100%'}}
//           value={this.state.moduleRequest}
//           onChange={(e) => this.setState({moduleRequest: e.target.value})}
//           autoResize={true} />
//       </Fieldset>
//     )
//   }
// }
