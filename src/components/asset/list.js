import React, { useState, useEffect, memo } from 'react';
import {Tree} from 'primereact/tree'
import {ContextMenu} from 'primereact/contextmenu';
import API from '../../_helper/api';
import {Button} from 'primereact/button';
import {Card} from 'primereact/card';

const ListAsset = ((props) => {
  const [nodes, setNodes] = useState([]);
  const [expandedKeys, setExpandedKeys] = useState({});
  const [selectedNodeKey, setSelectedNodeKey] = useState(null);
  const menu = [{
    "label": "Tambah Sub",
    "icon": "fa fa-code-fork",
    command: () => {
      onContextMenuAddSelected('asset');
    }
  }, {
    "label": "Delete",
    "icon": "fa fa-remove",
    command: () => {
      window.confirm("Delete Confirmation") && onContextMenuDeleteSelected('asset')
    }
  }];
  let cm;

  useEffect(() => {
    function loadAsset(){
      API.get('admin/asset/tree')
        .then(response => {
          setNodes(response.data);
        }).catch(err => {
          console.log(err);
      })
    }

    loadAsset();
  }, []);

  const onContextMenuAddSelected = ((page) => {

    let node = getCurrentNode();

    props.onSelect({
      addPageType: page,
      editPageType: null,
      childNode: node
    })
  });

  const onContextMenuDeleteSelected = () => {
    let node = getCurrentNode();

    function loadAsset(){
      API.get('admin/asset/tree')
        .then(response => {
          setNodes(response.data);
        })
        .catch(err => console.log(err));
    }

    API.delete('admin/asset/crud/' + node.id, {
      data: {
        id: node.id
      }
    })
      .then(() => loadAsset())
      .catch(error => {
        console.log(error);
      });
  };

  const getCurrentNode = () => {
    let node = nodes;

    if (selectedNodeKey.toString().indexOf('-') > 0) {
      let keys = selectedNodeKey.split('-');
      keys.forEach((el, i) => {
        let current;
        if (i === 0) {
          current = node[el];
          node = current;
        } else {
          if (node.children[el]) {
            current = node.children[el];
            node = current
          }
        }
      });
    } else {
      node = nodes[selectedNodeKey];
    }

    return node;
  };

  const reload = () => {
    function loadAsset(){
      API.get('admin/asset/tree')
        .then( response => {
          setNodes(response.data);
        })
        .catch( err => {
          console.log(err);
        })

    }

    loadAsset();
  };

  const addAsset = () => {
    props.onSelect({
      addPageType: 'asset',
      selectedNodeKey: '',
      childNode: {
        data: ''
      }
    })
  };

  const header = (
    <div style={{
      background: '#edf0f5',
      border: '1px solid #c8c8c8', padding: 10, overflow: 'auto',
    }}>
      <span style={{paddingLeft: 10, fontWeight: 'bold', verticalAlign: '-webkit-baseline-middle'}}>Asset</span>
      <Button
        style={{float: 'right'}}
        icon="pi pi-plus"
        className="p-button-raised p-button-rounded"
        onClick={addAsset}
      />
      <Button
        style={{float: 'right', marginRight: 10}}
        icon="fa fa-undo"
        className="p-button-raised p-button-rounded"
        onClick={reload}
      />
    </div>
  );

  return (
    <Card header={header}>
      { nodes === null && (
        <span>API koneksi error</span>
      )}
      <ContextMenu
        model={menu}
        ref={el => cm = el}
      />
      <Tree
        value={nodes}
        selectionMode="single"
        expandedKeys={expandedKeys}
        selectionKeys={selectedNodeKey}
        onSelectionChange={e => {
          setSelectedNodeKey(e.value)
        }}
        onToggle={e => {
          console.log(e);
          setExpandedKeys(e.value)
        }}
        onContextMenuSelectionChange={event => setSelectedNodeKey(event.value)}
        onContextMenu={e => cm.show(e.originalEvent)}
        onSelect={ event => {
          // setSelectedNodeKey(event.node.key);
          props.onSelect({
            editPageType: event.node.data.role,
            addPageType: null,
            childNode: event.node
          });
        }}
        style={{
          border: 'none',
          padding: 0,
          overflow: 'auto',
          height: (window.innerHeight - 166) + 'px',
          maxHeight: (window.innerHeight - 166) + 'px'
        }}
      />
    </Card>
  )

});

export default memo(ListAsset, (prev, next) => {

});
