import React, {Component} from 'react';
import API from '../../_helper/api';
import {Button} from "primereact/button";
import {AddModule} from './add';
import {OverlayPanel} from "primereact/overlaypanel";
import {Fieldset} from "primereact/fieldset";
import {Column} from "primereact/column";
import {DataTable} from "primereact/datatable";
import {InputText} from "primereact/inputtext";
import {ContextMenu} from "primereact/contextmenu";
import {Dialog} from 'primereact/dialog';
import {EditModule} from "./edit";
import {withContext} from "../../AppContext";

class ModuleList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      moduleList: [],
      moduleId: '',
      visibleEdit: false,
      moduleSelected: {},
      menu: [{
        "label": "edit",
        "icon": "fa fa-pencil",
        command: () => {
          this.onEdit();
        }
      },{
        "label": "delete",
        "icon": "fa fa-warning",
        command: () => {
          window.confirm("Delete Confirmation") && this.onDelete();
        }
      }]
    };

    this.onSelectModule = this.onSelectModule.bind(this);
    this.onCloseAddModule = this.onCloseAddModule.bind(this);
    this.fetchModule = this.fetchModule.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onEdited = this.onEdited.bind(this);
    this.onHide = this.onHide.bind(this);
  }

  componentDidMount() {
    this.fetchModule();
  }

  fetchModule() {
    API.get('admin/module')
      .then((data) => {
        this.setState({
          moduleList: data.data
        });
      })
      .catch( err => {
        this.props.handlingRequestError(err);
      });
  }

  onCloseAddModule(e){
    console.log(e);
    this.op.toggle(e);
    this.fetchModule();
  }

  onSelectModule(e) {
    this.setState({
      moduleSelected: e.value,
      moduleId: e.value.id
    })
  }

  onDelete(){
    API.delete('admin/module/' + this.state.moduleSelected.id)
      .then((data) => {
        this.fetchModule();
      })
  }

  onEdit(){
    this.setState({
      visibleEdit: true
    });
  }

  onEdited(){
    this.onHide();
    this.fetchModule();
  }

  onHide() {
    this.setState({visibleEdit: false});
  }

  render() {

    const {network} = this.props;

    let header = <div style={{'textAlign':'left'}}>
      <i className="pi pi-search" style={{margin:'4px 4px 0 0'}}>
      </i>
      <InputText type="search" onInput={(e) => this.setState({globalFilter: e.target.value})} placeholder="Global Search" size="50"/>
      <Button disabled={!network.api} style={{marginLeft: 10}} icon="pi pi-plus" className="p-button-raised" onClick={(e) => this.op.toggle(e)}  />
    </div>;

    return (
      <div  className="p-grid">
        <OverlayPanel ref={(el) => this.op = el}>
          <AddModule reload={this.fetchModule} onClose={(e) => this.op.toggle(e)} />
        </OverlayPanel>

        <Dialog header="Module Edit" visible={this.state.visibleEdit} style={{width: '30vw'}} onHide={this.onHide} >
          <EditModule data={this.state.moduleSelected} done={this.onEdited}/>
        </Dialog>

        <div className="p-col-12">
          <Fieldset legend="List Module " toggleable={true}>
            <ContextMenu model={this.state.menu} ref={el => this.cm = el} />
            <DataTable
              editable={true}
              header={header}
              globalFilter={this.state.globalFilter}
              emptyMessage="No records found"
              ref={(el) => this.dt = el}
              value={this.state.moduleList}
              contextMenuSelection={this.state.moduleSelected}
              onContextMenuSelectionChange={e => this.setState({moduleSelected: e.value})}
              onContextMenu={e => this.cm.show(e.originalEvent)}
            >
              <Column field="name" header="Name" />
              <Column field="serial_number" header="Serial Number" />
              <Column field="timeout" header="Timeout" />
              <Column field="type" header="Type" />
            </DataTable>
          </Fieldset>
        </div>
      </div>
    )
  }
}

export default withContext(ModuleList);
