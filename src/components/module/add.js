import React, { useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import { Button } from 'primereact/button';
import API from '../../_helper/api';

const AddModule = ((props) => {

  const [data, setData] = useState({
    name: '',
    serialNumber: '',
    status: 1,
    timeout: '',
    type: ''
  })

  const [typeList] = useState([
    {
      label: 'A',
      value: 'A'
    },{
      label: 'B',
      value: 'B'
    },{
      label: 'C',
      value: 'C'
    },{
      label: 'Modbus',
      value: 'Modbus'
    }]
  )

  const onChangeValue = (e) => {
    let newData = {
      ...data,
      [e.target.name]: e.target.value
    }

    setData(newData)
  }

  const onSave = (e) => {
    API.post('admin/module/create', {
      name: data.name,
      serialNumber: data.serialNumber,
      type: data.type,
      status: data.status,
      timeout: data.timeout
    }).then(result => {
      console.log(result);
      props.reload();
      props.onClose();
    }).catch(err => {
      console.log(err);
    })
  }

  return (
    <div className="p-grid p-fluid">
      <div className="p-col-12 p-lg-12">
        <div className="card card-w-title">
          <h1>New Module</h1>
          <div className="p-grid">
            <div className="p-col-12 p-md-12">
              <label htmlFor="name">Name</label>
              <InputText autoComplete="off" name="name" value={data.name} onChange={onChangeValue} autoFocus />
            </div>
          </div>

          <div className="p-grid">
            <div className="p-col-12 p-md-12">
              <label htmlFor="serialNumber">Serial Number</label>
              <InputText autoComplete="off" name="serialNumber" value={data.serialNumber} onChange={onChangeValue} />
            </div>
          </div>

          <div className="p-grid">
            <div className="p-col-12 p-md-12">
              <label htmlFor="type">Module Type</label>
              <Dropdown
                name="type"
                value={data.type}
                options={typeList}
                onChange={onChangeValue}
                placeholder="Select Module Type"
              />
            </div>
          </div>

          <div className="p-grid">
            <div className="p-col-12 p-md-12">
              <label htmlFor="status">Status</label>
              <InputText name="status" value={data.status} onChange={onChangeValue} />
            </div>
          </div>

          <div className="p-grid">
            <div className="p-col-12 p-md-12">
              <label htmlFor="timeout">Timeout</label>
              <InputText name="timeout" value={data.timeout} onChange={onChangeValue} />
            </div>
          </div>

          <Button label="Save" className="p-button-raised" onClick={onSave} />
        </div>
      </div>
    </div>
  )

})

export default AddModule
