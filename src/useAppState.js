import { useState } from 'react'

/**
 * Our custom React hook to manage state
 */

const useAppState = () => {
  const initialState = { count: 0 }

  // Manage the state using React.useState()
  const [state, setState] = useState(initialState)

  // Build our actions. We'll use useMemo() as an optimization,
  // so this will only ever be called once.
  const actions = useMemo(() => getActions(setState), [setState])

  return { state, actions }
}

// Define your actions as functions that call setState().
// It's a bit like Redux's dispatch(), but as individual
// functions.
const getActions = setState => ({
  increment: () => {
    setState(state => ({ ...state, count: count + 1 }))
  },
  decrement: () => {
    setState(state => ({ ...state, count: count - 1 }))
  }
})

export default useAppState;
