import React, {useState, useEffect} from 'react';
import API from '../../_helper/api';
import {Button} from "primereact/button";
import {Dropdown} from 'primereact/dropdown';
import {InputText} from "primereact/inputtext";
import {InputTextarea} from 'primereact/inputtextarea';
import {Card} from 'primereact/card';
import {InputSwitch} from 'primereact/inputswitch';

const functionsList = [
  {
    label: 'currentTime',
    value: 'currentTime',
  },{
    label: 'currentValue',
    value: 'currentValue',
  },{
    label: 'previousTime',
    value: 'previousTime',
  },{
    label: 'previousValue',
    value: 'previousValue',
  },{
    label: 'previousHourValue',
    value: 'previousHourValue',
  },{
    label: 'previousDayValue',
    value: 'previousDayValue',
  }
];

const initialSaveInterval = [
  {
    label: 'Just In Time',
    value: 'jit'
  },{
    label: 'Hour',
    value: 'hour'
  },{
    label: 'Day',
    value: 'day'
  }
]

const initialParameter = [{
  key: '',
  function: '',
  pointId: ''
}];

const FormulaEditor = (({formula}) => {
  const [parameter, setParameter] = useState(initialParameter);

  const [saveIntervalList] = useState(initialSaveInterval);
  const [saveInterval, setSaveInterval] = useState();

  const [logic, setLogic] = useState('');
  const [active, setActive] = useState(false);
  const [currentData, setCurrentData] = useState({});

  useEffect(() => {
    setCurrentData(formula);
    elaborate(formula);
  }, [formula])


  const onChangeParams = ((e) => {
    const {name, dataset, value} = e.currentTarget;
    const {index} = dataset;
    let newParameter = [...parameter];

    newParameter[index][name] = value;
    setParameter(newParameter);
  })

  const onChangeFunction = ((e) => {
    const {id, name} = e.target;
    let newParameter = [...parameter];
    newParameter[id][name] = e.value;

    setParameter(newParameter);
  })

  const onSave = (() => {
    let isActive = (active) ? '1' : '0';

    API.post('admin/formula', {
      currentData,
      parameter,
      logic,
      active: isActive,
      saveInterval: saveInterval
    })
    .then(result => {
      console.log(result)
    })
    .catch(error => {
      console.error(error);
    })
  })

  const elaborate = ((formula) => {
    let config = formula.config || '';

    if(config !== ''){
      let configParsed = JSON.parse(config)
      let parameter = configParsed.parameter;
      let newParameter = [];

      Object.keys(parameter).forEach((el, i) => {
        let funct = parameter[el].slice(0, parameter[el].indexOf('('));
        let pointId = parameter[el].slice(parameter[el].indexOf('(')+1, parameter[el].indexOf(')'));

        newParameter.push({
          key: el,
          function: funct,
          pointId: pointId
        });
      });

      setParameter(newParameter);
      setLogic(configParsed.formula);
    }else{
      setParameter(initialParameter)
      setLogic('');
    }

    setActive((formula.active === "1") ? true : false)
    setSaveInterval(formula.save_interval);

  })

  const paramComp = (el, idx) => {
    return (
      <div className="p-inputgroup">
      <InputText size={5} data-index={idx} name="key" placeholder="Parameter" value={el.key} onChange={onChangeParams}/>
      <Dropdown
        data-index={idx}
        id={idx.toString()}
        index={idx}
        name="function"
        value={el.function}
        options={functionsList}
        onChange={onChangeFunction}
        placeholder="Select a Function"
      />

      <InputText data-index={idx}
        value={el.pointId}
        name="pointId"
        placeholder="pointId"
        onChange={(e) => {
          const {name, dataset, value} = e.currentTarget;
          const {index} = dataset
          let newParameter = [...parameter];
          newParameter[index][name] = value;

          setParameter(newParameter);
        }}
      />

      <Button
        icon="pi pi-times"
        data-index={idx}
        onClick={(e) => {
          const {index} = e.currentTarget.dataset;
          let newParameter = [...parameter];
          newParameter.splice(index, 1);
          setParameter(newParameter);
        }}
        className="p-button-danger"
       />
    </div>
  )};

  const  cardFooter = <Button label="save" onClick={onSave} />;

  return (
    <Card  footer={cardFooter}>
    <div className="p-grid">
      <div className="p-col-6">
        <h4>Parameter <Button label="New Parameter" onClick={() => {
          setParameter([...parameter, {
            key: '',
            function: '',
            pointId: ''
          }])}}/>
        </h4>

        {parameter.map((el, idx) => {
          return(
            <div style={{paddingTop: 5}} key={idx}>{paramComp(el, idx)}</div>
          )}
        )}

      </div>

      <div className="p-col-6">
        <h4>Formula Logic</h4>
        <InputTextarea
          rows={3}
          cols={40}
          value={logic}
          onChange={(e) => setLogic(e.currentTarget.value)} />
      </div>

      <div className="p-col-6">
        <h4>Active</h4>
        <div className="p-grid p-align-center">
          <div className="p-col-4">
            <InputSwitch
              inputId="isActive"
              name="isActive"
              style={{verticalAlign: 'middle'}}
              onLabel="Active" offLabel="Pasif"
              checked={active}
              onChange={(e) => setActive(e.value)}/>

              <label style={{ paddingLeft: 10}} htmlFor="isActive">{(active) ? 'True' : 'False'}</label>

              {(active) ?
                <p> Formula will be executed <b> right after loket receive data </b>  </p> :
                <p> Formula will be executed <b> each vismon's request </b> </p> }

          </div>
        </div>
      </div>

      <div className="p-col-6">
      <h4>Save Interval</h4>
        <Dropdown
            name="saveInterval"
            value={saveInterval}
            options={saveIntervalList}
            onChange={(e) => setSaveInterval(e.value)}
            placeholder="Select a Function" />
          </div>
      </div>

    </Card>
  )
})

export default FormulaEditor;
