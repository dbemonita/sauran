import React, { useState, useEffect } from 'react';
import {Button} from 'primereact/button';
import {InputText} from 'primereact/inputtext';
import {Card} from 'primereact/card';
import api from '../../_helper/api';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Message} from 'primereact/message';

const formInit = {
  host: '',
  port: '',
  user: '',
  pass: ''
};

const SetEmail = () => {

  const [configEmail, setConfigEmail] = useState([]);
  const [form, setForm] = useState(formInit);
  const [msgError, setMsgError] = useState({});

  useEffect(() => {
    api.get('config-report/setup-email')
    .then(result => {
      console.log(result.data);
      setConfigEmail(result.data);
    })
    .catch( e => {
      console.log(e);
    });
  }, []);

  const onEdit = (currentData) => {
    console.log(currentData);
    setForm(currentData);
  };

  const onDelete = (id) => {
    if(window.confirm('Konfirmasi delete')){
      api.delete('config-report/setup-email/'+id)
      .then( done => {
        api.get('config-report/setup-email')
        .then(result => {
          console.log(result.data);
          setConfigEmail(result.data);
        });
      })
      .catch(err => {
        console.log(err);
      });
    }
    
    // api.delete('config-report/setup-email/'+id)
  };

  const actionColumn = (rowData, column) => {
    return <div className="p-inputgroup">
      <Button 
        onClick={ () => {
          onEdit(rowData)
        }}
        label="" 
        icon="pi pi-pencil" 
        className="p-button-warning"
      />
      <Button 
        onClick={ () => {
          onDelete(rowData.id)
        }}
        label="" 
        icon="pi pi-trash" 
        className="p-button-danger"
        style={{ marginRight: '.25em' }} 
      />
    </div>;
  }

  const onChange = (e) => {
    const {name, value} = e.currentTarget;
    setForm({
      ...form,
      [name]: value
    })
  }

  const onProceed = (e) => {
    if(!checkError()){
      let send;
      
      if(form.id){
        send = api.patch('config-report/setup-email/'+form.id, form);
      }else{
        send = api.post('config-report/setup-email', form);
      }
      
      send.then(result => {
        api.get('config-report/setup-email')
        .then( resp => {
          setConfigEmail(resp.data);
          setForm(formInit)
        })
      })
      .catch( e => {
        console.log(e);
      });
    }
  }

  const checkError = () => {
    let isError = false;
    let msg = {};
    for(let name in form){
      if(form.hasOwnProperty(name)){
        console.log(name, form[name]);
        if(form[name] === ''){
          msg[name] = 'Perlu untuk diisi';
          isError = true;
        }
      }
    }

    setMsgError({
      ...msg
    });
    
    return isError;
  };

  return (

    <div className="p-grid p-justify-center">
      <div className="p-col-12 p-lg-6 p-md-12 p-sm-12">
        <DataTable value={configEmail} autoLayout={true}>
          <Column field="host" header="Email Host" />
          <Column field="port" header="Email Port" />
          <Column field="user" header="Email User" />
          <Column field="pass" header="Email Pass" />
          <Column body={actionColumn} style={{textAlign:'center', width: '8em'}}/>
        </DataTable>
      </div>
      <div className="p-col-12 p-lg-4 p-md-12 p-sm-12">
        <Card>
        <div className="p-grid">
          <div className="p-lg-12 p-md-12 p-sm-12">
            <h4>{(form.id) ? 'Update' : 'New '} Email Setup</h4>
            <p>Host</p>
            <InputText 
              name="host"
              style={{width: '100%'}}
              value={form.host}
              onChange={onChange}
              className={ msgError.host ? 'p-error' : ''}
            />
            {msgError.host && 
              <Message style={{marginTop: '0.5em'}} severity="error" text={msgError.host} />
            }

            <p>Port</p>
            <InputText 
              name="port"
              value={form.port}
              onChange={onChange}
              className={ msgError.port ? 'p-error' : ''}
            />
            {msgError.port && 
              <Message severity="error" text={msgError.port} />
            }

            <p>User</p>
            <InputText 
              name="user"
              style={{width: '100%'}}
              autoComplete="new-password"
              value={form.user}
              onChange={onChange}
              className={ msgError.user ? 'p-error' : ''}
            />
            {msgError.user && 
              <Message style={{marginTop: '0.5em'}} severity="error" text={msgError.user} />
            }

            <p>Pass</p>
            <InputText 
              name="pass"
              style={{width: '100%'}}
              type="password"
              autoComplete="new-password"
              value={form.pass}
              onChange={onChange}
              className={ msgError.pass ? 'p-error' : ''}
            />
            {msgError.pass && 
              <Message style={{marginTop: '0.5em'}} severity="error" text={msgError.pass} />
            }
          </div>
          <div className="p-col-12">
            <Button 
              type='button' 
              icon="pi pi-check" 
              label="Proceed" 
              className="p-button-raised p-button-rounded" 
              style={{float:'right'}}
              onClick={onProceed}
            />

            <Button 
              type='button' 
              icon="pi pi-undo" 
              label="clear" 
              className="p-button-raised p-button-warning" 
              onClick={() => setForm(formInit)}
            />
          </div>
        </div>
        </Card>
      </div>
      
    </div>

  );

};

export default SetEmail;