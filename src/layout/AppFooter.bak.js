import React, {Component} from 'react';
import {withContext} from "../AppContext";
import {Message} from "primereact/message";
import {ProgressBar} from 'primereact/progressbar';

class AppFooter extends Component {

  constructor(){
    super();
    this.state = {
      ws: true,
      api: true
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if(nextProps.network !== this.props.network){
      const {ws, api} = nextProps.network;
      setTimeout(() => {
        this.setState({
          ws: ws,
          api: api
        })
      }, 1500)
    }
  }

  render() {
    return (
      <div
        className="layout-footer"
        style={{
          position: 'fixed',
          flexGrow: 1,
          width: '100%',
          bottom: 0,
          padding: 3
        }}>
        {!this.state.ws && (
          <Message style={{
            padding: 0,
            marginRight: 3
          }} severity="error" text="Websocket error" />
        )}

        {!this.state.api && (
          <Message style={{
            padding: 0,
            marginRight: 3
          }} severity="error" text="API error" />
        )}
        {(this.props.onApiRequest) &&
          <ProgressBar style={{height:5, zIndex:9999}} mode="indeterminate" />}
      </div>
    );
  }
}

// export default withContext(AppFooter);
export default AppFooter;
