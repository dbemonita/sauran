import React, {Component} from 'react';
import ListParsingRef from '../components/parsing-ref/list';
import ModuleList from '../components/module/list';

class ModuleParsing extends Component {

  constructor(props) {
    super(props);
    this.state = {

    }
  }

  render() {
    return (
      <div  className="p-grid">
        <div className="p-col-12 p-md-9 p-md-12 p-sm-12">
          <ModuleList/>
        </div>
        <div className="p-col-12 p-md-9 p-md-12 p-sm-12">
          <ListParsingRef />
        </div>
      </div>
    )
  }
}

export default ModuleParsing;