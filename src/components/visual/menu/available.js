import React, { useEffect, useState} from 'react';
import {Tree} from 'primereact/tree';

const MenuAvailable = (props) => {

  const [selectedNodeKeys, setSelectedNodeKeys] = useState([]);
  const [expandedKeys, setExpandedKeys] = useState({});
  const [visualTree, setVisualTree] = useState([]);

  useEffect(() => {
    if(props.visualChecked){
      let expanding = {};
      Object.keys(props.visualChecked).forEach(el => {
        let toArr = el.split('-');
        for (let i = 1; i < toArr.length; i++) {
          let idx = toArr.slice(0, (toArr.length-i));
          if(idx.length > 0){
            expanding[idx.join('-')] = true;
          }else{
            expanding[idx] = true;
          }
          
        }
      });

      setSelectedNodeKeys(props.visualChecked);
      setExpandedKeys(expanding);
    }
  }, [props.visualChecked]);

  useEffect(() => {
    if(props.visualTree){
      setVisualTree(props.visualTree);
    }
  }, [props.visualTree]);

  return (
    <>
    <Tree style={{
        height: (window.innerHeight - 145) + 'px',
        maxHeight: (window.innerHeight - 145 ) + 'px'
      }}
      value={visualTree}
      selectionMode="checkbox"
      selectionKeys={selectedNodeKeys}
      expandedKeys={expandedKeys}
      onToggle={e => {
        setExpandedKeys(e.value)
      }}
      onSelectionChange={e => {
        props.onChangeVisualChecked(e.value);
        setSelectedNodeKeys(e.value);
      }}
    />
  </>
  )
};

export default MenuAvailable;