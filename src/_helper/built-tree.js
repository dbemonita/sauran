import {default as LTT} from 'list-to-tree';

function buildTree (data, filtered) {
  let tree = new LTT(data, {
    key_id: 'id',
    key_parent: 'parentId',
    key_child: 'children'
  });

  if(filtered) {
    tree.sort(sortOrder());
  }

  let record = tree.GetTree();
  return record;
}

function sortOrder() {
  return (a, b) => {
    const aid = Number(a.get('order'));
    const bid = Number(b.get('order'));

    if(aid < bid) {
      return -1;
    }else if(aid < bid){
      return 1;
    }else{
      return 0;
    }
  };
}

function buildTreeUser (data, allVListFlat, allVListFlatIdKey) {

  let vFilter4 = new Set([]);
  let pparent = {};

  for(let i in data){
    if(data.hasOwnProperty(i)){
      if(pparent[data[i].parentId] === undefined){
        let parent = allVListFlatIdKey[data[i].parentId];
        vFilter4.add({
          ...parent
        });
        pparent[data[i].parentId] = true;
      }
    
      if(data[i].leaf){
        vFilter4.add({...data[i]});
      }
    }
  }
  
  let arrayVFilter = Array.from(vFilter4);
  
  return {
    tree: buildTree(arrayVFilter, true),
    list: arrayVFilter
  };
}

export {buildTree, buildTreeUser};