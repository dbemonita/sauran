import React, {Component} from 'react';
import {Tree} from "primereact/tree";
import API from '../../_helper/api';
import {ContextMenu} from 'primereact/contextmenu';
import {OverlayPanel} from "primereact/overlaypanel";
import {InputText} from "primereact/inputtext";
import {Button} from "primereact/button";
import {Dialog} from 'primereact/dialog';
import NewXml  from './new-xml';

export class VisualList extends Component {

  constructor(props){
    super(props);
    this.state = {
      files: [],
      menuFile: [
        {
          label:'Open',
          icon:'fa fa-fw fa-code',
          command: () => {
            this.onOpenFile();
          }
        },
        {
          label:'Rename',
          icon:'pi pi-fw pi-pencil',
          command: (e) => {
            this.onRenameFile(e.originalEvent);
          }
        },
        {
          label:'Delete',
          icon:'pi pi-fw pi-trash',
          command: () => {
            window.confirm("Delete Confirmation") && this.onDelete();
          }
        }
      ],
      menuFolder: [
        {
          label: 'New Folder',
          icon: 'pi pi-fw pi-folder',
          command: (e) => {
            this.onNewFolder(e.originalEvent);
          }
        },
        {
          label: 'New File',
          icon: 'pi pi-fw pi-file',
          command: (e) => {
            this.setState({
              visible: true
            })
          }
        },
        {
          label: 'Delete Folder',
          icon:'pi pi-fw pi-trash',
          command: () => {
            window.confirm("Delete Confirmation") && this.onDeleteFolder();
          }
        },
        {
          label: 'Expand',
          icon: 'pi pi-fw pi-angle-down',
          command: () => {
            let expandedKeys = {...this.state.expandedKeys};
            if (expandedKeys[this.state.selectedFileKey])
              delete expandedKeys[this.state.selectedFileKey];
            else
              expandedKeys[this.state.selectedFileKey] = true;

            this.setState({expandedKeys: expandedKeys});
          }
        }
      ],
      menu: [],
      expandedKeys: null,
      selectedFileKey: null,
      newFolderName: '',
      newFolderRootName: '',
      newFileName: '',
      newAny: '',
      visualTypeList: [],
      visualType: '',
      renameFileName: ''
    };

    this.onContextMenu = this.onContextMenu.bind(this);
    this.onOpenFile = this.onOpenFile.bind(this);
    this.onNewFolder = this.onNewFolder.bind(this);
    this.getCurrentFile = this.getCurrentFile.bind(this);
    this.onContextMenuSelectionChange = this.onContextMenuSelectionChange.bind(this);
    this.getCurrentPath = this.getCurrentPath.bind(this);
    this.createNewFolder = this.createNewFolder.bind(this);
    this.createNewFile = this.createNewFile.bind(this);
    this.fetchData = this.fetchData.bind(this);
    this.getVisualType = this.getVisualType.bind(this);
    this.expands = this.expands.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onDeleteFolder = this.onDeleteFolder.bind(this);
    this.createNewXml = this.createNewXml.bind(this);
    this.onRenameFile = this.onRenameFile.bind(this);
    this.renameFile = this.renameFile.bind(this);
  }

  componentDidMount() {
    this.fetchData(false);
    this.getVisualType();
  }

  getVisualType(){
    API.get('admin/visual-type')
    .then( response => {
      this.setState({
        visualTypeList: response.data
      })
    })
    .catch( error => {
      console.log(error);
    })
  }

  fetchData(expanded = true){
    API.get('admin/visual/list')
      .then( resp => {
        if (expanded) {
          this.expands();
        }

        this.setState({
          files: resp.data
        })
      })
      .catch( err => {
        console.log(err);
      })
  }

  onContextMenu(event){
    const {node} = event;
    let typeMenu = node.label.includes('.xml') ? 'menuFile' : 'menuFolder';
    let menu = this.state[typeMenu];
    this.setState({
      menu: menu
    });
    this.cm.show(event.originalEvent);
  }

  onContextMenuSelectionChange(event){
    this.setState({selectedFileKey: event.value})
  }

  onOpenFile(){
    let file = this.getCurrentFile();
    this.props.onOpenFile(file.data.path);
  }

  onNewFolder(e){
    this.expands();
    this.op.toggle(e);
  }

  onRenameFile(e){
    let file = this.getCurrentFile();

    this.setState({
      renameFileName: file.label
    })

    this.opRe.toggle(e);
  }

  renameFile(e){
    const {renameFileName} = this.state;

    let file = this.getCurrentFile();
    let path = this.getCurrentPath();

    let oldPath = path;
    let newPath = path.replace(file.label, renameFileName)

    API.post('admin/xml/rename-file', {
      newPath, oldPath
    }).then(response => {
      this.fetchData(true);
      this.opRe.hide();
    }).catch( err => {
      console.log(err);
    })
  }

  expands(){
    let expandedKeys = {...this.state.expandedKeys};
    if (expandedKeys[this.state.selectedFileKey]) {
      // delete expandedKeys[this.state.selectedFileKey];
    }else {
      expandedKeys[this.state.selectedFileKey] = true;
    }
    this.setState({expandedKeys: expandedKeys});
  }

  getCurrentFile() {
    const {files, selectedFileKey} = this.state;
    let node = files;

    if (selectedFileKey.toString().indexOf('-') > 0) {
      let keys = selectedFileKey.split('-');
      keys.forEach((el, i) => {
        let current;
        if (i === 0) {
          current = node[el];
          node = current;
        } else {
          if (node.children[el]) {
            current = node.children[el];
            node = current
          }
        }
      });
    } else {
      node = files[selectedFileKey];
    }

    return node;
  }

  getCurrentPath() {
    const {files, selectedFileKey} = this.state;
    let node = files;
    let path = '';

    if(selectedFileKey === null){
      return path;
    }else {
      if(selectedFileKey.toString().indexOf('-') > 0) {
        let keys = selectedFileKey.split('-');
        keys.forEach((el, i) => {
          let current;
          if (i === 0) {
            current = node[el];
            node = current;
            path = node.data.name;
          } else {
            if (node.children[el]) {
              current = node.children[el];
              node = current;
              path = path.concat('/', node.data.name);
            }
          }
        });
      } else {
        path = files[selectedFileKey].data.name;
      }

      return path;
    }
  }

  createNewFolder(e){

    let currentPath = this.getCurrentPath();
    let newPath;
    const {type} = e.currentTarget.dataset;

    if(type === 'root'){
      const {newFolderRootName} = this.state;
      newPath = '/' + newFolderRootName;
    }else{
      const {newFolderName} = this.state;
      newPath = currentPath.concat('/', newFolderName);
    }

    API.post('admin/xml/new-folder', {
      folderPath: newPath
    }).then(response => {
      this.fetchData(true);
      console.log(response);
    }).catch( err => {
      console.log(err);
    })
  }

  createNewXml(data){
    this.expands();
    let user = JSON.parse(localStorage.getItem("user")) || {};
    this.setState({
      newFileName: data.fileName,
      visualType: data.visualType.label,
      user: user
    }, () => this.createNewFile());
    this.setState({
      visible: false
    })
  }

  createNewFile(){
    const {newFileName, visualType, user} = this.state;
    let currentPath = this.getCurrentPath();
    let newPath;
    if(currentPath !== ''){
      newPath = currentPath.concat('/', newFileName, '.xml');
    }else{
      newPath = newFileName.concat('.xml');
    }

    API.post('admin/xml/new-file', {
      filePath: newPath,
      visualType: visualType,
      user: user
    }).then(response => {
      this.fetchData(true);
      if(response.data.template){
        this.props.onOpenFile(newPath);
      }
    }).catch( err => {
      console.log(err);
    })
  }

  onDelete(){
    let currentPath = this.getCurrentPath();
    API.post('admin/xml/remove-file', {
      filePath: currentPath
    }).then( response => {
      if(currentPath !== ''){
        this.fetchData(true);
      }
    }).catch( err => {
      console.log(err);
    })
  }

  onDeleteFolder(){
    let currentFile = this.getCurrentFile();
    let currentPath = this.getCurrentPath();

    console.log('currentFile',currentFile)
    console.log('currentPath',currentPath)

    if(currentFile.children){
      window.alert('Cant delete folder that has children');
    }else{
      API.post('admin/xml/remove-folder', {
        folderPath: currentPath
      }).then( response => {
        if(currentPath !== ''){
          this.fetchData(true);
        }
      }).catch( err => {
        console.log(err);
      })
    }

  }

  render() {
    return (
      <div>
        {/* New folder */}
        <OverlayPanel ref={(el) => this.op = el}>
          <div className="p-inputgroup">
            <InputText value={this.state.newFolderName} onChange={(e) => this.setState({newFolderName: e.target.value})} />
            <Button style={{float: 'right'}} icon="pi pi-plus" onClick={this.createNewFolder}/>
          </div>
        </OverlayPanel>

        {/* Rename file */}
        <OverlayPanel ref={(el) => this.opRe = el}>
          <div className="p-inputgroup">
            <InputText value={this.state.renameFileName} onChange={(e) => this.setState({renameFileName: e.target.value})} />
            <Button style={{float: 'right'}} icon="pi pi-pencil" onClick={this.renameFile}/>
          </div>
        </OverlayPanel>

         <Dialog header="New File" visible={this.state.visible} onHide={() => this.setState({visible: false})} >
            {(this.state.visualTypeList.length > 0) && <NewXml onDone={this.createNewXml} visualType={this.state.visualTypeList}/>}
         </Dialog>
        <ContextMenu model={this.state.menu} ref={el => this.cm = el} />

        <div className="p-inputgroup">
          <InputText placeholder="Folder Name" size={15} value={this.state.newFolderRootName} onChange={(e) => this.setState({newFolderRootName: e.target.value})} />
          <Button label="New Folder Root" data-type='root' onClick={this.createNewFolder}/>
        </div>
        <br/>
        <Tree
          value={this.state.files}
          expandedKeys={this.state.expandedKeys}
          onToggle={e => this.setState({expandedKeys: e.value})}
          style={{
            // maxHeight: 200,
            overflow: 'auto',
            height: (window.innerHeight - 138) + 'px',
            maxHeight: (window.innerHeight - 138) + 'px'
          }}
          onContextMenuSelectionChange={this.onContextMenuSelectionChange}
          onContextMenu={this.onContextMenu}
        />
      </div>
    )
  }
}
