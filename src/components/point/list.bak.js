import React, { Component } from 'react';
import {DataTable} from 'primereact/components/datatable/DataTable';
import {Column} from 'primereact/components/column/Column';
import {InputText} from 'primereact/components/inputtext/InputText';
import {Dropdown} from 'primereact/components/dropdown/Dropdown';
import API from '../../_helper/api';
import {Button} from "primereact/button";
import {Dialog} from 'primereact/dialog';
import {AddPoint} from './add';
import {ContextMenu} from "primereact/contextmenu";

export class ListPoint extends Component {
  constructor(props) {
    super(props);
    this.state = {
      assetId: '',
      typeList: [],
      list: [],
      updated: {},
      addDialogVisible: false,
      isShowButtonUpdate: false,
      selectedPoint: null,
      menu: [{
        "label": "Delete",
        "icon": "fa fa-recycle",
        command: () => {
          window.confirm("Delete Confirmation") && this.onDelete();
        }
      }]
    };

    this.loadData = this.loadData.bind(this);
    this.commonEditor = this.commonEditor.bind(this);
    this.onEditorValueChange = this.onEditorValueChange.bind(this);
    this.onTypeEditorChange = this.onTypeEditorChange.bind(this);
    this.inputTextEditor = this.inputTextEditor.bind(this);
    this.typeEditor = this.typeEditor.bind(this);
    this.trackUpdated = this.trackUpdated.bind(this);
    this.onAddPoint = this.onAddPoint.bind(this);
    this.onHideAddDialog = this.onHideAddDialog.bind(this);
    this.onSaveChange = this.onSaveChange.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  async componentDidMount() {
    const {assetId} = this.props;
    const typeList = await API.get('admin/point-type');

    if(assetId){
      this.setState({
        assetId: assetId,
        typeList: typeList.data
      }, () => {
        this.loadData();
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    console.log('nextProps', nextProps);
    if (nextProps.assetId !== this.props.assetId) {
      this.setState({
        assetId: nextProps.assetId
      }, () => {
        this.loadData();
      });
    }
  }

  loadData(){
    const {assetId} = this.state;
    API.get('admin/point', {
      params: {
        assetId: assetId
      }
    }).then( result => {
      this.setState({
        list: result.data
      })
    }).catch( err => {
      console.log(err);
    })
  }

  onDelete(){
    const {selectedPoint} = this.state;
    API.delete('admin/point/' + selectedPoint.id)
      .then((data) => {
        this.loadData();
      })
  }

  inputTextEditor(props) {
    const {field} = props;
    return <InputText type="text" value={props.rowData[field]} onChange={(e) => this.onEditorValueChange(props, e.target.value)} />;
  }

  onEditorValueChange(props, value) {
    let updated = [...props.value];
    console.log(props);
    console.log(value);
    console.log('updated', this.state.updated);
    // if(props.field === 'id'){

    // }
    // updated[props.rowIndex].idRef = this.state.list[props.rowIndex].id;
    if(!this.state.list[props.rowIndex].idRef){
      updated[props.rowIndex].idRef = this.state.list[props.rowIndex].id;
    }
    updated[props.rowIndex][props.field] = value;
    this.setState({list: updated});
    this.trackUpdated(props);
  }

  onTypeEditorChange(props, e) {
    const {value, originalEvent} = e;
    const label = originalEvent.currentTarget.textContent;

    let updated = [...props.value];

    if(!this.state.list[props.rowIndex].idRef){
      updated[props.rowIndex].idRef = this.state.list[props.rowIndex].id;
    }
    updated[props.rowIndex][props.field] = label;
    updated[props.rowIndex].point_type_id = value;

    this.setState({list: updated});
    this.trackUpdated({...props, field: 'point_type_id'});
  }

  trackUpdated(props){
    const {rowIndex, field} = props;
    const {updated} = this.state;
    let updating = (updated[rowIndex]) ? [...updated[rowIndex], field] : [field];

    this.setState({
      updated:{
        ...this.state.updated,
        [rowIndex]: updating
      }
    }, () => {
      if(Object.keys(this.state.updated).length > 0){
        this.setState({
          isShowButtonUpdate: true
        })
      }
    });
  }

  commonEditor(props){
    return this.inputTextEditor(props);
  }

  typeEditor(props){
    return <Dropdown
      value={props.rowData.point_type_id} options={this.state.typeList}
      onChange={(e) => this.onTypeEditorChange(props, e)} style={{width:'100%'}} placeholder="Select Type"/>
  }

  onAddPoint(){
    this.setState({
      addDialogVisible: true
    })
  }

  onHideAddDialog(){
    this.setState({
      addDialogVisible: false
    })
  }

  onSaveChange(){
    const {updated, list} = this.state;
    let payloads = [];
    this.setState({
      isShowButtonUpdate: false
    });

    for (let idx in updated){
      if(updated.hasOwnProperty(idx)){
        let obj = {};
        let source = list[idx];

        updated[idx].forEach(el => {
          obj.idRef= source.idRef;
          obj[el] = source[el];
        });

        payloads.push(obj);
      }
    }

    API.patch('admin/point/' + this.state.assetId, {
      data: payloads
    }).then(result => {
      this.loadData();
    }).catch(err => {
      this.loadData();
    })
  }

  static lastValueTpl(rowData, column) {
    return <span style={{color: 'green'}}>{rowData['lastValue']}</span>;
  }

  render(){
    const {list} = this.state;

    const header = <div className="p-clearfix" style={{'lineHeight':'1.87em'}}> Points
      <Button icon="pi pi-plus" style={{'float':'right'}} onClick={this.onAddPoint}/>
    </div>;

    const footer = <div className="p-clearfix" style={{
      lineHeight:'1.87em',
      display: (this.state.isShowButtonUpdate) ? "block" : "none"
      }}>
      <Button icon="pi pi-save" style={{'float':'right'}} onClick={this.onSaveChange} label="Update"/>
    </div>;

    return (
      <div className="p-grid p-fluid">
        <div className="p-col-12 p-lg-12">
          <ContextMenu model={this.state.menu} ref={el => this.cm = el} onHide={() => this.setState({selectedPoint: null})}/>
          <Dialog header="New Point" visible={this.state.addDialogVisible} onHide={this.onHideAddDialog}>
            <AddPoint hide={this.onHideAddDialog} assetId={this.state.assetId} reload={this.loadData}/>
          </Dialog>

          <DataTable
            value={list}
            editable={true}
            header={header}
            footer={footer}
            contextMenuSelection={this.state.selectedPoint}
            onContextMenuSelectionChange={e => this.setState({selectedPoint: e.value})}
            onContextMenu={e => this.cm.show(e.originalEvent)}
          >
            <Column field="id" header="Id" editor={this.commonEditor} />
            <Column field="name" header="Name" editor={this.commonEditor} />
            <Column field="lastValue" header="Last Value" body={ListPoint.lastValueTpl} />
            <Column field="unit" header="Unit" editor={this.commonEditor} />
            <Column field="type" header="Type" editor={this.typeEditor} />
            <Column field="range_max" header="Range Max" editor={this.commonEditor} />
            <Column field="range_min" header="Range Min" editor={this.commonEditor} />
            <Column field="m" header="M" editor={this.commonEditor} />
            <Column field="c" header="C" editor={this.commonEditor} />
          </DataTable>
        </div>
      </div>
    )
  }
}
