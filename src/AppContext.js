import React, {Component} from "react";
import API from './_helper/api';

const AppContext = React.createContext();

export class AppContextProvider extends Component {
  constructor() {
    super();
    this.state = {
      user: JSON.parse(localStorage.getItem("user")) || {},
      token: localStorage.getItem("token") || "",
      notif: {},
      network: {
        ws: true,
        api: true
      },
      message: {},
      onApiRequest: false,
      isShowConfirm: false,
      isConfirm: false
    };

    this.setMessage = this.setMessage.bind(this);
    this.setNetwork = this.setNetwork.bind(this);
    this.setOnApiRequest = this.setOnApiRequest.bind(this);
    this.handlingRequestError = this.handlingRequestError.bind(this);
    this.hideConfirm = this.hideConfirm.bind(this);
    this.showConfirm = this.showConfirm.bind(this);
    this.setConfirm = this.setConfirm.bind(this);
  }

  login = (credentials) => {
    return API.post("admin/auth/login", credentials)
      .then(response => {

        const {token, user} = response.data;
        localStorage.setItem("token", token);
        localStorage.setItem("user", JSON.stringify(user));
        this.setState({
          user,
          token
        });

        return response;
      });
  };

  logout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    this.setState({
      user: {},
      token: ""
    })
  };

  hideConfirm = () => {
    this.setState({
      isShowConfirm: false
    })
  }

  showConfirm = () => {
    this.setState({
      isShowConfirm: true
    })
  }

  setConfirm = (val) => {
    console.log(val);
    this.setState({
      isConfirm: val
    })
  }

  setMessage = (data) => {
    this.setState({
      message: data
    });
  };

  handlingRequestError = (data) => {
    this.setMessage({
      code: 400,
      content: data.message
    });
    this.setNetwork('api', false);
  };

  setNetwork = (type, status) => {
    this.setState({
      network: {
        ...this.state.network,
        [type]: status
      }
    })
  };

  setOnApiRequest = (onOff) =>{
    this.setState({onApiRequest: onOff});;
  }

  render() {
    return (
      <AppContext.Provider
        value={{
          login: this.login,
          logout: this.logout,
          setNetwork: this.setNetwork,
          setMessage: this.setMessage,
          setOnApiRequest: this.setOnApiRequest,
          hideConfirm: this.hideConfirm,
          showConfirm: this.showConfirm,
          setConfirm: this.setConfirm,
          handlingRequestError: this.handlingRequestError,
          ...this.state
        }}>
        {this.props.children}

      </AppContext.Provider>
    )
  }
}

export const withContext = Component => {
  return props => {
    return (
      <AppContext.Consumer>
        {
          globalState => {
            return (
              <Component
                {...globalState}
                {...props}
              />
            )
          }
        }
      </AppContext.Consumer>
    )
  }
}
