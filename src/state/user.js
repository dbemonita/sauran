const userReducer = (state, action) => {
  switch (action.type) {
    case 'setUser':
      return {
        ...state,
        user: action.payload
      };
    case 'removeUser':
      return {

      };
    default:
      return state;
  }
};

export default userReducer;