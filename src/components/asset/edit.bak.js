import React, {Component} from 'react';
import {InputText} from 'primereact/inputtext';
import {Dropdown} from 'primereact/dropdown';
import {Button} from 'primereact/button';
import API from '../../_helper/api';
import {TabView, TabPanel} from 'primereact/tabview';
import ListPoint from '../point/list';

export class EditAsset extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showButtonUpdate: false,
      data: {
        id: '',
        name: '',
        parentId: '',
        type: ''
      },
      assetTypeList: []
    };

    this.onUpdate = this.onUpdate.bind(this);
    this.onChangeValue = this.onChangeValue.bind(this);
    this.loadAssetType = this.loadAssetType.bind(this);
  }

  componentDidMount() {
    const {data} = this.props.node;
    this.loadAssetType();
    this.setState({
      data: {
        id: data.id || '',
        name: data.name || '',
        parentId: data.parentId || '',
        type: data.assetTypeId
      }
    });
  }

  loadAssetType() {
    API.get('admin/asset-type')
      .then(result => {
        this.setState({
          assetTypeList: result.data
        })
      })
      .catch(e => {
        console.log(e);
      })
  }

  componentDidUpdate(nextProps) {
    console.log('edit', nextProps.node);
    if (nextProps.node.key !== this.props.node.key) {
      this.setState({
        data: {
          id: nextProps.node.data.id,
          name: nextProps.node.data.name,
          parentId: nextProps.node.data.parentId || '',
          type: nextProps.node.data.assetTypeId
        }
      });
    }
  }

  onChangeValue(e) {
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value
      },
      showButtonUpdate: true
    })
  }

  onUpdate() {
    const {data} = this.state;
    API.patch('admin/asset/crud/' + data.id, {
      name: data.name,
      parentId: data.parentId,
      type: data.type
    }).then(result => {
      this.props.reload();
    }).catch(err => {
      console.log(err);
    })
  }

  render() {
    const {data} = this.state;
    return (
      <div className="p-grid p-fluid">
        <div className="p-col-12 p-lg-12">
          <TabView activeIndex={this.state.activeIndex} onTabChange={(e) => this.setState({activeIndex: e.index})}>
            <TabPanel header="Detail" leftIcon="fa fa-edit">
              <div className="p-grid">
                <div className="p-col-12 p-md-12">
                  <label htmlFor="name">Name</label>
                  <InputText name="name" value={data.name} onChange={this.onChangeValue} autoFocus/>
                </div>
              </div>

              <div className="p-grid">
                <div className="p-col-12 p-md-12">
                  <InputText name="name" type="hidden" value={data.parentId} onChange={this.onChangeValue}/>
                </div>
              </div>

              <div className="p-grid">
                <div className="p-col-12 p-md-12">
                  <label htmlFor="assetType">Asset Type</label>
                  <Dropdown
                    name="assetType"
                    value={this.state.data.type}
                    options={this.state.assetTypeList}
                    onChange={this.onChangeValue}
                    placeholder="Select Asset Type"
                  />
                </div>
              </div>

              <div className="p-grid">
                <div className="p-col-12 p-md-12">
                  {(this.state.showButtonUpdate) && (
                    <Button label="Update" className="p-button-raised" onClick={this.onUpdate}/>
                  )}
                </div>
              </div>
            </TabPanel>
            <TabPanel header="Points" rightIcon="fa fa-dot-circle-o">
              <ListPoint assetId={data.id} />
            </TabPanel>
          </TabView>


        </div>
      </div>
    )
  }
}
