import React, {lazy, Suspense } from 'react';
import {Route, Switch, useRouteMatch} from 'react-router-dom';
import Topbar from '../pages/config-report/topbar';

const Dashboard = (
  lazy(() => (
    import('../pages/config-report/dashboard')
  ))
);

const SetEmail = (
  lazy(() => (
    import('../pages/config-report/set-email')
  ))
);

const New = (
  lazy(() => (
    import('../pages/config-report/new')
  ))
);

const Edit = (
  lazy(() => (
    import('../pages/config-report/edit')
  ))
);

const LoadingMessage = () => (
  "Loading..."
);

const ConfigReport = ({match}) => {

  let { path, url } = useRouteMatch();

  return (
    <div  className="p-grid">
        <div className="p-col-12" style={{marginTop: '-10px'}}>
      <Topbar />
      <div style={{margin: '.5em'}}>
      <Suspense fallback={<LoadingMessage />}>
        <Switch>
          <Route path={`${url}/`} exact >
            <Dashboard />
          </Route>
          <Route path={`${url}/set-email`} >
            <SetEmail />
          </Route>
          <Route path={`${url}/new`} >
            <New />
          </Route>
          <Route path={`${url}/edit/:id`} >
            <Edit />
          </Route>
        </Switch>
      </Suspense>
    </div>
    </div>
    </div>
  )
};

export default ConfigReport