import React, { Component } from 'react';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import { Button } from 'primereact/button';
import API from '../../_helper/api';

export class AddAsset extends Component {
  constructor(props) {
    super(props);
    this.state = {
      parentAsset: {},
      node: {},
      showButtonSave: true,
      asset: {
        name: '',
        assetType: ''
      },
      assetTypeList: []
    };

    this.onChangeValue = this.onChangeValue.bind(this);
    this.onSave = this.onSave.bind(this);
    this.loadAssetType = this.loadAssetType.bind(this);
  }

  componentDidMount() {
    this.loadAssetType();
    if(this.props.node){
      this.setState({
        node: this.props.node,
        parentAsset: this.props.node.data
      });
    }
  }

  loadAssetType(){
    API.get('admin/asset-type')
      .then(result => {
        this.setState({
          assetTypeList: result.data
        })
      })
      .catch(e => {
        console.log(e);
      })
  }

  onSave(e) {
    const { asset, parentAsset } = this.state;
    API.post('admin/asset/crud/create', {
      name: asset.name,
      parentId: parentAsset.id,
      type: asset.assetType
    }).then(result => {
      this.setState({
        showButtonSave: false
      });
    }).catch(err => {
      console.log(err);
    })
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.node){
      if (nextProps.node.data.id !== this.props.node.data.id) {
        this.setState({
          parentAsset: nextProps.node.data,
          asset: {
            name: '',
            assetType: ''
          },
          showButtonSave: true
        });
      }
    }else{
      this.setState({
        parentAsset: {}
      });
    }
  }

  onChangeValue(e) {
    this.setState({
      asset: {
        ...this.state.asset,
        [e.target.name]: e.target.value
      }
    })
  }

  render() {
    const { asset, parentAsset} = this.state;

    return (
      <div className="p-grid p-fluid">
        <div className="p-col-12 p-lg-12">
          <div className="card card-w-title">

            {(Object.keys(parentAsset).length > 0) ? (
              <h1>{"Anak Asset Baru (" + parentAsset.name +")"}</h1>
            ) : (
              <h1>{"Asset Baru"}</h1>
            )}

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="name">Name</label>
                <InputText name="name" autoComplete="off" value={asset.name} onChange={this.onChangeValue} />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="assetType">Asset Type</label>
                <Dropdown
                  name="assetType"
                  value={this.state.asset.assetType}
                  options={this.state.assetTypeList}
                  onChange={this.onChangeValue}
                  placeholder="Select Asset Type"
                />
              </div>
            </div>
            {(this.state.showButtonSave) && (
              <Button label="Save" className="p-button-raised" onClick={this.onSave} />
            )}
          </div>
        </div>
      </div>
    )
  }


}
