import React, {Component} from 'react';
import UserList from "../components/user/list";

class UserManagement extends Component {

  constructor(props) {
    super(props);
    this.state = {

    }
  }

  render() {
    return (
      <div  className="p-grid">
        <div className="p-col-12">
          <UserList />
        </div>
      </div>
    )
  }
}

export default UserManagement;