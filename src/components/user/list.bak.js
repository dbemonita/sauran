import React, {Component} from 'react';
import API from '../../_helper/api';
import {DataTable} from 'primereact/components/datatable/DataTable';
import {Column} from "primereact/column";
import {Dropdown} from "primereact/dropdown";
import {InputText} from "primereact/inputtext";
import {Button} from "primereact/button";
import AddUser from "./add";
import {Dialog} from "primereact/dialog";
import {ContextMenu} from "primereact/contextmenu";
import {withContext} from '../../AppContext';
import { IntlProvider, FormattedRelativeTime } from "react-intl";

class UserList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedUser: null,
      userList: [],
      roleList: [],
      addDialogVisible: false,
      menu: [{
        "label": "Delete",
        "icon": "fa fa-recycle",
        command: () => {
          window.confirm("Delete Confirmation") && this.onDelete();
        }
      }],
      data: {
        roleId: null
      },
      updated: {},
      isShowButtonUpdate: false
    };

    this.fetchRole = this.fetchRole.bind(this);
    this.fetchUser = this.fetchUser.bind(this);
    this.roleEditor = this.roleEditor.bind(this);
    this.onEditorValueChange = this.onEditorValueChange.bind(this);
    this.inputTextEditor = this.inputTextEditor.bind(this);
    this.commonEditor = this.commonEditor.bind(this);
    this.trackUpdated = this.trackUpdated.bind(this);
    this.onRoleEditorChange = this.onRoleEditorChange.bind(this);
    this.onSaveChange = this.onSaveChange.bind(this);
    this.onHideAddDialog = this.onHideAddDialog.bind(this);
    this.onAddUser = this.onAddUser.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  componentDidMount() {
    console.log('kehed= ================')
    this.fetchRole();
    this.fetchUser();
  }

  fetchUser(){
    API.get('admin/user')
      .then( result => {
        this.setState({
          userList: result.data
        })
      })
      .catch( err => {
        console.log(err);
      })
  }

  fetchRole(){
    API.get('admin/role')
      .then( result => {
        this.setState({
          roleList: result.data
        })
      })
      .catch( err => {
        console.log(err);
      })
  }

  roleEditor(props){
    return (this.props.user.roleId === 0) ? <Dropdown
      value={props.value[props.rowIndex].role_id} options={this.state.roleList}
      onChange={(e) => this.onRoleEditorChange(props, e)} style={{width:'100%'}} placeholder="Select Type"/> : props.rowData.role;
  }

  onRoleEditorChange(props, e) {
    const {value, originalEvent} = e;
    const label = originalEvent.currentTarget.textContent;

    let updated = [...props.value];

    updated[props.rowIndex].userId = this.state.userList[props.rowIndex].userId;
    updated[props.rowIndex][props.field] = label;
    updated[props.rowIndex].role_id = value;

    this.setState({
      list: updated,
      data: {
        roleId: value
      }
    });
    this.trackUpdated({...props, field: 'role_id'});
  }

  inputTextEditor(props) {
    const {field} = props;
    return <InputText type="text" value={props.rowData[field]} onChange={(e) => this.onEditorValueChange(props, e.target.value)} />;
  }

  onEditorValueChange(props, value) {
    let updated = [...props.value];

    updated[props.rowIndex].userId = this.state.userList[props.rowIndex].userId;
    updated[props.rowIndex][props.field] = value;
    this.setState({userList: updated});
    this.trackUpdated(props);
  }

  trackUpdated(props){
    const {rowIndex, field} = props;
    const {updated} = this.state;

    let updating;
    if(updated[rowIndex]){
      // console.log(updated[rowIndex]);
      // console.log(field);
      // console.log(updated[rowIndex].includes(field));
      if(!updated[rowIndex].includes(field)){
        updating = [...updated[rowIndex], field]
      }else{
        updating = updated[rowIndex];
      }
    }else{
      updating = [field];
    }

    this.setState({
      updated:{
        ...this.state.updated,
        [rowIndex]: updating
      }
    }, () => {
      if(Object.keys(this.state.updated).length > 0){
        this.setState({
          isShowButtonUpdate: true
        })
      }
    });
  }

  commonEditor(props){
    return this.inputTextEditor(props);
  }

  onHideAddDialog(){
    this.setState({
      addDialogVisible: false
    })
  }

  onSaveChange(){
    const {updated, userList} = this.state;
    let payloads = [];
    this.setState({
      isShowButtonUpdate: false
    });

    for (let idx in updated){
      if(updated.hasOwnProperty(idx)){
        let obj = {};
        let source = userList[idx];

        updated[idx].forEach(el => {
          obj.user_id= source.userId;
          obj[el] = source[el];
        });

        payloads.push(obj);
      }
    }

    API.patch('admin/user/1', payloads).then(result => {
      this.fetchUser();
    }).catch(err => {
      this.fetchUser();
    })
  }

  onAddUser(){
    this.setState({
      addDialogVisible: true
    })
  }

  onDelete(){
    const {selectedUser} = this.state;
    API.delete('admin/user/' + selectedUser.userId)
      .then((data) => {
        this.fetchUser();
      })
  }

  lastLoginTempl(rowData, column){
    return <FormattedRelativeTime
      numeric="auto"
      unit="second"
      updateIntervalInSeconds={1 * 60}
      value={ (rowData.last_login) - (Math.floor(new Date().getTime() / 1000)) } />
  }

  render() {
    // console.log(this.props.user);
    const header = <div className="p-clearfix" style={{'lineHeight':'1.87em'}}> User
      <Button icon="pi pi-plus" style={{'float':'right'}} onClick={this.onAddUser}/>
    </div>;

    const footer = <div className="p-clearfix" style={{
      lineHeight:'1.87em',
      display: (this.state.isShowButtonUpdate) ? "block" : "none"
    }}>
      <Button icon="pi pi-save" style={{'float':'right'}} onClick={this.onSaveChange} label="Update"/>
    </div>;

    return (
      <div  className="p-grid">
        <div className="p-col-12 ">

          <ContextMenu model={this.state.menu} ref={el => this.cm = el} onHide={() => this.setState({selectedUser: null})}/>
          <Dialog header="New User" visible={this.state.addDialogVisible} onHide={this.onHideAddDialog}>
            <AddUser hide={this.onHideAddDialog} roleList={this.state.roleList} reload={this.fetchUser}/>
          </Dialog>
          <IntlProvider locale="id-ID">
            <DataTable
              value={this.state.userList}
              editable={true}
              header={header}
              footer={footer}
              contextMenuSelection={this.state.selectedUser}
              onContextMenuSelectionChange={e => this.setState({selectedUser: e.value})}
              onContextMenu={e => this.cm.show(e.originalEvent)}
            >
              <Column field="username" header="Username" editor={this.commonEditor}  rowSpan={2}/>
              <Column field="role" header="Role" editor={this.roleEditor}/>
              { this.props.user.roleId === 0 && (
                <Column field="password" header="New Password" editor={this.commonEditor}/>
              )}
              <Column field="email" header="Email" editor={this.commonEditor} rowSpan={2}/>
              <Column field="last_login" body={this.lastLoginTempl} header="Last Login"  rowSpan={2}/>
            </DataTable>
          </IntlProvider>
        </div>
      </div>
    )
  }
}

export default withContext(UserList);
