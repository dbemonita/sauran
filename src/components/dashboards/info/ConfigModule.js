import React, { Component } from 'react';
import API from '../../../_helper/api';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';

export class ConfigModule extends Component{

  constructor(props){
    super(props);
    this.state = {
      moduleId: '',
      data: []
    };

    this.fetchData = this.fetchData.bind(this);
  }

  componentDidMount() {
    if(this.state.moduleId){
      this.fetchData();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.moduleId !== this.props.moduleId) {
      this.setState({
        moduleId: nextProps.moduleId
      }, () => {
        this.fetchData();
      })
    }
  }

  fetchData(){
    API.get('monitor/module/info-config/' + this.state.moduleId)
      .then((data) => {
        if(data.data){
          this.setState({
            data: data.data
          })
        }
      })
  }

  render() {
    return (
      <DataTable value={this.state.data}>
        <Column field="name" header="Name" />
        <Column field="serial_number" header="Serial Number" />
        <Column field="type" header="Type" />
        <Column field="status" header="Status" />
        <Column field="timeout" header="Timeout (ms)" />
      </DataTable>
    )
  }
}