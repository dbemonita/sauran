import React, {Component} from 'react';
import FormulaList from '../components/formula/list';
import FormulaEditor from '../components/formula/editor';

class Formula extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formula: ''
    }

    this.onUserSelected = this.onUserSelected.bind(this);
  }

  onUserSelected(formula){
    this.setState({
      formula: formula
    })
  }

  render(){
    return (
      <div  className="p-grid">
        <div className="p-md-3 p-sm-12">
          <FormulaList onUserSelected={this.onUserSelected}/>
        </div>
        <div className="p-md-9 p-sm-12">
          <FormulaEditor formula={this.state.formula || ''}/>
        </div>
      </div>
    )
  }
}

export default Formula;
