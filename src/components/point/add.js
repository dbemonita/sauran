import React, { Component, useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import API from '../../_helper/api';
import { Button } from 'primereact/button';
import { Dropdown } from 'primereact/dropdown';

const AddPoint = (props) => {

  const [data, setData] = useState({
    id: '',
    name: '',
    unit: '',
    pointTypeId: null,
    rangeMin: 0,
    rangeMax: 100,
    m: 1,
    c: 0,
    assetId: ''
  });
  const [pointTypeList, setPointTypeList] = useState([]);

  useEffect(() => {

    async function fetch(){
      let dataFetch = await API.get('admin/point-type');
      setPointTypeList(dataFetch.data);
    }

    fetch();
  }, []);

  useEffect( () => {
    setData({
      ...data,
      assetId: props.assetId
    })
  }, [props.assetId]);

  const onChangeValue =  e => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    });
  };

  const onSave = (e) => {
    API.post('admin/point/create', {
      id: data.id,
      name: data.name,
      unit: data.unit,
      pointTypeId: data.pointTypeId,
      rangeMin: data.rangeMin,
      rangeMax: data.rangeMax,
      m: data.m,
      c: data.c,
      assetId: data.assetId
    }).then(result => {
      props.reload();
      props.hide();
    }).catch(err => {
      props.reload();
      props.hide();
    })
  };

  return (
    <div className="p-grid p-fluid">
      <div className="p-col-12 p-lg-12">
        <h1>New Point</h1>

        <div className="p-grid">
          <div className="p-col-12 p-md-12">
            <label htmlFor="id">ID</label>
            <InputText autoComplete="off" min="1" name="id" value={data.id} type="number" onChange={onChangeValue}  />
          </div>
        </div>

        <div className="p-grid">
          <div className="p-col-12 p-md-12">
            <label htmlFor="name">Name</label>
            <InputText autoComplete="off" name="name" value={data.name} onChange={onChangeValue} autoFocus />
          </div>
        </div>

        <div className="p-grid">
          <div className="p-col-12 p-md-4">
            <label htmlFor="unit">Unit</label>
            <InputText autoComplete="off" name="unit" value={data.unit} onChange={onChangeValue}  />
          </div>
          <div className="p-col-12 p-md-4">
            <label htmlFor="m">M</label>
            <InputText name="m" value={data.m} onChange={onChangeValue}  />
          </div>
          <div className="p-col-12 p-md-4">
            <label htmlFor="c">C</label>
            <InputText name="c" value={data.c} onChange={onChangeValue}  />
          </div>
        </div>

        <div className="p-grid">
          <div className="p-col-12 p-md-6">
            <label htmlFor="rangeMin">Range Min</label>
            <InputText name="rangeMin" value={data.rangeMin} onChange={onChangeValue}  />
          </div>
          <div className="p-col-12 p-md-6">
            <label htmlFor="rangeMax">Range Max</label>
            <InputText name="rangeMax" value={data.rangeMax} onChange={onChangeValue}  />
          </div>
        </div>

        <div className="p-grid">
          <div className="p-col-12 p-md-12">
            <label htmlFor="pointTypeId">Point Type</label>
            <Dropdown
              name="pointTypeId"
              value={data.pointTypeId}
              options={pointTypeList}
              onChange={onChangeValue}
              placeholder="Select Point Type"
            />
          </div>
        </div>

        <div className="p-grid">
          <div className="p-col">
            <Button label="Save" className="p-button-raised" onClick={onSave} />
          </div>
        </div>

      </div>
    </div>
  )

};

export default AddPoint;


// export class AddPoint extends Component {
//   constructor(props) {
//     super(props);
//
//     this.onChangeValue = this.onChangeValue.bind(this);
//     this.onSave = this.onSave.bind(this);
//   }
//
//   async componentDidMount() {
//     let data = await API.get('admin/point-type');
//
//     this.setState({
//       pointTypeList: data.data,
//       data: {
//         ...this.state.data,
//         assetId: this.props.assetId
//       }
//     })
//   }
//
//   componentWillReceiveProps(nextProps) {
//     if (nextProps.assetId !== this.props.assetId) {
//       this.setState({
//         data: {
//           ...this.state.data,
//           assetId: nextProps.assetId
//         }
//       });
//     }
//   }
//
//   onChangeValue(e) {
//     this.setState({
//       data: {
//         ...this.state.data,
//         [e.target.name]: e.target.value
//       }
//     })
//   }
//
//   onSave(e) {
//     const { data } = this.state;
//     API.post('admin/point/create', {
//       id: data.id,
//       name: data.name,
//       unit: data.unit,
//       pointTypeId: data.pointTypeId,
//       rangeMin: data.rangeMin,
//       rangeMax: data.rangeMax,
//       m: data.m,
//       c: data.c,
//       assetId: data.assetId
//     }).then(result => {
//       this.props.reload();
//       this.props.hide();
//     }).catch(err => {
//       this.props.reload();
//       this.props.hide();
//     })
//   }
//
//   render(){
//     const {data} = this.state;
//     return (
//       <div className="p-grid p-fluid">
//         <div className="p-col-12 p-lg-12">
//             <h1>New Point</h1>
//
//             <div className="p-grid">
//               <div className="p-col-12 p-md-12">
//                 <label htmlFor="id">ID</label>
//                 <InputText autoComplete="off" min="1" name="id" value={data.id} type="number" onChange={this.onChangeValue}  />
//               </div>
//             </div>
//
//             <div className="p-grid">
//               <div className="p-col-12 p-md-12">
//                 <label htmlFor="name">Name</label>
//                 <InputText autoComplete="off" name="name" value={data.name} onChange={this.onChangeValue} autoFocus />
//               </div>
//             </div>
//
//             <div className="p-grid">
//               <div className="p-col-12 p-md-4">
//                 <label htmlFor="unit">Unit</label>
//                 <InputText autoComplete="off" name="unit" value={data.unit} onChange={this.onChangeValue}  />
//               </div>
//               <div className="p-col-12 p-md-4">
//                 <label htmlFor="m">M</label>
//                 <InputText name="m" value={data.m} onChange={this.onChangeValue}  />
//               </div>
//               <div className="p-col-12 p-md-4">
//                 <label htmlFor="c">C</label>
//                 <InputText name="c" value={data.c} onChange={this.onChangeValue}  />
//               </div>
//             </div>
//
//             <div className="p-grid">
//               <div className="p-col-12 p-md-6">
//                 <label htmlFor="rangeMin">Range Min</label>
//                 <InputText name="rangeMin" value={data.rangeMin} onChange={this.onChangeValue}  />
//               </div>
//               <div className="p-col-12 p-md-6">
//                 <label htmlFor="rangeMax">Range Max</label>
//                 <InputText name="rangeMax" value={data.rangeMax} onChange={this.onChangeValue}  />
//               </div>
//             </div>
//
//             <div className="p-grid">
//               <div className="p-col-12 p-md-12">
//                 <label htmlFor="pointTypeId">Point Type</label>
//                 <Dropdown
//                   name="pointTypeId"
//                   value={this.state.data.pointTypeId}
//                   options={this.state.pointTypeList}
//                   onChange={this.onChangeValue}
//                   placeholder="Select Point Type"
//                 />
//               </div>
//             </div>
//
//             <div className="p-grid">
//               <div className="p-col">
//                 <Button label="Save" className="p-button-raised" onClick={this.onSave} />
//               </div>
//             </div>
//
//           </div>
//       </div>
//     )
//   }
// }