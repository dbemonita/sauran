import React, {useEffect, useState} from 'react';
import MenuAvailable from "./menu/available";
import MenuOrder from "./menu/order";
import API from '../../_helper/api';
const LTT = require('list-to-tree');

const getCheckedUser = async (userId) => {
  return API.get('v2/admin/visual/checked/user/' + userId);
};
const getMenuAvailableList = async () => {
  return API.get('admin/visual/list');
};
const getList = async () => {
  return API.get('admin/visual/list?tree=false');
};

function rebuildTree(userList, visualList){

  let keyVisualList = {};
  let newVisualList = {};
  visualList.forEach(el => keyVisualList[el.key] = el);

  Object.keys(userList).forEach( el => {
    if(keyVisualList[el]){
      let keysDestruct = el.split('-');

      for(let i=0; i< keysDestruct.length; i++){
        let kk = keysDestruct.slice(0, i+1).join('-');
        newVisualList[kk] = {
          order: userList[el].order,
          ...keyVisualList[kk]
        };
      }
    }
  });

  debugger;
  let treeing = Object.keys(newVisualList).map(el => newVisualList[el]);
  // console.log(treeing);
  // debugger;
  let ltt = new LTT(treeing, {
    key_id: 'id',
    key_parent: 'parentId',
    key_child: 'children'
  });
  // debugger;
  ltt.sort(function (a, b) {
    let aorder = Number(a.get('order'));
    let border = Number(b.get('order'));
    return (aorder > border) ? 1 : -1;
  });

  return ltt.GetTree();
}

const VisualSelect = (props) => {

  const [menuAvailableList, setMenuAvailableList] = useState([]);
  const [visualChecked, setVisualChecked] = useState({});
  const [visualList, setVisualList] = useState([]);
  const [visualMenuUser, setVisualMenuUser] = useState([]);


  useEffect( () => {
    async function getData(){
      let getMenu = await getMenuAvailableList();
      setMenuAvailableList(getMenu.data);
    }

    getData();
  }, []);

  useEffect(() => {
    if(props.user){
      async function getData(){
        const {data} = await getCheckedUser(props.user.userId);
        const list = await getList(props.user.userId);
        setVisualList(list.data);
        setVisualChecked(data);
        let tree = rebuildTree(data, list.data);

        setVisualMenuUser(tree);
      }
      getData();
    }
  }, [props.user]);

  const onChangeCheckVisual = (data) => {
    setVisualChecked(data);
    let tree = rebuildTree(data, visualList);
    setVisualMenuUser(tree);
  };

  const onChangeOrder = (data) => {
    console.log(data);
    setVisualMenuUser(data);
  };

  return (
    <div
      className="p-grid"
      style={{
        background: 'linear-gradient(to right, #e1e4e8, #edf0f5)',
        marginTop: 0
      }}>

      <div className="p-md-6 p-sm-12">
        <span>Visual Menu List</span>
        <MenuAvailable
          visualChecked={visualChecked}
          onChangeCheckVisual={onChangeCheckVisual}
          menuAvailableList={menuAvailableList}
        />
      </div>
      <div className="p-md-6 p-sm-12">
        <span>Visual Menu List Selected (drag to re-order)</span>
        <MenuOrder
          visualChecked={visualChecked}
          visualMenuUser={visualMenuUser}
          onChangeOrder={onChangeOrder}
        />
      </div>
    </div>
  );
};

export default VisualSelect;