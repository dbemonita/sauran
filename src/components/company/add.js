import React, { Component } from 'react';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { Button } from 'primereact/button';
import API from '../../_helper/api';

export class AddCompany extends Component {
  constructor(props) {
    super(props);
    this.state = {
      parentNode: props.node,
      data: {
        name: '',
        parentId: props.node.id,
        status: 1,
        address: ''
      }
    };

    this.onChangeValue = this.onChangeValue.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.node.key !== this.props.node.key) {
      this.setState({
        parentNode: nextProps.node,
        data: {
          ...this.state.data,
          parentId: nextProps.node.data.id
        }
      });
    }
  }

  onSave(e){
    const {data} = this.state;
    API.post('admin/company/create', {
      name: data.name,
      parentId: data.parentId,
      address: data.address,
      status: data.status
    }).then(result => {
      console.log(result);
      // this.props.reload();
    }).catch(err => {
      console.log(err);
    })
  }

  onChangeValue(e) {
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value
      }
    })
  }

  render() {
    const { data } = this.state;
    
    return (
      <div className="p-grid p-fluid">
        <div className="p-col-12 p-lg-12">
          <div className="card card-w-title">
            <h1>New Company</h1>

            <div className="p-grid">
              <div className="p-col-12 p-md-12">
                <label htmlFor="name">Name</label>
                <InputText name="name" value={data.name} onChange={this.onChangeValue} autoFocus />
              </div>
            </div>

            <div className="p-grid">
              <div className="p-col-12 p-md-6">
                <label htmlFor="address">Address</label>
                <InputTextarea name="address" value={data.address} onChange={this.onChangeValue} rows={5} ></InputTextarea>
              </div>
            </div>
            
            <Button label="Save" className="p-button-raised" onClick={this.onSave}/>

          </div>
        </div>
      </div>
    )
  }


}