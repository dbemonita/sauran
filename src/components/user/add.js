import React, { useState, memo } from 'react';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import { Button } from 'primereact/button';
import API from '../../_helper/api';

const AddUser = memo((props) => {

  const [data, setData] = useState({
    username: '',
    role_id: '',
    email: '',
    password: ''
  });

  const onSave = (e) => {
    API.post('admin/user/create', data)
      .then(result => {
      setData({
        username: '', role_id: '', email: '', password: ''
      });

      props.reload();
      props.hide();
    }).catch(err => {
      console.log(err);
    })
  };

  const onChangeValue = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }

  return (
    <div className="p-grid p-fluid">
      <div className="p-col-12 p-lg-12">
        <div className="card card-w-title">

          <div className="p-grid">
            <div className="p-col-12 p-md-12">
              <label htmlFor="username">Username</label>
              <InputText name="username" autoComplete="off" value={data.username} onChange={onChangeValue} />
            </div>
          </div>

          <div className="p-grid">
            <div className="p-col-12 p-md-12">
              <label htmlFor="password">Password</label>
              <InputText name="password" autoComplete="off" value={data.password} onChange={onChangeValue} />
            </div>
          </div>

          <div className="p-grid">
            <div className="p-col-12 p-md-12">
              <label htmlFor="email">Email</label>
              <InputText name="email" autoComplete="off" value={data.email} onChange={onChangeValue} />
            </div>
          </div>

          <div className="p-grid">
            <div className="p-col-12 p-md-12">
              <label htmlFor="assetType">Role</label>
              <Dropdown
                name="role_id"
                value={data.role_id}
                options={props.roleList}
                onChange={onChangeValue}
                placeholder="Select Role"
              />
            </div>
          </div>

          <Button label="Save" className="p-button-raised" onClick={onSave} />
        </div>
      </div>
    </div>
  )

});

export default AddUser;
