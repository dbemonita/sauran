import React, { useState, useEffect, lazy, Suspense } from 'react';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'font-awesome/css/font-awesome.min.css';
import '../layout/layout.css';
import '../layout/primeflex.css';
import '../App.css';
import classNames from "classnames";
import AppTopbar from '../layout/AppTopBar';
import AppFooter from "./AppFooter";
import Sidebar from "./Sidebar";
import AppRoute from "./AppRoute";
import {useStateValue} from '../appState';


const isDesktop = () => {
  return window.innerWidth > 1024;
};

const Login = (
  lazy(() =>(
    import("../pages/Login")
  ))
);
const LoadingMessage = () => (
  "Loading..."
);

const AppWrapper = () => {

  const [wrapperClass, setWrapperClass] = useState({});
  const [menuClick, setMenuClick] = useState(false);
  const [layoutMode] = useState("overlay");
  const [mobileMenuActive, setMobileMenuActive] = useState(false);
  const [staticMenuInactive] = useState(false);
  const [overlayMenuActive, setOverlayMenuActive] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [{user}] = useStateValue();

  useEffect(() => {
    if(user.username){
      setIsLoggedIn(true);
    }else{
      setIsLoggedIn(false);
    }
  },[user]);

  useEffect( () => {
    setWrapperClass(
      classNames('layout-wrapper', {
      'layout-overlay': layoutMode === 'overlay',
      'layout-static': layoutMode === 'static',
      'layout-static-sidebar-inactive': staticMenuInactive && layoutMode === 'static',
      'layout-overlay-sidebar-active': overlayMenuActive && layoutMode === 'overlay',
      'layout-mobile-sidebar-active': mobileMenuActive
    }));
  }, [menuClick,layoutMode, staticMenuInactive, overlayMenuActive, mobileMenuActive]);

  const onToggleMenu = (event) => {
    setMenuClick(true);
    if (isDesktop()) {
      if (layoutMode === 'overlay') {
        setOverlayMenuActive(!overlayMenuActive);
      }
      else if (layoutMode === 'static') {
        setOverlayMenuActive(!staticMenuInactive);
      }
    }
    else {
      setMobileMenuActive(!mobileMenuActive);
    }

    event.preventDefault();
  };

  const onMenuItemClick = ({layoutMode, staticMenuInactive, overlayMenuActive, mobileMenuActive}) => {
    setWrapperClass(
      classNames('layout-wrapper', {
        'layout-overlay': layoutMode === 'overlay',
        'layout-static': layoutMode === 'static',
        'layout-static-sidebar-inactive': staticMenuInactive && layoutMode === 'static',
        'layout-overlay-sidebar-active': overlayMenuActive && layoutMode === 'overlay',
        'layout-mobile-sidebar-active': mobileMenuActive
      })
    );
    setOverlayMenuActive(false);
  };

  return (
    <Suspense fallback={<LoadingMessage />}>
      {!isLoggedIn ? <Login /> :
      <div className={wrapperClass}>

        <AppTopbar onToggleMenu={onToggleMenu} />
        <Sidebar onMenuItemClick={onMenuItemClick}/>
        <AppRoute/>
        <AppFooter />
        <div className="layout-mask">
        </div>
      </div>}
    </Suspense>
  )

};

export default AppWrapper;