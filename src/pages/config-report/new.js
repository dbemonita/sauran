import React, { useState, useEffect } from 'react';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { ReactMultiEmail, isEmail } from 'react-multi-email';
import 'react-multi-email/style.css';
import api from '../../_helper/api';
import {Dropdown} from 'primereact/dropdown';
import {Message} from 'primereact/message';
import {Card} from 'primereact/card';
import cronstrue from 'cronstrue';

const formInit = {
  crontime: '* * * * *',
  subject: '',
  body: '',
  recipient: '',
  epaperId: '',
  configEmailId: ''
};
const New = () => {

  const [emails, setEmails] = useState([]);
  const [emailSetup, setEmailSetup] = useState([]);
  const [emailSetupSelect, setEmailSetupSelect] = useState([]);
  const [epaper, setEpaper] = useState([]);
  const [form, setForm] = useState(formInit);
  const [template, setTemplate] = useState({});
  const [msgError, setMsgError] = useState({});
  
  useEffect(() => {
    api.get('config-report/epaper')
    .then(result => {
      setEpaper(result.data);
    })
    .catch(e => {
      console.log(e);
    });
  }, []);

  useEffect(() => {
    api.get('config-report/setup-email')
    .then(result => {
      const {data} = result;
      if(data.length){
        let rcd = data.map(el => {
          return {
            id: el.id, 
            name: '('+el.id+') ' + el.host + ' - ' + el.user
          };
        });
        console.log('rcd', rcd);
        setEmailSetup(rcd);
      }
    })
    .catch(e => {
      console.log(e);
    });
  }, []);

  const removeEmail = (index) => {
    let newEmail = [...emails];
    newEmail.splice(index, 1);
    setEmails(newEmail);
  };

  const onChange = (e) => {
    const {value, name} = e.currentTarget;
    setForm({
      ...form,
      [name]: value
    });
  };

  const onProceed = (e) => {
    if(!checkError()){
      api.post('config-report', form)
      .then(result => {
        if(result.data.status){
          alert('Data saved');
        }
      })
      .catch(err => {
        console.log(err);
      });
    }
  };

  const checkError = () => {
    let isError = false;
    let msg = {};
    for(let name in form){
      if(form.hasOwnProperty(name)){
        console.log(name, form[name]);
        if(form[name] === ''){
          msg[name] = 'Perlu untuk diisi';
          isError = true;
        }
      }
    }

    setMsgError({
      ...msg
    });
    return isError;
  };

  const footer = (
    <div style={{overflow: 'auto' }}>
      <Button
        type='button'
        icon="pi pi-check"
        style={{ float: 'right'}}
        label="Proceed"
        className="p-button-raised p-button-rounded" 
        onClick={onProceed}
      />
    </div>
  )

  return (

    <div className="p-grid p-justify-center" style={{ marginTop: '.5em' }}>
      <div className="p-col-4">
        <Card footer={footer}>
          <h4>Report Setup</h4>
          <p>Cron Time</p>
          <InputText 
            id='crontime'
            name='crontime'
            style={{ width: '100%' }}
            value={form.crontime}
            onChange={onChange}
            className={ msgError.crontime ? 'p-error' : ''}
          />
          {msgError.crontime && 
            <Message style={{marginTop: '0.5em'}} severity="error" text={msgError.crontime} />
          }

          <p>Setup Email</p>
          <Dropdown value={emailSetupSelect} 
            options={emailSetup} 
            onChange={(e) => {
              setEmailSetupSelect(e.value);
              setForm({
                ...form,
                configEmailId: e.value.id
              })
            }}
            optionLabel="name"
            className={ msgError.configEmailId ? 'p-error' : ''}
            style={{ width: '100%' }}
          />
          {msgError.configEmailId &&
            <Message style={{marginTop: '0.5em'}} severity="error" text={msgError.configEmailId} />
          }

          <p>Subject</p>
          <InputText 
            id="subject" 
            name="subject" 
            style={{ width: '100%' }} 
            value={form.subject}
            onChange={onChange}
            className={ msgError.subject ? 'p-error' : ''}
          />
          {msgError.subject && 
            <Message style={{marginTop: '0.5em'}} severity="error" text={msgError.subject} />
          }

          <p>Body (html)</p>
          <InputTextarea 
            id="body" 
            name="body" 
            style={{ width: '100%' }} 
            value={form.body}
            onChange={onChange}
            className={ msgError.body ? 'p-error' : ''}
          />
          {msgError.body && 
            <Message style={{marginTop: '0.5em'}} severity="error" text={msgError.body} />
          }

          <p>Recipient</p>
          <ReactMultiEmail
            style={{ width: '100%', border: msgError.recipient ? '1px solid red' : '1px solid #a6a6a6' }}
            id="recipient"
            placeholder="email"
            emails={emails}
            onChange={(email) => {
              setEmails(email);
              setForm({
                ...form,
                recipient: JSON.stringify(email)
              })
            }}
            validateEmail={email => {
              return isEmail(email);
            }}
            getLabel={(email, index) => {
              return (
                <div data-tag key={index}>
                  {email}
                  <span data-tag-handle onClick={() => removeEmail(index)}>
                    ×
                    </span>
                </div>
              );
            }}
          />
          {msgError.recipient && 
            <Message style={{marginTop: '0.5em'}} severity="error" text={msgError.recipient} />
          }

          <p>Template</p>
          <Dropdown value={template} 
            options={epaper} 
            onChange={(e) => {
              setTemplate(e.value);
              setForm({
                ...form,
                epaperId: e.value.id
              })
            }}
            optionLabel="name"
            className={ msgError.epaperId ? 'p-error' : ''}
            style={{ width: '100%' }}
          />
          {msgError.epaperId &&
            <Message style={{marginTop: '0.5em'}} severity="error" text={msgError.epaperId} />
          }
        </Card>
      </div>

      <div className="p-col-4" >
        <h4>Info</h4>
          {cronstrue.toString(form.crontime, {throwExceptionOnParseError: false})}
      </div>
    </div>
  )

};

export default New;