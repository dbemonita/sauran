import React, { memo, useEffect, useState } from 'react';
import {Dropdown} from 'primereact/dropdown';
import API from '../_helper/api';
import {TabView,TabPanel} from 'primereact/tabview';
import StatusModule from '../components/dashboards/info/StatusModule';
import {ConfigModule} from '../components/dashboards/info/ConfigModule';
import ModulePointList from '../components/dashboards/info/ModulePointList';
import LogModule from '../components/dashboards/log';
import SystemLoket from  '../components/dashboards/system/Loket';
import SystemServer from  '../components/dashboards/system/Server';

const Dashboard = (props) => {
  const [moduleSelected, setModuleSelected] = useState('');
  const [moduleList, setModuleList] = useState([]);
  const [moduleId, setModuleId] = useState('');

  useEffect(() => {
    function fetchModule() {
      API.get('admin/module')
        .then((data) => {
          setModuleList(data.data)
        })
    }

    fetchModule();
  }, []);

  const onChangeModule = (e) => {
    let currentModuleSelected = moduleList.filter(el => {
      return (el.id === e.value);
    });

    setModuleId(e.value);
    setModuleSelected(currentModuleSelected[0]);
  };

  return (
    <div className="p-grid">
      <div className="p-col-12">

        <div className="p-grid">
          <div className="p-col-12">
            <TabView renderActiveOnly={false}>
              <TabPanel header="MODULE" >
                <div className="p-grid">
                  <div className="p-col-12">
                    <Dropdown
                      style={{ minWidth: 200}}
                      value={moduleId}
                      options={moduleList}
                      onChange={onChangeModule}
                      filter={true}
                      filterPlaceholder="Select Module"
                      filterBy="label,value"
                      placeholder="Select Module"
                    />
                  </div>

                  {(moduleId !== '') ? <div className="p-col-12">
                    <TabView renderActiveOnly={false}>
                      <TabPanel header="Status" >
                        <StatusModule moduleId={moduleId}/>
                      </TabPanel>
                      <TabPanel header="Info Parsing" >
                        <ModulePointList moduleId={moduleId} />
                      </TabPanel>
                      <TabPanel header="Info Config" >
                        <ConfigModule moduleId={moduleId}/>
                      </TabPanel>
                    </TabView>
                  </div> : ''}

                  {(moduleId !== '') ?
                    <div className="p-col-12">
                      <LogModule moduleSelected={moduleSelected} moduleId={moduleId}/>
                    </div> : ''}
                </div>
              </TabPanel>

              <TabPanel header="SYSTEM" >
                <div className="p-col-12">
                  <SystemLoket />
                </div>
                <div className="p-col-12">
                  <SystemServer />
                </div>
              </TabPanel>
            </TabView>

          </div>
        </div>

      </div>
    </div>
  );

};

export default memo(Dashboard);
