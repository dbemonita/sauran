import React, {Component} from 'react';
import API from '../_helper/api';
import {ListBox} from 'primereact/listbox';
import {Button} from "primereact/button";
import {AddModule} from '../components/module/add';
import {ListParsingRef} from '../components/parsing-ref/list';


export class ModuleManagement extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      moduleList: [],
      moduleId: '',
      moduleSelected: ''
    };

    this.onSelectModule = this.onSelectModule.bind(this)
  }

  componentDidMount() {
    this.fetchModule(this.state.first);
  }

  fetchModule() {
    API.get('admin/module')
      .then((data) => {
        this.setState({
          moduleList: data.data
        });
      })
  }

  onSelectModule(e) {
    this.setState({
      moduleSelected: e.value,
      moduleId: e.value.id
    })
  }

  render() {
    const header = (
      <div style={{
        background: '#edf0f5',
        border: '1px solid #c8c8c8', padding: 10, overflow: 'auto',
        }}>
        <span style={{paddingLeft: 10, fontWeight: 'bold',verticalAlign: '-webkit-baseline-middle'}}>List Module</span>
        <Button style={{float: 'right'}} icon="pi pi-plus" className="p-button-raised p-button-rounded" onClick={(e) => this.op.toggle(e)}  />
      </div>
    );
    return (
      <div  className="p-grid">
        {/*<OverlayPanel ref={(el) => this.op = el}>*/}
        {/*  <AddModule onClose={(e) => this.op.toggle(e)} />*/}
        {/*</OverlayPanel>*/}

        {/*<div className="p-col-12 p-lg-3 p-md-3 p-sm-12">*/}
        {/*  <Card header={header} >*/}
        {/*    <ListBox*/}
        {/*      style={{width: "100%", border: 'none'}}*/}
        {/*      value={this.state.moduleSelected} options={this.state.moduleList}*/}
        {/*      onChange={this.onSelectModule}*/}
        {/*      optionLabel="name"*/}
        {/*    />*/}
        {/*  </Card>*/}
        {/*</div>*/}
        <div className="p-col-12 p-md-9 p-md-12 p-sm-12">
          <ListParsingRef moduleId={this.state.moduleId}/>
        </div>
      </div>
    )
  }
}