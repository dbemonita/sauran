const responseReducer = (state, action) => {
  switch (action.type) {
    case 'setMessage':
      return {
        ...state,
        ...action.payload
      };
    case 'removeMessage':
      return {
      };
    default:
      return state;
  }
};

export default responseReducer;