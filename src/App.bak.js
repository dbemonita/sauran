import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'font-awesome/css/font-awesome.min.css';
import './layout/layout.css';
import './layout/primeflex.css';
import './App.css';
import React, { useState, useEffect } from 'react';
import AppWrapper from './layout/AppWrapper'
import AppFooter from "./layout/AppFooter";
import AppMenu from './layout/AppMenu';
import AppTopbar from './layout/AppTopBar';
import AppInlineProfile from './layout/AppInlineProfile';
import classNames from 'classnames';
import { withContext } from "./AppContext"
import {ScrollPanel} from 'primereact/scrollpanel';
import {ProgressBar} from 'primereact/progressbar';
import {Growl} from 'primereact/growl';
import network from "./_helper/network";
import API from './_helper/api'

let growl;
const notifCode = {
  100: 'warn',
  200: 'success',
  300: 'info',
  400: 'error',
  'error': 'error',
  'warning': 'warn',
  'success': 'success',
  'info': 'info'
};
const logo = 'koala-svg.svg';
const isDesktop = () => {
  return window.innerWidth > 1024;
}
// const addClass = (element, className) => {
//   if (element.classList)
//     element.classList.add(className);
//   else
//     element.className += ' ' + className;
// }
//
// const removeClass = (element, className) => {
//   if (element.classList)
//     element.classList.remove(className);
//   else
//     element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
// };

const App = (props) => {

  const [menu, setMenu] = useState([]);
  const [menuClick, setMenuClick] = useState(false);
  const [layoutMode] = useState("overlay");
  const [staticMenuInactive] = useState(false);
  const [overlayMenuActive, setOverlayMenuActive] = useState(false);
  const [mobileMenuActive, setMobileMenuActive] = useState(false);

  const [wrapperClass, setWrapperClass] = useState({});
  const [sidebarClassName] = useState(
    classNames("layout-sidebar", { 'layout-sidebar-dark': false })
  );

  useEffect(() => {
    network.setupInterceptors({
      setOnApiRequest: props.setOnApiRequest,
      setMessage: props.setMessage,
      setNetwork: props.setNetwork,
      handlingRequestError: props.handlingRequestError
    });
  }, [props.setOnApiRequest, props.setMessage, props.setNetwork, props.handlingRequestError])

  useEffect( () => {
    if(Object.keys(props.user).length > 0){
      API.get('/admin/page')
      .then( result => {
        setMenu(result.data);
      })
    }
  }, [props.user]);

  useEffect(() => {
    if(props.message && Object.keys(props.message).length > 0){
      growl.show({
        severity: notifCode[props.message.code],
        summary: notifCode[props.message.code],
        detail: props.message.content
      });
    }
  }, [props.message])

  useEffect( () => {
    setWrapperClass(
      classNames('layout-wrapper', {
      'layout-overlay': layoutMode === 'overlay',
      'layout-static': layoutMode === 'static',
      'layout-static-sidebar-inactive': staticMenuInactive && layoutMode === 'static',
      'layout-overlay-sidebar-active': overlayMenuActive && layoutMode === 'overlay',
      'layout-mobile-sidebar-active': mobileMenuActive
    })
    )
  }, [menuClick,layoutMode, staticMenuInactive, overlayMenuActive, mobileMenuActive])

  useEffect(() => {
    if (mobileMenuActive)
      addClass(document.body, 'body-overflow-hidden');
    else
      removeClass(document.body, 'body-overflow-hidden')
  }, [mobileMenuActive]);

  const onToggleMenu = (event) => {
    setMenuClick(true);
    if (isDesktop()) {
      if (layoutMode === 'overlay') {
        setOverlayMenuActive(!overlayMenuActive);
      }
      else if (layoutMode === 'static') {
        setOverlayMenuActive(!staticMenuInactive);
      }
    }
    else {
      setMobileMenuActive(!mobileMenuActive)
    }

    event.preventDefault();
  };

  return (
    <div className={wrapperClass}>
      <Growl ref={(el) => growl = el} />
      {props.token &&
          <AppTopbar onToggleMenu={onToggleMenu}/> }

      <div className={sidebarClassName} >

      <ScrollPanel style={{height: '100%'}}>
        <div className="layout-sidebar-scroll-content">
          <div className="layout-logo">
            <img style={{maxHeight: 60}} alt="Logo" src={logo}/>
          </div>

          {/*<AppInlineProfile/>*/}

          <AppMenu model={menu} onMenuItemClick={(event) => {
            if (!event.item.items) {
              setOverlayMenuActive(false);
              setMobileMenuActive(false);
            }
          }}/>

        </div>
        </ScrollPanel>
      </div>
      <AppWrapper/>

      {(props.onApiRequest) &&
        <ProgressBar style={{
          height:5,
          zIndex:9999,
          position: 'fixed',
          width: '100%',
          bottom: 0,
          left: 0
        }} mode="indeterminate" />}
      <AppFooter {...props.network}/>
      <div className="layout-mask">

      </div>
    </div>
  )

}

export default withContext(App);
