import React, {useEffect, useState} from 'react';
import MenuAvailable from "./menu/available";
import MenuOrder from "./menu/order";
import API from '../../_helper/api';
import {Button} from "primereact/button";
import {default as LTT} from 'list-to-tree';
// import {default as IronTree} from '@denq/iron-tree';

const difference = (oldData, newData) => {
  let added =  newData.filter(el => { return !oldData.includes(el) });
  let deleted = oldData.filter(el => { return !newData.includes(el) });

  return {added, deleted};
};

const getCheckedVList = async (userId) => {
  return API.get('v2/admin/visual/checked/user/' + userId);
};

const getAllVListFlat = async () => {
  return API.get('admin/visual/list?tree=false');
};

function sortOrder() {
  return (a, b) => {
    const aid = Number(a.get('order'));
    const bid = Number(b.get('order'));

    a.set('label', a.get('label') + ' - ' + aid);
    b.set('label', b.get('label') + ' - ' + aid);
    if(aid < bid) {
      return -1;
    }else if(aid < bid){
      return 1
    }else{
      return 0;
    }
  };
}

const buildTree = (data, filtered) => {
  let tree = new LTT(data, {
    key_id: 'id',
    key_parent: 'parentId',
    key_child: 'children'
  });

  if(filtered) {
    tree.sort(sortOrder());
  }

  let record = tree.GetTree();
  return record;
};

const buildTreeUser = (data, allVListFlat, allVListFlatIdKey) => {
  let vIdList = Object.keys(data).map(el => data[el].path);
  let pathOrder = Object.keys(data).reduce((obj, item) => {
    return {
      ...obj,
      [data[item].path]: data[item].order
    };
  }, {});

  let maxOrder = 1;
  let vFilter3 = new Set([]);
  let addedPath = [];

  let parent = {};
  let epath = '';
  allVListFlat.forEach( el => {
    if(el.leaf){
      if(vIdList.includes(el.path)){
        if(maxOrder < pathOrder[el.path]) maxOrder = pathOrder[el.path];
        let paths = el.path.split('/');
        if(paths.length > 0){

          paths.forEach((path, idx) => {
            console.log('+++',idx, paths.length);
            if(idx < paths.length-1){
              console.log('*******', idx, paths.length);
              let cpath = (epath === '') ? path : epath.concat('/', path);
              console.log(cpath);
              let parentPath = allVListFlat.filter( elem => elem.data.path === cpath);
              console.log('addedPath', addedPath);
              if(!addedPath.includes(cpath)){
                if(parentPath.length > 0){
                  console.log('adding path', parentPath[0]);
                  vFilter3.add({
                    ...parentPath[0],
                    order: pathOrder[path]
                  });
                  parent[parentPath[0].parentId] = true;
                  addedPath.push(cpath);
                }
              }
              epath = cpath;
            }
          });
        }

        if(!addedPath.includes(el.path)){
          vFilter3.add({
            ...el,
            order: pathOrder[el.path]
          });

          if(parent[el.parentId] === undefined){
            vFilter3.add({
              ...allVListFlatIdKey[el.parentId],
              order: pathOrder[el.path]
            });

            parent[el.parentId] = true;
          }
          addedPath.push(el.path);
        }
      }
    }
  });

  console.log(Array.from(vFilter3));
  // debugger;

  return buildTree(Array.from(vFilter3), true);
};

const VisualSelect = (props) => {

  const [allVListFlat, setAllVListFlat] = useState([]);
  const [allVListFlatIdKey, setAllVListFlatIdKey] = useState([]);
  const [allVListTree, setAllVListTree] = useState([]);
  const [allVListTreeUser, setAllVListTreeUser] = useState([]);
  const [checkedVList, setCheckedVList] = useState([]);

  useEffect(() => {
    async function fetchData() {
      let {data} = await getAllVListFlat();

      if(data.length > 0){
        let dataIdKey = data.reduce((obj, el) => {
          return {
            ...obj,
            [el.id]: el
          };
        }, {});
        setAllVListFlat(data);
        setAllVListFlatIdKey(dataIdKey);

        let tree = buildTree(data);
        setAllVListTree(tree);
      }
    }
    fetchData();
  }, []);

  useEffect(() => {
    if(props.user){
      const fetchData = async function(){
        const {data}  = await getCheckedVList(props.user.userId);
        let tree = buildTreeUser(data, allVListFlat, allVListFlatIdKey);

        setAllVListTreeUser(tree);
        setCheckedVList(data);
      }

      fetchData();
    }
  }, [props.user, allVListFlat, allVListFlatIdKey]);

  const onChangeCheckVisual = (data) => {
    console.log('change check list', data);
    let {deleted, added} = difference(Object.keys(checkedVList), Object.keys(data));
    let newCheckedVList = checkedVList;
    if(added.length > 0){
      added.forEach(el => {
        let onlyPathArr = allVListFlat.filter( elem => elem.key.toString() === el.toString());
        if(onlyPathArr.length > 0) {
          newCheckedVList[onlyPathArr[0].key] = onlyPathArr[0];
        }
      });
    }

    if(deleted.length > 0){
      deleted.forEach( el => delete newCheckedVList[el]);
    }

    let tree = buildTreeUser(newCheckedVList, allVListFlat, allVListFlatIdKey);

    setAllVListTreeUser(tree);
  };

  const onChangeOrder = (dataTree) => {
    dataTree.sort(sortOrder());
    setAllVListTreeUser(dataTree);
  };

  const onUpdate = () => {
    console.log(allVListTreeUser);
    debugger;
  };

  return (
    <div
      className="p-grid"
      style={{
        background: 'linear-gradient(to right, #e1e4e8, #edf0f5)',
        marginTop: 0
      }}>

      <div className="p-md-12 p-sm-12">
        <Button 
          className="p-button-raised" 
          icon="pi pi-save"
          onClick={() => onUpdate()}
        />
      </div>
      <div className="p-md-6 p-sm-12">
        <span>Visual Menu List</span>
        <MenuAvailable
          checkedVList={checkedVList}
          onChangeCheckVisual={onChangeCheckVisual}
          allVListTree={allVListTree}
        />
      </div>
      <div className="p-md-6 p-sm-12">
        <span>Visual Menu List Selected (drag to re-order)</span>
        <MenuOrder
          // visualChecked={visualChecked}
          checkedVList={checkedVList}
          visualMenuUser={allVListTreeUser}
          onChangeOrder={onChangeOrder}
        />
      </div>
    </div>
  )
};

export default VisualSelect;