import React, { useEffect, memo} from 'react';
import {Message} from 'primereact/message';
import {default as networkSetup} from "../_helper/network";
import {useStateValue} from '../appState';
import {Growl} from 'primereact/growl';

let growl;
const notifyCode = {
  100: 'warn',
  200: 'success',
  300: 'info',
  400: 'error',
  'error': 'error',
  'warning': 'warn',
  'success': 'success',
  'info': 'info'
};

const AppTopbar = (props) => {
  const [{user, message}, dispatch] = useStateValue();

  useEffect(() => {
    networkSetup.setupInterceptors(dispatch);
  }, [dispatch]);

  useEffect(() => {
    if(message && Object.keys(message).length > 0){
      growl.show({
        severity: notifyCode[message.code],
        summary: notifyCode[message.code],
        detail: message.content
      });
    }
  }, [message]);

  const onLogout = () => {
    if(user.username){
      localStorage.removeItem("user");
      localStorage.removeItem("token");
      dispatch({
        type: 'removeUser'
      });
    }
  };

  return (
    <React.Fragment>
    <div className="layout-topbar clearfix">
      <button className="p-link layout-menu-button" onClick={props.onToggleMenu}>
        <span className="pi pi-bars" />
      </button>
      <div className="layout-topbar-icons">
        <Message severity="info" text={ "v" + process.env.REACT_APP_VERSION} />
        <button className="p-link">
          <span className="layout-topbar-icon pi pi-sign-out" onClick={onLogout}/>
        </button>
      </div>
    </div>
    <Growl ref={(el) => growl = el} />
    </React.Fragment>
  );
};

export default memo(AppTopbar, () => {
  return false;
});