import React, { useEffect, useState } from 'react';
import { subscriber, emitter } from "../../../_helper/subscriber";
import { Fieldset } from 'primereact/fieldset';

const SystemServer = (props) => {
  const getColor = (value) => {
    let color = {};
    switch (true) {
      case value < 60:
        color = { color: 'green' };
        break;
      case value >= 60:
        color = { color: 'orange' };
        break;
      case value >= 90:
        color = { color: 'red' };
        break;
      default:
        color = { color: 'black' };
        break;
    }

    return color;
  };

  const [dataEnv, setDataEnv] = useState({});
  const [dataProcess, setDataProcess] = useState([]);

  useEffect(() => {

    function onWsMessage(resp) {
      setDataProcess(resp);
    }

    let systemMonitor = subscriber.subscribe('systemServerMonitoring');
    systemMonitor.watch(onWsMessage);

    emitter.emit('getSystemServerMonitoring', '', (err, res) => {
      if (err) {
        console.log(err);
        // this.props.handlingRequestError(err);
      } else {
        setDataEnv(res);
      }
    });

    return function cleaning() {
      let channel = subscriber.channel('systemServerMonitoring');
      if (channel) {
        channel.unwatch(onWsMessage);
        subscriber.unsubscribe('systemServerMonitoring');
      }
    };
  }, []);

  return (
    <Fieldset legend="System " toggleable={true}>
      {Object.keys(dataEnv).length > 0 &&
        <div className="p-grid">
          <div className="p-col">
            <h4>OS Info</h4>
            <ul>
              <li>Platform: {dataEnv.os.platform}, {dataEnv.os.distro}</li>
              <li>Arch: {dataEnv.os.arch}</li>
            </ul>
          </div>
          <div className="p-col">
            <h4>Disk Usage</h4>
            <ul>
              {dataEnv.disk.map((el, i) => {
                return (
                  <li key={i}>
                    {el.mount} - <span key={"x-" + i} style={getColor(el.used)}>{el.used}%</span>
                  </li>
                )
              })}
            </ul>
          </div>
          <div className="p-col">
            <h4>Webservices</h4>
            <ul>
              <li>Mysql: {dataEnv.webservice.mysql}</li>
              <li>Nginx: {dataEnv.webservice.nginx}</li>
              <li>NodeJs: {dataEnv.webservice.node}</li>
              <li>Redis: {dataEnv.webservice.redis}</li>
            </ul>
          </div>
          <div className="p-col">
            <h4>Network</h4>
            <ul>
              {dataEnv.network.map((el, i) => {
                return (
                  <li key={i}>
                    {el.iface}
                    <ul key={"xx-" + i} style={{ listStyle: 'none' }}>
                      <li key={"xxx" + i}>IP : {el.ip4}</li>
                    </ul>
                  </li>
                )
              })}
            </ul>
          </div>
        </div>
      }
      <hr />
      {Object.keys(dataEnv).length > 0 &&
        <div className="p-grid">
          <div className="p-col-12">
            <h4 style={{ marginBottom: 0 }}>Resource Usage</h4>
          </div>
          {dataProcess.map((el, i) => {
            return (
              <div key={"k-" + i} className="p-col">
                <ul key={i}>
                  {Object.keys(dataProcess[i]).map((elem, key) => {
                    return (
                      <li key={key}>{elem} : {dataProcess[i][elem]}</li>
                    )
                  })}
                </ul>
              </div>
            )
          })}
        </div>
      }
    </Fieldset>
  )
};

export default SystemServer;