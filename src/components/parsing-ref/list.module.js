import React, {memo} from 'react';
import {Dropdown} from 'primereact/dropdown';

const ListModule = ({props}) => {
  console.log('render list module');

  return (
    <Dropdown
      value={props.value[props.rowIndex].moduleId}
      options={props.moduleList}
      onChange={(e) => props.onModuleChange(props, e)}
      style={{width:'100%'}}
      placeholder="Select a Module"
    />);
};

export default memo(ListModule);
