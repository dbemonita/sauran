import React, {useState, useEffect} from "react";
import { ScrollPanel } from "primereact/scrollpanel";
import classNames from "classnames";
import AppInlineProfile from "./AppInlineProfile";
import AppMenu from "./AppMenu";

const logo = 'koala-svg.svg';

const addClass = (element, className) => {
  if (element.classList)
    element.classList.add(className);
  else
    element.className += ' ' + className;
};
const removeClass = (element, className) => {
  if (element.classList)
    element.classList.remove(className);
  else
    element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
};

const Sidebar = (props) => {
  const [sidebarClassName] = useState(
    classNames("layout-sidebar", { 'layout-sidebar-dark': false })
  );
  const [staticMenuInactive] = useState(false);
  const [layoutMode] = useState("overlay");
  const [overlayMenuActive, setOverlayMenuActive] = useState(false);
  const [mobileMenuActive, setMobileMenuActive] = useState(false);

  useEffect(() => {
    if (mobileMenuActive)
      addClass(document.body, 'body-overflow-hidden');
    else
      removeClass(document.body, 'body-overflow-hidden')
  }, [mobileMenuActive]);

  return (
    <div className={sidebarClassName} >
       <ScrollPanel style={{ height: '100%' }}>
         <div className="layout-sidebar-scroll-content">
           <div className="layout-logo">
             <img style={{ maxHeight: 60 }} alt="Logo" src={logo} />
           </div>

           <AppInlineProfile />

           <AppMenu onMenuItemClick={(event) => {
             if (!event.item.items) {
               setOverlayMenuActive(false);
               setMobileMenuActive(false);
               props.onMenuItemClick({
                  layoutMode, staticMenuInactive, overlayMenuActive, mobileMenuActive
                });
             }
           }} />

         </div>
       </ScrollPanel>

     </div>
  )
};

export default Sidebar;