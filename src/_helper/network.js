import API from './api';
import socket from "./socket-cluster";

let intervalReEquest;
let _dispatch;

export default {
  setupInterceptors: (dispatch) => {
    // _appContext = {};
    _dispatch = dispatch;
    API.interceptors.response.use(fullFilledResponse, rejectedResponse);
    API.interceptors.request.use(fullFilledRequest, errorRequest);

    socket.on('connect', function (status) {
      // console.log('connected');
      if(!status.isAuthenticated){
        // console.log('token ', token);
        // console.log('not authed');
      }
      _dispatch({
        type: 'setNetwork',
        payload: {type: 'ws', status: true}
      });
    });

    socket.on('deauthenticate', function (status) {
      console.log(status);
    });

    socket.on('error', function(data) {
      console.log('error : ' + data);
      // appContext.setNetwork('ws', false);

      _dispatch({
        type: 'setNetwork',
        payload: {type: 'ws', status: false}
      });
    });

    socket.on('subscribeFail', function(channelname) {
      console.log('subscribeFail:' + channelname);
    });
  }
}

function fullFilledRequest(config){
  // _appContext.setOnApiRequest(true);
  _dispatch({
    type: 'setOnApiRequest',
    payload: true
  });

  let tokenStorage = localStorage.getItem('token');
  config.headers.common['Authorization'] = 'Bearer ' + tokenStorage;

  return config;
}

function errorRequest(error){
  // _appContext.setOnApiRequest(false);
  _dispatch({
    type: 'setOnApiRequest',
    payload: false
  });
  console.log('request error', error.request);
  return Promise.reject(error);
}

function fullFilledResponse(response){
  if(response.headers['message-code']){
    const messageCode = response.headers['message-code'];
    const messageContent = response.headers['message-content'];

    // _appContext.setMessage({
    //   code: messageCode,
    //   content: messageContent
    // });
    _dispatch({
      type: 'setMessage',
      payload: {code: messageCode, content: messageContent}
    });

  }

  // _appContext.setNetwork('api', true);
  // _appContext.setOnApiRequest(false);
  _dispatch({
    type: 'setNetwork',
    payload: {type: 'api', status: true}
  });

  _dispatch({
    type: 'setOnApiRequest',
    payload: false
  });
  if (response.data.error) {
    console.log('wtf');
  }

  return response;
}

function rejectedResponse(error){
  // _appContext.setOnApiRequest(false);
  const originalRequest = error.config;
  if(error.response){
    if(error.response.status === 401){
      if(originalRequest.url.includes('login')){
        return Promise.reject(error);
      }

      originalRequest._retry = true;
      if(intervalReEquest){
        clearInterval(intervalReEquest);
      }

      let token = localStorage.getItem('token');
      return refreshToken(token)
        .then( token => {
          localStorage.setItem('token', token);
          originalRequest.headers.Authorization = 'Bearer ' + token;
          return API(originalRequest);
        })
        .catch( err => {
          return Promise.reject(err);
        })
    }else{
      console.log('Network error from here');
    }
  } else if (error.request) {

    //TODO: trigger notif error
    // _appContext.handlingRequestError(error);

    if(!error.message === 'Network Error'){
      if(!intervalReEquest){
        intervalReEquest = setInterval(function (){
          originalRequest._retry = true;
          return API(originalRequest);
        }, 5000, originalRequest);
      }
    }

    return Promise.reject(error);
  } else {
    return Promise.reject(error);
  }
}

async function refreshToken (token) {
  return API.post('/auth/refresh',{token: token})
  .then( response => {
    console.log('new token ', response.data.token);
    // socket.emit('login', response, function (err) {
    //   if (err) {
    //     console.log(err);
    //   } else {
    //     console.log('socket logged in');
    //   }
    // });
    return Promise.resolve(response.data.token);
  })
  .catch( err => {
    console.log('error on refresh token');
    return Promise.reject(err);
  })
}
