import React, { useState, useEffect } from 'react';
import { InputText } from 'primereact/inputtext';
import {Dropdown} from "primereact/dropdown";
import { Button } from 'primereact/button';
import API from "../../_helper/api";

const EditModule = (props) => {

  const [data, setData] = useState({
    name: '',
    serial_number: '',
    status: 1,
    timeout: '',
    type: ''
  });

  const [typeList] = useState([
    {
      label: 'A',
      value: 'A'
    },{
      label: 'B',
      value: 'B'
    },{
      label: 'C',
      value: 'C'
    },{
      label: 'Modbus',
      value: 'Modbus'
    }
  ])

  useEffect(() => {
    if(Object.keys(props.data).length > 0){
      setData(props.data)
    }
  }, [props.data]);

  const onChangeValue = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }

  const onSave = () => {
    API.patch('admin/module/' + data.id, {
      name: data.name,
      serialNumber: data.serial_number,
      type: data.type,
      status: data.status,
      timeout: data.timeout
    }).then(result => {
      props.done()
    }).catch(err => {
      console.log(err);
    })
  }

  return (
    <div className="p-grid p-fluid">
      <div className="p-col-12 p-lg-12">
        <h1> Module Edit {data.name} </h1>
        <div className="card card-w-title">
          <div className="p-grid">
            <div className="p-col-12 p-md-12">
              <label htmlFor="name">Name</label>
              <InputText name="name" value={data.name} onChange={onChangeValue} autoFocus />
            </div>
          </div>

          <div className="p-grid">
            <div className="p-col-12 p-md-12">
              <label htmlFor="parent_id">Serial Number</label>
              <InputText name="serial_number" value={data.serial_number} onChange={onChangeValue} />
            </div>
          </div>

          <div className="p-grid">
            <div className="p-col-12 p-md-6">
              <label htmlFor="address">Address</label>
              <Dropdown
                name="type"
                value={data.type}
                options={typeList}
                onChange={onChangeValue}
                placeholder="Select Module Type"
              />
            </div>
          </div>

          <div className="p-grid">
            <div className="p-col-12 p-md-12">
              <label htmlFor="timeout">Timeout</label>
              <InputText name="timeout" value={data.timeout} onChange={onChangeValue} />
            </div>
          </div>

          <Button label="Save" className="p-button-raised" onClick={onSave}/>

        </div>
      </div>
    </div>
  )

}

export default EditModule;
